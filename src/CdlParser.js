/* --- CdlParser.js
Recursively build out the CDL representation from XML. Work asynchronously because we
sometimes need to fetch more CDL while parsing, which is why we use Promise.

For example, for

  <comp char='亻' uni='4EBB' points='0,0 34,128' />

fetch the CDL for the comp 亻 (U+4EBB) before parsing the remainder of the whole character (such as 他 U+4ED6).

Notes about map, Promise.all, and bind, as used in this module:

map is a built-in javascript method. It takes in a function, runs it on each array
element, and returns a new array with the results. E.g.:
var x = [1,2,3].map(function(num) { return num + 2; }); // x is [3,4,5]

return Promise.all(childNodes.map(this._recursiveBuildCDLRepresentation.bind(this)));

is equivalent to the following:

var recursiveCdlPromises = [];
for (var i = 0; i < childNodes.length; i++) {
  var childNode = childNodes[i];
  recursiveCdlPromises.push(this._recursiveBuildCDLRepresentation(childNode));
}
return Promise.all(recursiveCdlPromises);

Promise.all is a function which takes an array of promises and returns a new promise which resolves
after all the promises passed in have finished resolving. So, this code

  return Promise.all(childNodes.map(this._recursiveBuildCDLRepresentation.bind(this)));

is recursively calling this._recursiveBuildCDLRepresentation(node) on all the child nodes, then waiting
until all those async calls have finished before continuing.

There's a new async/await syntax in the new version of javascript which makes working with Promises
a lot cleaner, but it's going to take a bit for all the browsers to start supporting it.

The .bind(this) call is a quirk of javascript - functions constantly forget what 'this' is supposed to be when they're
called out of context. Calling .bind(this) forces the function to remember the correct 'this' when it's called.
*/

var CdlStroke = require('./CdlStroke');
var CdlChar = require('./CdlChar');
var Point = require('./Point');
var Utils = require('./Utils');

var parseHead = function(headString) {
  if (headString === 'cut') return CdlStroke.STROKE_HEAD_CUT;
  if (headString === 'corner') return CdlStroke.STROKE_HEAD_CORNER;
  if (headString === 'vertical') return CdlStroke.STROKE_HEAD_VERTICAL;
  return CdlStroke.STROKE_HEAD_NORMAL;
};

var parseTail = function(tail) {
  if (tail === 'cut') return CdlStroke.STROKE_TAIL_CUT;
  if (tail === 'long') return CdlStroke.STROKE_TAIL_LONG;
  if (tail === 'reserved') return CdlStroke.STROKE_TAIL_RESERVED;
  return CdlStroke.STROKE_TAIL_NORMAL;
};

/* --- parsePoints:
    Parse a points attribute like '10,0 96,0 82,42 128,42 110,128 90,114'.
    First split by space ' ', then split by comma ','.
    ---return a Point array, or null if no points element in the given xmlNode.
*/
var parsePoints = function(xmlNode) {
  if (!xmlNode.$.points) return null;
  return xmlNode.$.points.split(' ').map(function(pointStr) {
    // TODO: don't assume integer coords (parseInt), should support float -- parseFloat OK?
    var coords = pointStr.split(',').map(function(coord) { return parseInt(coord, 10); });
    return new Point(coords[0], coords[1]);
  });
};

var getUnicodeHex = function(charOrUnicode) {
  /* This function may be called with charOrUnicode being a Hanzi string
     like '𩨎' or a hex string like '29A0E'. Be careful to support code points
     greater than U+FFFF that are more than one code unit in UTF-16 and up to six
     digits in hex. In JavaScript, string.length is the number of UTF-16 code units,
     not code points or characters. */
  if (!charOrUnicode.match(/^[0-9A-Fa-f]+$/)) { // not a string of hex digits
    return charOrUnicode.codePointAt(0).toString(16).toUpperCase(); // codePointAt NOT charCodeAt!
  }
  var unicode = charOrUnicode.toUpperCase();
  if (unicode.length > 6 || !unicode.match(/^[0-9A-F]+$/)) {
    throw new Error(
      'Invalid Chinese character or Unicode hex: ' +
      charOrUnicode + '. Must be either a single Chinese character ' +
      'or the Unicode hex for a single character'
    );
  }
  return unicode;
};

/* --- excludeNull:
    Enable use as in [].filter(excludeNull) to exclude null elements.

    ---param elm the element to be included or excluded.
    ---return true to include the element, false to exclude it.

    Arrays of objects representing cdl/comp/stroke elements may include null elements
    corresponding to XML comments or any XML elements other than cdl/comp/stroke.
    Compare how _recursiveBuildCDLRepresentation returns empty promise if XML is invalid or unrecognized.
*/
var excludeNull = function(elm) {
  return elm; // Boolean context; true except for false, 0, "", null, undefined, and NaN.
};

var CdlParser = function(loaderFunc, xmlParser) {
  this._loader = loaderFunc;
  this._xmlParser = xmlParser;
};

CdlParser.prototype.loadAndParse = function(charOrUnicode, variant) {
  return this._loadParsedCharXml(charOrUnicode, variant)
    .then(function(parsedXml) {
      return this._recursiveBuildCDLRepresentation(parsedXml);
    }.bind(this));
};

CdlParser.prototype._loadParsedCharXml = function(charOrUnicode, variant) {
  return Promise.resolve().then(function() {
    return this._loader(getUnicodeHex(charOrUnicode), variant || 0, this._xmlParser);
  }.bind(this)).then(function(charXml) {
    // if this is already parsed by the loader, just return it
    if (typeof charXml === 'object') {
      return charXml;
    }
    return Utils.parseXml(charXml, this._xmlParser);
  }.bind(this));
};

/* --- _recursiveBuildCDLRepresentation:
    Recursively build a CDL representation.

    ---param xmlNode the XML node.
    ---return a promise for an array of CdlStroke and/or CdlChar objects,
              or an empty promise if the XML is invalid or unrecognized.
*/
CdlParser.prototype._recursiveBuildCDLRepresentation = function(xmlNode) {
  return Promise.resolve().then(function() {
    var points;
    if (xmlNode['#name'] === 'stroke') {
      var strokeType = xmlNode.$.type;
      var head = parseHead(xmlNode.$.head);
      var tail = parseTail(xmlNode.$.tail);
      points = parsePoints(xmlNode);
      return new CdlStroke(strokeType, points, head, tail);
    } else if (xmlNode['#name'] === 'cdl' || xmlNode['#name'] === 'comp') {
      var char = xmlNode.$.char;
      var unicode = xmlNode.$.uni;
      var variant = parseInt(xmlNode.$.variant, 10) || 0;
      var radical = xmlNode.$.radical;
      points = parsePoints(xmlNode);
      return Promise.resolve().then(function() {
        var childrenAreIncluded = xmlNode.$$;
        if (childrenAreIncluded) {
          return Promise.all(xmlNode.$$.map(this._recursiveBuildCDLRepresentation.bind(this)));
        }
        return this._loadParsedCharXml(unicode, variant)
          .then(function(fullCharXmlNode) {
            variant = parseInt(fullCharXmlNode.$.variant, 10) || 0; // overwrite the variant with what's in the full spec
            radical = fullCharXmlNode.$.radical; // overwrite the radical with what's in the full spec
            return fullCharXmlNode.$$; // extract just the children
          })
          .then(function(childNodes) {
            return Promise.all(childNodes.map(this._recursiveBuildCDLRepresentation.bind(this)));
          }.bind(this));
      }.bind(this)).then(function(childObjs) {
        return new CdlChar(char, unicode, childObjs.filter(excludeNull), points, radical, variant);
      });
    }
    // if the XML is invalid or unrecognized just return an empty promise
    return Promise.resolve();
  }.bind(this));
};

module.exports = CdlParser;
