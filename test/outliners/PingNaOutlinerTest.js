/* eslint-env mocha */
var assert = require('chai').assert;
var PingNaOutliner = require('../../src/outliners/PingNaOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('PingNaOutliner#addPoints', function() {
  it('should return the same output as GetPingNaOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(788, 1772),
      new Point(2920, 904),
      new Point(3856, 904)
    ];
    var outline = new Outline(32, 103, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(788, 1885, 1),
      new OutlinePoint(1321, 1017, 2),
      new OutlinePoint(2920, 1017, 2),
      new OutlinePoint(4082, 1130, 1),
      new OutlinePoint(4082, 1105, 1),
      new OutlinePoint(3938, 1046, 2),
      new OutlinePoint(3843, 948, 2),
      new OutlinePoint(3743, 791, 1),
      new OutlinePoint(2920, 791, 2),
      new OutlinePoint(1321, 791, 2),
      new OutlinePoint(788, 1772, 1)
    ];

    PingNaOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
