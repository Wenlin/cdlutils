/* eslint-env mocha */
var assert = require('chai').assert;
var NaOutliner = require('../../src/outliners/NaOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('NaOutliner#addPoints', function() {
  it('should return the same output as GetNaOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(1308, 3088), new Point(3804, 800)];
    var outline = new Outline(32, 115, 1, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1308, 3088, 1),
      new OutlinePoint(1769, 2041, 2),
      new OutlinePoint(2818, 1095, 2),
      new OutlinePoint(3994, 927, 1),
      new OutlinePoint(3994, 899, 1),
      new OutlinePoint(3860, 862, 2),
      new OutlinePoint(3771, 802, 2),
      new OutlinePoint(3677, 706, 1),
      new OutlinePoint(2628, 1113, 2),
      new OutlinePoint(1691, 1927, 2),
      new OutlinePoint(1280, 3074, 1)
    ];

    NaOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetNaOutline in ch_zzsol.c case #2', function() {
    var refPoints = [new Point(2679, 4024), new Point(3752, 2563)];
    var outline = new Outline(32, 115, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2564, 4208, 1),
      new OutlinePoint(2679, 4208, 2),
      new OutlinePoint(2794, 4139, 2),
      new OutlinePoint(2944, 4024, 1),
      new OutlinePoint(2794, 3925, 1),
      new OutlinePoint(2991, 3326, 2),
      new OutlinePoint(3439, 2786, 2),
      new OutlinePoint(3942, 2690, 1),
      new OutlinePoint(3942, 2662, 1),
      new OutlinePoint(3808, 2625, 2),
      new OutlinePoint(3719, 2565, 2),
      new OutlinePoint(3625, 2469, 1),
      new OutlinePoint(3160, 2767, 2),
      new OutlinePoint(2746, 3365, 2),
      new OutlinePoint(2564, 4208, 1)
    ];

    NaOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
