/* eslint-env mocha */
var assert = require('chai').assert;
var ArcQuadrantOutliner = require('../../src/outliners/ArcQuadrantOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ArcQuadrantOutliner#addPoints', function() {
  it('should return the same output as GetArcQuadrantOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(1282, 2308), new Point(788, 1814)];
    var outline = new Outline(32, 120, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1282, 2188, 1),
      new OutlinePoint(1245, 2188, 2),
      new OutlinePoint(1240, 2187, 2),
      new OutlinePoint(1207, 2178, 1),
      new OutlinePoint(1174, 2169, 2),
      new OutlinePoint(1166, 2164, 2),
      new OutlinePoint(1136, 2149, 1),
      new OutlinePoint(1106, 2134, 2),
      new OutlinePoint(1099, 2131, 2),
      new OutlinePoint(1072, 2107, 1),
      new OutlinePoint(1046, 2087, 2),
      new OutlinePoint(1039, 2080, 2),
      new OutlinePoint(1016, 2057, 1),
      new OutlinePoint(994, 2032, 2),
      new OutlinePoint(985, 2019, 2),
      new OutlinePoint(970, 1994, 1),
      new OutlinePoint(953, 1967, 2),
      new OutlinePoint(947, 1956, 2),
      new OutlinePoint(935, 1928, 1),
      new OutlinePoint(923, 1900, 2),
      new OutlinePoint(922, 1894, 2),
      new OutlinePoint(916, 1871, 1),
      new OutlinePoint(911, 1844, 2),
      new OutlinePoint(908, 1839, 2),
      new OutlinePoint(908, 1814, 1),
      new OutlinePoint(668, 1814, 1),
      new OutlinePoint(668, 1839, 2),
      new OutlinePoint(671, 1888, 2),
      new OutlinePoint(676, 1915, 1),
      new OutlinePoint(682, 1946, 2),
      new OutlinePoint(699, 1996, 2),
      new OutlinePoint(711, 2024, 1),
      new OutlinePoint(723, 2052, 2),
      new OutlinePoint(745, 2097, 2),
      new OutlinePoint(762, 2124, 1),
      new OutlinePoint(783, 2153, 2),
      new OutlinePoint(814, 2192, 2),
      new OutlinePoint(836, 2217, 1),
      new OutlinePoint(855, 2242, 2),
      new OutlinePoint(896, 2281, 2),
      new OutlinePoint(922, 2301, 1),
      new OutlinePoint(947, 2319, 2),
      new OutlinePoint(998, 2352, 2),
      new OutlinePoint(1028, 2367, 1),
      new OutlinePoint(1058, 2382, 2),
      new OutlinePoint(1112, 2401, 2),
      new OutlinePoint(1145, 2410, 1),
      new OutlinePoint(1180, 2419, 2),
      new OutlinePoint(1245, 2428, 2),
      new OutlinePoint(1282, 2428, 1)
    ];

    ArcQuadrantOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
