/* eslint-env mocha */
var assert = require('chai').assert;
var HengPieOutliner = require('../../src/outliners/HengPieOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengPieOutliner#addPoints', function() {
  it('should return the same output as GetHengPieOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1854, 3884),
      new Point(3388, 3884),
      new Point(736, 2568)
    ];
    var outline = new Outline(32, 111, 1, 2);

    var expectedOutlinePoints = [
      new OutlinePoint(1854, 3852, 1),
      new OutlinePoint(1854, 3916, 1),
      new OutlinePoint(3228, 3916, 1),
      new OutlinePoint(3377, 4106, 1),
      new OutlinePoint(3481, 4039, 2),
      new OutlinePoint(3593, 3895, 2),
      new OutlinePoint(3634, 3751, 1),
      new OutlinePoint(3499, 3673, 1),
      new OutlinePoint(3007, 3095, 2),
      new OutlinePoint(1846, 2652, 2),
      new OutlinePoint(641, 2441, 1),
      new OutlinePoint(625, 2457, 1),
      new OutlinePoint(1734, 2696, 2),
      new OutlinePoint(2802, 3198, 2),
      new OutlinePoint(3255, 3852, 1)
    ];

    HengPieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
