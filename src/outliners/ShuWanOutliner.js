var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addSWGInsidePoints(outline, refPoints[1], refPoints[2]);
    OutlineHelpers.addSCRightEndPoints(outline, refPoints[3]);
    OutlineHelpers.addSWGOutsidePoints(outline, refPoints[1], refPoints[2]);
  }
};
