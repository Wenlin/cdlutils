/* eslint-env mocha */
var assert = require('chai').assert;
var DianOutliner = require('../../src/outliners/DianOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('DianOutliner#addPoints', function() {
  it('should return the same output as GetDianOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(1932, 4128), new Point(2296, 3604)];
    var outline = new Outline(32, 115, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1867, 4222, 1),
      new OutlinePoint(2478, 3886, 2),
      new OutlinePoint(2469, 3736, 2),
      new OutlinePoint(2380, 3613, 1),
      new OutlinePoint(2291, 3490, 2),
      new OutlinePoint(2195, 3507, 2),
      new OutlinePoint(2190, 3635, 1),
      new OutlinePoint(2186, 3762, 2),
      new OutlinePoint(2026, 3993, 2),
      new OutlinePoint(1844, 4206, 1)
    ];

    DianOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
