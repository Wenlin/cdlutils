/* eslint-env mocha */
var assert = require('chai').assert;
var PieDianOutliner = require('../../src/outliners/PieDianOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('PieDianOutliner#addPoints', function() {
  it('should return the same output as GetPieDianOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1182, 4128),
      new Point(728, 2308),
      new Point(1816, 1112)
    ];
    var outline = new Outline(32, 111, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(745, 2371, 1),
      new OutlinePoint(1821, 1553, 2),
      new OutlinePoint(1966, 1261, 2),
      new OutlinePoint(1896, 1131, 1),
      new OutlinePoint(1826, 1002, 2),
      new OutlinePoint(1732, 1007, 2),
      new OutlinePoint(1610, 1240, 1),
      new OutlinePoint(1488, 1472, 2),
      new OutlinePoint(1070, 1932, 2),
      new OutlinePoint(634, 2371, 1),
      new OutlinePoint(818, 2692, 2),
      new OutlinePoint(995, 3363, 2),
      new OutlinePoint(1071, 4239, 1),
      new OutlinePoint(1179, 4213, 2),
      new OutlinePoint(1295, 4157, 2),
      new OutlinePoint(1337, 4101, 1),
      new OutlinePoint(1271, 4061, 1),
      new OutlinePoint(1180, 3268, 2),
      new OutlinePoint(966, 2661, 2)
    ];

    PieDianOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
