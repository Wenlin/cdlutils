var OutlinePoint = require('../OutlinePoint');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    if (refPoints[0].x !== refPoints[1].x) { // slant
      // TODO: test this branch
      var ctrl = outline.getLastOutlinePoint().toPoint();
      ctrl.x -= outline.shuRadius * 2;
      OutlineHelpers.addNortheastCornerPoint(outline, refPoints[1]);
      OutlineHelpers.addHengRightEndOutlinePoints(outline, refPoints[1], refPoints[2]);
      OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[1]);
      outline.addPoint(ctrl, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(ctrl, OutlinePoint.OFF_CURVE_CUBIC);
    } else { // no slant
      OutlineHelpers.addNortheastCornerPoint(outline, refPoints[1]);
      OutlineHelpers.addHengRightEndOutlinePoints(outline, refPoints[1], refPoints[2]);
      OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[1]);
    }
  }
};
