var Point = require('./Point');

var OutlinePoint = function(x, y, flag) {
  this.x = x;
  this.y = y;
  this.flag = flag;
};

// ----- consts -----

OutlinePoint.OFF_CURVE_QUADRATIC = 0; /* bits 0, 1 both clear: off curve, 2nd-order (quadratic) */
OutlinePoint.ON_CURVE            = 1; /* bit 0 set */
OutlinePoint.OFF_CURVE_CUBIC     = 2; /* bit 1 set: off curve, 3rd-order (cubic) */

// ----- getters -----

OutlinePoint.prototype.toPoint = function() { return new Point(this.x, this.y); };

module.exports = OutlinePoint;
