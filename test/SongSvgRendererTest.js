/* eslint-env mocha */
var assert = require('chai').assert;
var SongSvgRenderer = require('../src/SongSvgRenderer');
var OutlinePoint = require('../src/OutlinePoint');
var splitParseAndRound = require('./Helpers').splitParseAndRound;

describe('SongSvgRenderer#getPathString', function() {
  it('should return the same output as WriteSVGOutlineStroke in ch_zzstr.c case #1', function() {
    var outlinePoints = [
      new OutlinePoint(3606, 8499, 1),
      new OutlinePoint(3941, 8424, 2),
      new OutlinePoint(4302, 8261, 2),
      new OutlinePoint(4431, 8099, 1),
      new OutlinePoint(4106, 7884, 1),
      new OutlinePoint(4106, 7884, 1),
      new OutlinePoint(3358, 4051, 2),
      new OutlinePoint(1590, 1112, 2),
      new OutlinePoint(-245, -293, 1),
      new OutlinePoint(-277, -261, 1),
      new OutlinePoint(1361, 1244, 2),
      new OutlinePoint(2938, 4392, 2),
      new OutlinePoint(3606, 8499, 1)
    ];
    var height = 8192;
    var y = 0;

    var expectedPathString = 'M56.3438,-4.7969\n' +
      '\tC61.5781,-3.6250 67.2188,-1.0781 69.2344,1.4531\n' +
      '\tL64.1562,4.8125\n' +
      '\tL64.1562,4.8125\n' +
      '\tC52.4688,64.7031 24.8438,110.6250 -3.8281,132.5781\n' +
      '\tL-4.3281,132.0781\n' +
      '\tC21.2656,108.5625 45.9062,59.3750 56.3438,-4.7969\n' +
      '\tL56.3438,-4.7969\n' +
      '\tz';

    var pathString = SongSvgRenderer.getPathString(outlinePoints, y, height);
    assert.deepEqual(splitParseAndRound(pathString), splitParseAndRound(expectedPathString));
  });
});
