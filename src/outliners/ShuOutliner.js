var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addShuBottomOutlinePoints(outline, refPoints[1]);
  }
};
