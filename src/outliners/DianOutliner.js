var Utils = require('../Utils');
var Point = require('../Point');
var OutlinePoint = require('../OutlinePoint');
var HengOutliner = require('./HengOutliner');

module.exports = {
  addPoints: function(outline, refPoints) {
    /* head=tail='cut' means this is 横点, as in ⻗. */
    if (outline.isHeadCut() && outline.isTailCut()) {
      HengOutliner.addPoints(outline, refPoints);
      return;
    }
    var rad = outline.shuRadius; // dianRadius
    var dx = refPoints[1].x - refPoints[0].x;
    var dy = refPoints[1].y - refPoints[0].y;
    var hypotenuse = Utils.hypotenuse(dx, dy);

    /* First make all points relative to refPoints[0].
            The whole stroke is laid out horizontally. */
    /* Total ten points.
        a and j are first and last points, respectively. They
        are close together near refPoints[0], joined by a short
        straight line. */

    var j = new Point(0 - rad, 0 - Utils.intCast(rad / 4));
    var a = new Point(0 - rad, 0);

    /* b, c, e, f, h, and i are OFF_CURVE_CUBIC points. */
    var b = new Point(Utils.skPercent(hypotenuse, 80), Utils.skPercent(rad, 270));
    var c = new Point(hypotenuse - Utils.skPercent(rad, 8), Utils.skPercent(rad, 190));
    var e = new Point(hypotenuse + Utils.skPercent(rad, 80), 0 - Utils.skPercent(rad, 60));
    var f = new Point(hypotenuse + Utils.skPercent(rad, 20), 0 - Utils.skPercent(rad, 120));
    var h = new Point(Utils.skPercent(hypotenuse, 70), 0);
    var i = new Point(Utils.intCast((j.x + h.x) / 2), 0); // AFTER defining h.x

    /* d and g are ON_CURVE. Easiest to do them after
        their adjacent control points, and make d and g the midpoints,
        thus ensuring that cde and fgh are colinear.

        Note: for TrueType 2nd-order curves, all the off-curve points
            should be closer to the curve (decrease distance from nearest
            on-curve point by 25%). A bonus would be that we wouldn't
            need d and g (let the rasterizer insert them); only 8 points
            instead of ten. */
    var d = new Point(Utils.intCast((c.x + e.x) / 2), Utils.intCast((c.y + e.y) / 2));
    var g = new Point(Utils.intCast((f.x + h.x) / 2), Utils.intCast((f.y + h.y) / 2));

    outline.addPoint(a, OutlinePoint.ON_CURVE);
    outline.addPoint(b, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(c, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(d, OutlinePoint.ON_CURVE);
    outline.addPoint(e, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(f, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(g, OutlinePoint.ON_CURVE);
    outline.addPoint(h, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(i, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(j, OutlinePoint.ON_CURVE);

    /* Rotate clockwise around origin, and add refPoints[0] */
    outline.rotateAndTranslate(refPoints[0], dx, dy, hypotenuse);
  }
};
