/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheOutliner = require('../../src/outliners/HengZheOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheOutliner#addPoints', function() {
  it('should return the same output as GetHengZheOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(696, 1295),
      new Point(1896, 1295),
      new Point(1896, 776)
    ];
    var outline = new Outline(32, 61, 1, 2);

    var expectedOutlinePoints = [
      new OutlinePoint(696, 1263, 1),
      new OutlinePoint(696, 1327, 1),
      new OutlinePoint(1808, 1327, 1),
      new OutlinePoint(1890, 1417, 1),
      new OutlinePoint(1947, 1396, 2),
      new OutlinePoint(2008, 1352, 2),
      new OutlinePoint(2031, 1308, 1),
      new OutlinePoint(1957, 1234, 1),
      new OutlinePoint(1957, 695, 1),
      new OutlinePoint(1914, 652, 2),
      new OutlinePoint(1878, 634, 2),
      new OutlinePoint(1835, 634, 1),
      new OutlinePoint(1835, 1263, 1)
    ];

    HengZheOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
