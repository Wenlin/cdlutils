var Utils = require('../Utils');
var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addHengPieOutlineCornerPoints(outline, refPoints[1]);
    var curveStartPoint = outline.getLastOutlinePoint().toPoint();
    outline.deleteLastPoint(); // don't add curveStartPoint twice!

    /* Inside corner (also left top of pie): */
    var bottomRefPoint = new Point(
      refPoints[1].x - Utils.skPercent(outline.shuRadius, 120), // pieRadius
      refPoints[1].y - outline.hengRadius
    );

    OutlineHelpers.addPieTipOutline(outline, curveStartPoint, refPoints[2], bottomRefPoint, false);
  }
};
