var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addNortheastCornerPoint(outline, refPoints[1]);
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[2]);
    OutlineHelpers.addWanGouBottomOutline(outline, refPoints[2], refPoints[3], refPoints[4], true);
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[2]);
    OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[1]);
  }
};
