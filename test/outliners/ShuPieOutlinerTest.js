/* eslint-env mocha */
var assert = require('chai').assert;
var ShuPieOutliner = require('../../src/outliners/ShuPieOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuPieOutliner#addPoints', function() {
  it('should return the same output as GetShuPieOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1296, 1950),
      new Point(1296, 1490),
      new Point(336, 480)
    ];
    var outline = new Outline(32, 62, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1234, 2049, 1),
      new OutlinePoint(1296, 2049, 2),
      new OutlinePoint(1358, 2012, 2),
      new OutlinePoint(1439, 1950, 1),
      new OutlinePoint(1358, 1897, 1),
      new OutlinePoint(1358, 1428, 1),
      new OutlinePoint(1358, 855, 2),
      new OutlinePoint(1169, 689, 2),
      new OutlinePoint(352, 464, 1),
      new OutlinePoint(336, 480, 1),
      new OutlinePoint(1065, 731, 2),
      new OutlinePoint(1234, 915, 2),
      new OutlinePoint(1234, 1552, 1)
    ];

    ShuPieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
