var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var startAndEnd = OutlineHelpers.addPieTopOutline(outline, refPoints, true);
    OutlineHelpers.addPieTipOutline(outline, startAndEnd.start, refPoints[1], startAndEnd.end, true);
  }
};
