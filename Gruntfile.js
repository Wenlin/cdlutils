var webpack = require('webpack');

var year = new Date().getFullYear();
var banner = 'CdlUtils | Copyright (C) ' + year + ' Wenlin Institute, Inc. SPC. All Rights Reserved.\n'
	+ '\tThis JavaScript file is offered for licensing under the GNU Affero General Public License v.3:\n'
	+ '\t\thttps://www.gnu.org/licenses/agpl.html\n'
	+ '\tUse of the Wenlin CDL JavaScript API is subject to the Terms of Service:\n'
	+ '\t\thttps://wenlincdl.com/terms';

module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    webpack: {
      options: {
        plugins: [
          new webpack.BannerPlugin(banner)
        ],
        module: {
          loaders: [
            {
              test: /\.js$/,
              loader: 'eslint-loader',
            }
          ]
        },
        eslint: {  
          configFile: '.eslintrc'
        }
      },
      dist: {
        entry: './src/CdlUtils.js',
        output: {
          filename: 'dist/cdl-utils.js'
        }
      },
      test: {
        entry: 'mocha!./test.js',
        output: {
          filename: 'dist/test.js'
        }
      }
    },

    uglify: {
      options: {
        // uncomment the below to debug the minified JS
        // beautify: true,
        banner: '/*! ' + banner + ' */\n'
      },
      dist: {
        files: {
          'dist/cdl-utils.min.js': ['dist/cdl-utils.js']
        }
      }
    },

    clean: {
      pre: ['dist'],
      post: [
        'dist/*',
        '!dist/cdl-utils.js',
        '!dist/cdl-utils.min.js',
        '!dist/test.js',
      ]
    }
  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-webpack');

  grunt.registerTask('default', ['clean:pre', 'webpack:dist', 'webpack:test', 'uglify:dist', 'clean:post']);
};