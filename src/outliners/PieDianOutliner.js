var Utils = require('../Utils');
var OutlineHelpers = require('../OutlineHelpers');
var Point = require('../Point');
var OutlinePoint = require('../OutlinePoint');
var DianOutliner = require('./DianOutliner');

module.exports = {
  addPoints: function(outline, refPoints) {
    var rad = outline.shuRadius; // pieRadius

    /* First do the bottom, dian, because it's more complicated (rotation). */
    DianOutliner.addPoints(outline, refPoints.splice(1));

    /* The first and last points of dian are at its top; the first point
        is slightly to the right of the last. Adjust them to have the
        same y coordinate and to be further apart. They will be the bottom
        of the pie part of the stroke. */
    outline.getFirstOutlinePoint().x = outline.getLastOutlinePoint().x + rad; // move right a bit
    outline.getFirstOutlinePoint().y = outline.getLastOutlinePoint().y;

    var top = refPoints[0];
    var pieInsideBottom = outline.getFirstOutlinePoint();
    var pieOutsideBottom = outline.getLastOutlinePoint().toPoint();
    var pieOutsideTop = new Point(top.x - rad, top.y + rad);
    var pieInsideTop = new Point(top.x + Utils.skPercent(rad, 80), top.y - Utils.skPercent(rad, 60));

    /* Left side of pie */
    var ctrlPoints = OutlineHelpers.getPieControlPoints(pieOutsideTop, pieOutsideBottom, false);
    outline.addPoint(ctrlPoints.bottom, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints.top, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(pieOutsideTop, OutlinePoint.ON_CURVE);

    /* Top of pie */
    var serifPt = new Point(top.x + Utils.skPercent(rad, 140), top.y - Utils.intCast(rad / 4));
    OutlineHelpers.addNortheastConvexityPoints(outline, pieOutsideTop, serifPt);
    outline.addPoint(serifPt, OutlinePoint.ON_CURVE); // right serif

    /* Right side of pie */
    ctrlPoints = OutlineHelpers.getPieControlPoints(pieInsideTop, pieInsideBottom, false);
    outline.addPoint(pieInsideTop, OutlinePoint.ON_CURVE);
    outline.addPoint(ctrlPoints.top, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints.bottom, OutlinePoint.OFF_CURVE_CUBIC);
  }
};
