/* eslint-env mocha */
var assert = require('chai').assert;
var PieGouOutliner = require('../../src/outliners/PieGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('PieGouOutliner#addPoints', function() {
  it('should return the same output as GetPieGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(3752, 4024),
      new Point(1070, 904),
      new Point(632, 1684)
    ];
    var outline = new Outline(32, 118, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(3652, 4134, 1),
      new OutlinePoint(3786, 4104, 2),
      new OutlinePoint(3930, 4039, 2),
      new OutlinePoint(3982, 3974, 1),
      new OutlinePoint(3852, 3888, 1),
      new OutlinePoint(3343, 2433, 2),
      new OutlinePoint(2141, 1319, 2),
      new OutlinePoint(893, 786, 1),
      new OutlinePoint(893, 845, 2),
      new OutlinePoint(792, 1235, 2),
      new OutlinePoint(573, 1625, 1),
      new OutlinePoint(632, 1684, 1),
      new OutlinePoint(851, 1294, 2),
      new OutlinePoint(960, 1022, 2),
      new OutlinePoint(1070, 1022, 1),
      new OutlinePoint(2159, 1556, 2),
      new OutlinePoint(3208, 2675, 2)
    ];

    PieGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
