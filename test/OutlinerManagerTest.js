/* eslint-env mocha */
var assert = require('chai').assert;
var OutlinerManager = require('../src/OutlinerManager');
var SZPOutliner = require('../src/outliners/SZPOutliner');

describe('OutlinerManager', function() {
  describe('getOutlinerForStroke', function() {
    it('should return the correct renderer for a given stroke name', function() {
      assert.equal(OutlinerManager.getOutlinerForStroke('szp'), SZPOutliner);
    });
  });
});
