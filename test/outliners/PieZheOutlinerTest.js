/* eslint-env mocha */
var assert = require('chai').assert;
var PieZheOutliner = require('../../src/outliners/PieZheOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('PieZheOutliner#addPoints', function() {
  it('should return the same output as GetPieZheOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1264, 2696),
      new Point(683, 1384),
      new Point(1886, 1557)
    ];
    var outline = new Outline(32, 111, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1169, 2801, 1),
      new OutlinePoint(1296, 2772, 2),
      new OutlinePoint(1433, 2710, 2),
      new OutlinePoint(1483, 2649, 1),
      new OutlinePoint(1359, 2567, 1),
      new OutlinePoint(1252, 2064, 2),
      new OutlinePoint(999, 1679, 2),
      new OutlinePoint(738, 1495, 1),
      new OutlinePoint(1886, 1589, 1),
      new OutlinePoint(1886, 1525, 1),
      new OutlinePoint(794, 1328, 1),
      new OutlinePoint(683, 1273, 1),
      new OutlinePoint(528, 1468, 1),
      new OutlinePoint(566, 1468, 2),
      new OutlinePoint(644, 1509, 2),
      new OutlinePoint(683, 1550, 1),
      new OutlinePoint(888, 1765, 2),
      new OutlinePoint(1085, 2214, 2)
    ];

    PieZheOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
