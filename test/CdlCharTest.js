/* eslint-env mocha */
var chai = require('chai');
var assert = chai.assert;

var CdlChar = require('../src/CdlChar');
var CdlStroke = require('../src/CdlStroke');
var Point = require('../src/Point');

describe('CdlChar', function() {
  describe('toXml', function() {
    it('should output CDL compatible xml', function() {
      var expectedXml = '' +
      "<cdl char='人' uni='4EBA'>\n" +
      "\t<stroke type='p' points='60,0 0,128' tail='long' />\n" +
      "\t<stroke type='n' points='54,32 128,128' head='cut' />\n" +
      '</cdl>';

      var char = new CdlChar('人', '4EBA', [
        new CdlStroke('p', [
          new Point(60, 0),
          new Point(0, 128)
        ], CdlStroke.STROKE_HEAD_NORMAL, CdlStroke.STROKE_TAIL_LONG),
        new CdlStroke('n', [
          new Point(54, 32),
          new Point(128, 128)
        ], CdlStroke.STROKE_HEAD_CUT,  CdlStroke.STROKE_TAIL_NORMAL)
      ]);

      assert.equal(char.toXml(), expectedXml);
    });

    var compoundChar = new CdlChar('仁', '4EC1', [
      new CdlChar('亻', '4EBB',
        [
          new CdlStroke('p', [
            new Point(128, 0),
            new Point(0, 64)
          ]),
          new CdlStroke('s', [
            new Point(86, 46),
            new Point(86, 128)
          ])
        ],
        [
          new Point(0, 0),
          new Point(34, 128)
        ]
      ),
      new CdlChar('二', '4E8C',
        [
          new CdlStroke('h', [
            new Point(16, 0),
            new Point(112, 0)
          ]),
          new CdlStroke('h', [
            new Point(0, 128),
            new Point(128, 128)
          ])
        ],
        [
          new Point(46, 20),
          new Point(128, 112)
        ]
      , 'K1D1')
    ]);

    it('should not output recursive xml by default', function() {
      var expectedXml = '' +
      "<cdl char='仁' uni='4EC1'>\n" +
      "\t<comp char='亻' uni='4EBB' points='0,0 34,128' />\n" +
      "\t<comp char='二' uni='4E8C' points='46,20 128,112' />\n" +
      '</cdl>';

      assert.equal(compoundChar.toXml(), expectedXml);
    });

    it('should output recursive xml if specified', function() {
      var expectedXml = '' +
      "<cdl char='仁' uni='4EC1'>\n" +
      "\t<comp char='亻' uni='4EBB' points='0,0 34,128'>\n" +
      "\t\t<stroke type='p' points='128,0 0,64' />\n" +
      "\t\t<stroke type='s' points='86,46 86,128' />\n" +
      '\t</comp>\n' +
      "\t<comp char='二' uni='4E8C' points='46,20 128,112' radical='K1D1'>\n" +
      "\t\t<stroke type='h' points='16,0 112,0' />\n" +
      "\t\t<stroke type='h' points='0,128 128,128' />\n" +
      '\t</comp>\n' +
      '</cdl>';

      assert.equal(compoundChar.toXml({ recursive: true }), expectedXml);
    });
  });
});
