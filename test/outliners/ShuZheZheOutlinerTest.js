/* eslint-env mocha */
var assert = require('chai').assert;
var ShuZheZheOutliner = require('../../src/outliners/ShuZheZheOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuZheZheOutliner#addPoints', function() {
  it('should return the same output as GetShuZheZheOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1774, 4128),
      new Point(1774, 2925),
      new Point(2505, 2925),
      new Point(2505, 1684)
    ];
    var outline = new Outline(32, 99, 0, 1);

    var expectedOutlinePoints = [
      new OutlinePoint(1675, 4287, 1),
      new OutlinePoint(1774, 4287, 2),
      new OutlinePoint(1873, 4227, 2),
      new OutlinePoint(2002, 4128, 1),
      new OutlinePoint(1873, 4043, 1),
      new OutlinePoint(1873, 2957, 1),
      new OutlinePoint(2362, 2957, 1),
      new OutlinePoint(2495, 3123, 1),
      new OutlinePoint(2588, 3090, 2),
      new OutlinePoint(2689, 3018, 2),
      new OutlinePoint(2725, 2947, 1),
      new OutlinePoint(2604, 2826, 1),
      new OutlinePoint(2604, 1684, 1),
      new OutlinePoint(2406, 1684, 1),
      new OutlinePoint(2406, 2893, 1),
      new OutlinePoint(1873, 2893, 1),
      new OutlinePoint(1774, 2765, 1),
      new OutlinePoint(1691, 2796, 2),
      new OutlinePoint(1625, 2850, 2),
      new OutlinePoint(1576, 2925, 1),
      new OutlinePoint(1675, 3024, 1)
    ];

    ShuZheZheOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
