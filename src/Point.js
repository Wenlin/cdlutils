var Point = function(x, y) {
  this.x = x;
  this.y = y;
};

// ----- getters -----

Point.prototype.clone = function() { return new Point(this.x, this.y); };

// ----- mutators -----

Point.swapXs = function(point1, point2) {
  var tmp = point1.x;
  point1.x = point2.x;
  point2.x = tmp;
};

Point.swapYs = function(point1, point2) {
  var tmp = point1.y;
  point1.y = point2.y;
  point2.y = tmp;
};

module.exports = Point;
