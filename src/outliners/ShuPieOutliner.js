var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var rad = outline.shuRadius;
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);

    var curveStartPoint = new Point(refPoints[1].x + rad, refPoints[1].y - rad);
    var curveEndPoint = new Point(refPoints[1].x - rad, refPoints[1].y + rad);

    OutlineHelpers.addPieTipOutline(outline, curveStartPoint, refPoints[2], curveEndPoint, true);
  }
};
