var Utils = require('../Utils');
var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    if (refPoints[0].y === refPoints[1].y) {
      OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
      OutlineHelpers.addHengRightEndOutlinePoints(outline, refPoints[0], refPoints[1]);
    } else {
      /* Not exactly horizontal; rotated counter-clockwise */
      var dx = refPoints[1].x - refPoints[0].x;
      var dy = refPoints[1].y - refPoints[0].y;
      var hypotenuse = Utils.hypotenuse(dx, dy);

      /* First make all points relative to refPoints[0] */
      var flatPoints = [
        new Point(0, 0),
        new Point(hypotenuse, 0)
      ];

      OutlineHelpers.addHengLeftEndOutlinePoints(outline, flatPoints[0]);
      OutlineHelpers.addHengRightEndOutlinePoints(outline, flatPoints[0], flatPoints[1]);

      /* Now rotate around origin, and add refPoints[0] */
      outline.rotateAndTranslate(refPoints[0], dx, dy, hypotenuse);
    }
  }
};
