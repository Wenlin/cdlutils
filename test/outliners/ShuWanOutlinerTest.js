/* eslint-env mocha */
var assert = require('chai').assert;
var ShuWanOutliner = require('../../src/outliners/ShuWanOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuWanOutliner#addPoints', function() {
  it('should return the same output as GetShuWanOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2504, 3920),
      new Point(2504, 2623),
      new Point(2972, 2009),
      new Point(3414, 2009)
    ];
    var outline = new Outline(32, 111, 1, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2393, 3920, 1),
      new OutlinePoint(2615, 3920, 1),
      new OutlinePoint(2615, 2623, 1),
      new OutlinePoint(2615, 2340, 2),
      new OutlinePoint(2775, 2109, 2),
      new OutlinePoint(2972, 2109, 1),
      new OutlinePoint(3314, 2109, 2),
      new OutlinePoint(3514, 2209, 2),
      new OutlinePoint(3514, 2009, 1),
      new OutlinePoint(3514, 1959, 2),
      new OutlinePoint(3464, 1909, 2),
      new OutlinePoint(3314, 1909, 1),
      new OutlinePoint(2972, 1909, 1),
      new OutlinePoint(2653, 1909, 2),
      new OutlinePoint(2393, 2230, 2),
      new OutlinePoint(2393, 2623, 1)
    ];

    ShuWanOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
