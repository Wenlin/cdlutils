/* eslint-env mocha */
var assert = require('chai').assert;
var HZWGOutliner = require('../../src/outliners/HZWGOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HZWGOutliner#addPoints', function() {
  it('should return the same output as GetHZWGOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(892, 2122),
      new Point(2816, 2122),
      new Point(2816, 1220),
      new Point(3362, 920),
      new Point(3856, 920),
      new Point(3856, 1371)
    ];
    var outline = new Outline(32, 110, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(988, 2090, 1),
      new OutlinePoint(892, 2034, 1),
      new OutlinePoint(828, 2154, 1),
      new OutlinePoint(2657, 2154, 1),
      new OutlinePoint(2805, 2342, 1),
      new OutlinePoint(2908, 2305, 2),
      new OutlinePoint(3020, 2225, 2),
      new OutlinePoint(3060, 2146, 1),
      new OutlinePoint(2926, 2012, 1),
      new OutlinePoint(2926, 1220, 1),
      new OutlinePoint(2926, 1109, 2),
      new OutlinePoint(3122, 1019, 2),
      new OutlinePoint(3362, 1019, 1),
      new OutlinePoint(3619, 1019, 1),
      new OutlinePoint(3737, 1019, 2),
      new OutlinePoint(3807, 1032, 2),
      new OutlinePoint(3856, 1371, 1),
      new OutlinePoint(3880, 1395, 1),
      new OutlinePoint(3905, 1144, 2),
      new OutlinePoint(4054, 1069, 2),
      new OutlinePoint(4054, 969, 1),
      new OutlinePoint(4054, 871, 2),
      new OutlinePoint(3955, 821, 2),
      new OutlinePoint(3757, 821, 1),
      new OutlinePoint(3362, 821, 1),
      new OutlinePoint(3001, 821, 2),
      new OutlinePoint(2706, 1000, 2),
      new OutlinePoint(2706, 1220, 1),
      new OutlinePoint(2706, 2090, 1)
    ];

    HZWGOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetHZWGOutline in ch_zzsol.c case #2', function() {
    var refPoints = [
      new Point(875, 4024),
      new Point(2825, 4024),
      new Point(632, 1489),
      new Point(1436, 904),
      new Point(3752, 904),
      new Point(3752, 1976)
    ];
    var outline = new Outline(32, 120, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(971, 3992, 1),
      new OutlinePoint(875, 3936, 1),
      new OutlinePoint(811, 4056, 1),
      new OutlinePoint(2652, 4056, 1),
      new OutlinePoint(2813, 4264, 1),
      new OutlinePoint(2925, 4192, 2),
      new OutlinePoint(3047, 4036, 2),
      new OutlinePoint(3091, 3880, 1),
      new OutlinePoint(2945, 3796, 1),
      new OutlinePoint(1232, 2379, 1),
      new OutlinePoint(752, 1982, 2),
      new OutlinePoint(752, 1735, 2),
      new OutlinePoint(752, 1489, 1),
      new OutlinePoint(752, 1489, 1),
      new OutlinePoint(752, 1226, 2),
      new OutlinePoint(1059, 1012, 2),
      new OutlinePoint(1436, 1012, 1),
      new OutlinePoint(3493, 1012, 1),
      new OutlinePoint(3622, 1012, 2),
      new OutlinePoint(3698, 1172, 2),
      new OutlinePoint(3752, 1976, 1),
      new OutlinePoint(3779, 2003, 1),
      new OutlinePoint(3806, 1403, 2),
      new OutlinePoint(3968, 1212, 2),
      new OutlinePoint(3968, 958, 1),
      new OutlinePoint(3968, 850, 2),
      new OutlinePoint(3860, 796, 2),
      new OutlinePoint(3644, 796, 1),
      new OutlinePoint(1436, 796, 1),
      new OutlinePoint(927, 796, 2),
      new OutlinePoint(512, 1107, 2),
      new OutlinePoint(512, 1489, 1),
      new OutlinePoint(512, 1489, 1),
      new OutlinePoint(512, 1785, 2),
      new OutlinePoint(512, 2081, 2),
      new OutlinePoint(992, 2499, 1),
      new OutlinePoint(2705, 3992, 1)
    ];

    HZWGOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
