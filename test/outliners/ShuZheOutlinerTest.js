/* eslint-env mocha */
var assert = require('chai').assert;
var ShuZheOutliner = require('../../src/outliners/ShuZheOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuZheOutliner#addPoints', function() {
  it('should return the same output as GetShuZheOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1865, 2857),
      new Point(1865, 904),
      new Point(3734, 904)
    ];
    var outline = new Outline(32, 108, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1757, 3030, 1),
      new OutlinePoint(1865, 3030, 2),
      new OutlinePoint(1973, 2965, 2),
      new OutlinePoint(2113, 2857, 1),
      new OutlinePoint(1973, 2764, 1),
      new OutlinePoint(1973, 936, 1),
      new OutlinePoint(3410, 936, 1),
      new OutlinePoint(3550, 1122, 1),
      new OutlinePoint(3705, 1059, 2),
      new OutlinePoint(3805, 936, 2),
      new OutlinePoint(3842, 872, 1),
      new OutlinePoint(1973, 872, 1),
      new OutlinePoint(1865, 729, 1),
      new OutlinePoint(1774, 764, 2),
      new OutlinePoint(1703, 822, 2),
      new OutlinePoint(1649, 904, 1),
      new OutlinePoint(1757, 1012, 1)
    ];

    ShuZheOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
