/* eslint-env mocha */
var assert = require('chai').assert;
var HengOutliner = require('../../src/outliners/HengOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengOutliner#addPoints', function() {
  it('should return the same output as GetHengOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(528, 3400), new Point(3856, 3400)];
    var outline = new Outline(32, 115, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(624, 3368, 1),
      new OutlinePoint(528, 3312, 1),
      new OutlinePoint(464, 3432, 1),
      new OutlinePoint(3511, 3432, 1),
      new OutlinePoint(3660, 3630, 1),
      new OutlinePoint(3825, 3563, 2),
      new OutlinePoint(3931, 3432, 2),
      new OutlinePoint(3971, 3368, 1)
    ];

    HengOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
