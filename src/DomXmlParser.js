/* There are 2 XML parser implementations: browser and node.js.
When javascript is running in the browser it can use the browser's built-in XML parser,
which is not available in node.js.
See: http://stackoverflow.com/questions/7949752/cross-browser-javascript-xml-parsing
*/
var parseXml;
if (global.window) {
  if (typeof window.DOMParser !== 'undefined') {
    parseXml = function(xmlStr) {
      return ( new window.DOMParser() ).parseFromString(xmlStr, 'text/xml');
    };
  } else if (typeof window.ActiveXObject !== 'undefined' &&
       new window.ActiveXObject('Microsoft.XMLDOM')) {
    parseXml = function(xmlStr) {
      var xmlDoc = new window.ActiveXObject('Microsoft.XMLDOM');
      xmlDoc.async = 'false';
      xmlDoc.loadXML(xmlStr);
      return xmlDoc;
    };
  } else {
    throw new Error('No XML parser found');
  }
} else {
  parseXml = function() {
    throw new Error('DOM XML parsing not available in Node');
  };
}

/* --- getChildren:
    Get the child nodes of the given xml node, excluding text and comment nodes.

    ---param xmlNode.
    ---return the array of children.

    Can't just use xmlNode.children because IE :(
*/
var getChildren = function(xmlNode) {
  var children = [];
  for (var i = 0; i < xmlNode.childNodes.length; i++) {
    var child = xmlNode.childNodes[i];
    if (child.nodeName !== '#text' && child.nodeName !== '#comment') {
      children.push(child);
    }
  }
  return children;
};

/* --- recursiveTransform:
    Turn the output from the native DOM api into the same format as xml2js.
*/
var recursiveTransform = function(parsedXmlNode) {
  var transformedNode = {
    '#name': parsedXmlNode.nodeName,
    '$': {}
  };
  var attrs = parsedXmlNode.attributes;
  for (var i = 0; i < attrs.length; i++) {
    var attr = attrs[i];
    transformedNode.$[attr.name] = attr.value;
  }
  var children = getChildren(parsedXmlNode);
  if (children.length > 0) {
    transformedNode.$$ = [];
    for (i = 0; i < children.length; i++) {
      transformedNode.$$.push(recursiveTransform(children[i]));
    }
  }
  return transformedNode;
};

var DomXmlParser = {
  parseString: function(xml, xmlParsingOpts, callback) {
    try {
      var parsedXml = parseXml(xml).documentElement;
      callback(null, recursiveTransform(parsedXml));
    } catch (e) {
      callback(e);
    }
  }
};

module.exports = DomXmlParser;
