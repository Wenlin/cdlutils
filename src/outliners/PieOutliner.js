var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var startAndEnd = OutlineHelpers.addPieTopOutline(outline, refPoints, false);
    OutlineHelpers.addPieTipOutline(outline, startAndEnd.start, refPoints[1], startAndEnd.end, false);
  }
};
