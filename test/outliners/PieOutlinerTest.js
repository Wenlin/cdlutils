/* eslint-env mocha */
var assert = require('chai').assert;
var PieOutliner = require('../../src/outliners/PieOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('PieOutliner#addPoints', function() {
  it('should return the same output as GetPieOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(3076, 3400), new Point(528, 800)];
    var outline = new Outline(32, 115, 1, 2);

    var expectedOutlinePoints = [
      new OutlinePoint(3208, 3400, 1),
      new OutlinePoint(2730, 2119, 2),
      new OutlinePoint(1601, 1138, 2),
      new OutlinePoint(429, 669, 1),
      new OutlinePoint(413, 685, 1),
      new OutlinePoint(1480, 1151, 2),
      new OutlinePoint(2508, 2127, 2),
      new OutlinePoint(2944, 3400, 1)
    ];

    PieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetPieOutline in ch_zzsol.c case #2', function() {
    var refPoints = [new Point(1324, 3244), new Point(528, 1424)];
    var outline = new Outline(32, 108, 2, 2);

    var expectedOutlinePoints = [
      new OutlinePoint(1324, 3244, 1),
      new OutlinePoint(1324, 3060, 1),
      new OutlinePoint(1171, 2235, 2),
      new OutlinePoint(810, 1602, 2),
      new OutlinePoint(436, 1300, 1),
      new OutlinePoint(420, 1316, 1),
      new OutlinePoint(723, 1647, 2),
      new OutlinePoint(1016, 2340, 2),
      new OutlinePoint(1140, 3244, 1)
    ];

    PieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetPieOutline in ch_zzsol.c case #3', function() {
    var refPoints = [new Point(846, 1905), new Point(336, 1020)];
    var outline = new Outline(32, 67, 0, 2);

    var expectedOutlinePoints = [
      new OutlinePoint(789, 1968, 1),
      new OutlinePoint(865, 1950, 2),
      new OutlinePoint(947, 1913, 2),
      new OutlinePoint(977, 1877, 1),
      new OutlinePoint(903, 1828, 1),
      new OutlinePoint(903, 1828, 1),
      new OutlinePoint(796, 1410, 2),
      new OutlinePoint(545, 1090, 2),
      new OutlinePoint(285, 937, 1),
      new OutlinePoint(269, 953, 1),
      new OutlinePoint(488, 1127, 2),
      new OutlinePoint(699, 1492, 2),
      new OutlinePoint(789, 1968, 1)
    ];

    PieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
