/* eslint-env mocha */
var assert = require('chai').assert;
var XieGouOutliner = require('../../src/outliners/XieGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('XieGouOutliner#addPoints', function() {
  it('should return the same output as GetXieGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2400, 4128),
      new Point(3856, 800),
      new Point(3856, 1632)
    ];
    var outline = new Outline(32, 110, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2290, 4304, 1),
      new OutlinePoint(2400, 4304, 2),
      new OutlinePoint(2510, 4238, 2),
      new OutlinePoint(2653, 4128, 1),
      new OutlinePoint(2510, 4033, 1),
      new OutlinePoint(2510, 2185, 2),
      new OutlinePoint(2739, 1650, 2),
      new OutlinePoint(3735, 921, 1),
      new OutlinePoint(3886, 1662, 1),
      new OutlinePoint(3916, 1632, 1),
      new OutlinePoint(3886, 1216, 2),
      new OutlinePoint(3977, 1008, 2),
      new OutlinePoint(3977, 800, 1),
      new OutlinePoint(3977, 709, 2),
      new OutlinePoint(3916, 679, 2),
      new OutlinePoint(3856, 679, 1),
      new OutlinePoint(3774, 679, 2),
      new OutlinePoint(3692, 679, 2),
      new OutlinePoint(3614, 740, 1),
      new OutlinePoint(2538, 1575, 2),
      new OutlinePoint(2290, 2187, 2)
    ];

    XieGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
