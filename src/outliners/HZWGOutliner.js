var Utils = require('../Utils');
var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    if (refPoints[2].x === refPoints[1].x) {
      OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[1]);
      OutlineHelpers.addSWGInsidePoints(outline, refPoints[2], refPoints[3]);
      OutlineHelpers.addSWGRightEndPoints(outline, refPoints[4], refPoints[5]);
      OutlineHelpers.addSWGOutsidePoints(outline, refPoints[2], refPoints[3]);
      OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[1]);
    } else {
      OutlineHelpers.addHengPieOutlineCornerPoints(outline, refPoints[1]);

      /* This point is interpolated. */
      /* '10,0 90,0 [20,62] 0,104 33,128 128,128 128,84' */
      var interp = new Point(
          Utils.skPercent(refPoints[1].x, 22) + Utils.skPercent(refPoints[2].x, 78),
          Utils.skPercent(refPoints[1].y, 40) + Utils.skPercent(refPoints[2].y, 60)
      );
      OutlineHelpers.addHZWGYOutline(outline, interp, refPoints);
    }
  }
};
