var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[1]);
    OutlineHelpers.addNortheastCornerPoint(outline, refPoints[2]);
    OutlineHelpers.addHengPieOutlineCornerPoints(outline, refPoints[3]);
    var curveStartPoint = outline.getLastOutlinePoint().toPoint();
    outline.deleteLastPoint(); // don't add curveStartPoint twice!

    var bottomRefPoint = new Point(
      refPoints[3].x - outline.shuRadius,
      refPoints[3].y - outline.hengRadius
    );
    OutlineHelpers.addPieTipOutline(outline, curveStartPoint, refPoints[4], bottomRefPoint, false);
    OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[2]);
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[1]);
  }
};
