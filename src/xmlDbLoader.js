var Utils = require('./Utils');

var xmlDbLoader = function(cdlDbXml) {
  var parsedDb;

  return function(unicode, variant, xmlParser) {
    return Promise.resolve().then(function() {
      if (!parsedDb) {
        return Utils.parseXml(cdlDbXml, xmlParser).then(function(parsedXml) {
          parsedDb = {};
          parsedXml.$$.forEach(function(cdlDef) {
            var uniAttr = cdlDef.$.uni;
            var variantAttr = cdlDef.$.variant || 0;
            parsedDb[uniAttr + ':' + variantAttr] = cdlDef;
          });
        });
      }
    }).then(function() {
      return parsedDb[unicode + ':' + (variant || 0)];
    });
  };
};

module.exports = xmlDbLoader;
