var RenderBounds = require('./RenderBounds');
var OutlinerManager = require('./OutlinerManager');
var Outline = require('./Outline');
var PlainSvgRenderer = require('./PlainSvgRenderer');
var SongSvgRenderer = require('./SongSvgRenderer');
var Point = require('./Point');
var Utils = require('./Utils');

var MAX_SHU_RADIUS = 82;
var MAX_HENG_RADIUS = 50;
var DEFAULT_SHU_RADIUS = 37;
var DEFAULT_HENG_RADIUS = 13;

var FT_HALF_PIXEL = 32;
var FT_QUARTER_PIXEL = 16;
var FT_SHIFT_SIX = 6;

var convertOlX = function(x) {
  return Utils.intCast(x + FT_QUARTER_PIXEL);
};
var convertOlY = function(yy, flipHeight) {
  return Utils.intCast(flipHeight - yy + FT_HALF_PIXEL);
};

var getPaddedBounds = function(renderBounds, shuRadius) {
  var margin = 2 * shuRadius;
  return new RenderBounds(
    renderBounds.x + margin,
    renderBounds.y + margin,
    renderBounds.width - 2 * margin,
    renderBounds.height - 2 * margin
  );
};

var scaleBounds = function(bounds, boundingPoints) {
  var pointA = boundingPoints[0].clone();
  var pointB = boundingPoints[1].clone();

  if (pointA.x > pointB.x) { // compare RegularizeRectangle()
    Point.swapXs(pointA, pointB);
  }
  if (pointA.y > pointB.y) {
    Point.swapYs(pointA, pointB);
  }

  var scaledX = bounds.x + Utils.xyScaleMul(pointA.x, bounds.width);
  var scaledY = bounds.y + Utils.xyScaleMul(pointA.y, bounds.height);
  return new RenderBounds(
    scaledX,
    scaledY,
    bounds.x + Utils.xyScaleMul(pointB.x, bounds.width) - scaledX,
    bounds.y + Utils.xyScaleMul(pointB.y, bounds.height) - scaledY
  );
};

var renderPathStrings = function(cdlChar, radii, bounds, origBounds, rawBounds, options) {
  var pathStrings = [];
  cdlChar.components.forEach(function(component) {
    if (component.isStroke) {
      var scaledPoints = component.points.map(function(point) {
        return new Point(
          bounds.x + Utils.xyScaleMul(point.x, bounds.width),
          bounds.y + Utils.xyScaleMul(point.y, bounds.height)
        );
      });
      if (options.font === 'plain') {
        pathStrings.push(PlainSvgRenderer.getPathString(component.type, scaledPoints));
      } else {
        var strokeOutline = CharacterRenderer.renderStrokeOutline(component, radii, bounds, origBounds, scaledPoints);
        pathStrings.push(SongSvgRenderer.getPathString(strokeOutline, rawBounds.y, rawBounds.height));
      }
    } else { // component, not stroke
      var scaledBounds = scaleBounds(bounds, component.points);
      pathStrings = pathStrings.concat(renderPathStrings(component, radii, scaledBounds, origBounds, rawBounds, options));
    }
  });

  if (options.endStrokeIndex) {
    pathStrings = pathStrings.slice(0, options.endStrokeIndex);
  }
  if (options.startStrokeIndex) {
    pathStrings = pathStrings.slice(options.startStrokeIndex);
  }
  return pathStrings;
};

var getOutlinePoints = function(strokeName, outline, refPoints) {
  var outliner = OutlinerManager.getOutlinerForStroke(strokeName);
  outliner.addPoints(outline, refPoints);
  return outline.outlinePoints;
};

var HALFSIZE_NSTROKES = 35; /* Somewhere between 30 and 40 strokes */
var MAX_STROKES_FOR_PERCENT = 50; /* <= 2 * HALFSIZE_NSTROKES */
/* Use static array to hold values so only need to calculate once. */
var percentArray = [];
for (var i = 0, j = 0; i < MAX_STROKES_FOR_PERCENT; i++, j += 50) {
  percentArray[i] = 100 - Utils.intCast(j / HALFSIZE_NSTROKES);
}

var getPercentThicknessFromStrokeCount = function(strokeCount) {
  strokeCount = Math.min(MAX_STROKES_FOR_PERCENT, strokeCount);
  return percentArray[strokeCount - 1];
};

var getBaseRadius = function(radiusFactor, glyphWidth, percent) {
  /* FT_SHIFT_SIX is used here to convert to FT_F26Dot6 fractional pixels. */
  /* The division by 1024 corresponds to the multiplication by radiusFactor,
      since radiusFactors are measured in increments of (glyph_width / 1024). */
  /* (x / 1024 == x >> 10) and (x << FT_SHIFT_SIX == x << 6); net effect is (x >> 4 == x / 16) */
  return Utils.intCast((((glyphWidth * radiusFactor) >> (10 - FT_SHIFT_SIX)) * percent) / 100);
};


var CharacterRenderer = {
  // just in the public API so it can be tested explicitly
  calculateStrokeRadii: function(hengRadiusFactor, shuRadiusFactor, glyphWidth, strokeCount) {
    var percent = getPercentThicknessFromStrokeCount(strokeCount);

    var hengRadius = getBaseRadius(hengRadiusFactor, glyphWidth, percent) & 0xffffffc0; // don't want fractional pixels...;
    var shuRadius = getBaseRadius(shuRadiusFactor, glyphWidth, percent);

    return {
      heng: Math.max(FT_HALF_PIXEL, hengRadius), // not less than half a pixel
      shu: Math.max(FT_HALF_PIXEL, shuRadius)
    };
  },

  // just in the public API so it can be tested explicitly
  renderStrokeOutline: function(stroke, radii, bounds, origBounds, points) {
    var outline = new Outline(
      radii.heng,
      radii.shu,
      stroke.head,
      stroke.tail
    );

    var flipHeight = origBounds.y * 2 + origBounds.height;
    var refPoints = points.map(function(point) {
      return new Point(
        convertOlX(point.x),
        convertOlY(point.y, flipHeight)
      );
    });
    return getOutlinePoints(stroke.type, outline, refPoints);
  },
  // options is a map with the following optional keys:
  //
  // startStrokeIndex: the stroke num (0-indexed) to begin with (default 0)
  // endStrokeIndex: the stroke num (0-indexed) to end with (default last stroke in char)
  // fixMargin: bool to fix the margins (based on stroke count/thickness) (default true)
  // hengRadiusFactor: int (default 13)
  // shuRadiusFactor: int (default 37)
  getSvgPathStrings: function(cdlChar, renderBounds, options) {
    if (!options) options = {};

    var hengRadiusFactor = Math.min(options.hengRadius || DEFAULT_HENG_RADIUS, MAX_HENG_RADIUS);
    var shuRadiusFactor = Math.min(options.shuRadius || DEFAULT_SHU_RADIUS, MAX_SHU_RADIUS);
    var glyphWidth = renderBounds.width;
    var strokeCount = cdlChar.getNumStrokes();
    var radii = CharacterRenderer.calculateStrokeRadii(hengRadiusFactor, shuRadiusFactor, glyphWidth, strokeCount);

    var highPrecisionBounds = new RenderBounds(
      renderBounds.x * 64,
      renderBounds.y * 64,
      renderBounds.width * 64,
      renderBounds.height * 64
    );

    if (options.fixMargin !== false) {
      highPrecisionBounds = getPaddedBounds(highPrecisionBounds, radii.shu);
    }

    var origBounds = highPrecisionBounds;
    var scaledBounds = highPrecisionBounds;
    if (cdlChar.isScaled /* TODO: add back ZinIsPUACJK stuff if needed */) {
      scaledBounds = scaleBounds(highPrecisionBounds, cdlChar.points);
    }

    return renderPathStrings(cdlChar, radii, scaledBounds, origBounds, renderBounds, options);
  }
};

module.exports = CharacterRenderer;
