var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addNortheastCornerPoint(outline, refPoints[1]);
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[2]);
    OutlineHelpers.addShuBottomOutlinePoints(outline, refPoints[3]);
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[2]);
    OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[1]);
  }
};
