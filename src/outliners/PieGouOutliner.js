var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var curvePoints = OutlineHelpers.addPieTopOutline(outline, refPoints, false); // five points
    OutlineHelpers.addPieGouBottomOutline(outline, curvePoints.start, refPoints[1], refPoints[2], curvePoints.end);
  }
};
