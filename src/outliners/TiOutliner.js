var Utils = require('../Utils');
var OutlinePoint = require('../OutlinePoint');

module.exports = {
  addPoints: function(outline, refPoints) {
    var rad = outline.shuRadius;

    outline.addPoint(refPoints[1], OutlinePoint.ON_CURVE);
    outline.addPoint(refPoints[1].x,
      refPoints[1].y - Utils.intCast(rad / 2), OutlinePoint.ON_CURVE);

    if (refPoints[1].x - refPoints[0].x < refPoints[1].y - refPoints[0].y) {
      // more vertical, as in san-dian-shui. Make it rounded.
      outline.addPoint(refPoints[0].x + rad,
          refPoints[0].y - Utils.skPercent(rad, 70), OutlinePoint.ON_CURVE);
      outline.addPoint(refPoints[0].x + Utils.skPercent(rad, 20),
          refPoints[0].y - rad, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(refPoints[0].x - Utils.skPercent(rad, 90),
          refPoints[0].y - Utils.skPercent(rad, 80), OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(refPoints[0].x - Utils.skPercent(rad, 170),
          refPoints[0].y + Utils.skPercent(rad, 190), OutlinePoint.ON_CURVE);
      outline.addPoint(refPoints[0].x - rad,
          refPoints[0].y + Utils.skPercent(rad, 150), OutlinePoint.ON_CURVE);
    } else { // more horizontal.
      outline.addPoint(refPoints[0].x,
          refPoints[0].y - rad, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoints[0].x - Utils.skPercent(rad, 10),
          refPoints[0].y - Utils.skPercent(rad, 90), OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(refPoints[0].x - Utils.skPercent(rad, 90),
          refPoints[0].y - Utils.skPercent(rad, 50), OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(refPoints[0].x - Utils.skPercent(rad, 140),
          refPoints[0].y + Utils.skPercent(rad, 170), OutlinePoint.ON_CURVE);
      outline.addPoint(refPoints[0].x - Utils.skPercent(rad, 70),
          refPoints[0].y + Utils.skPercent(rad, 150), OutlinePoint.ON_CURVE);
    }
  }
};
