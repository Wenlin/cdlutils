/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheTiOutliner = require('../../src/outliners/HengZheTiOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheTiOutliner#addPoints', function() {
  it('should return the same output as GetHengZheTiOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(336, 1668),
      new Point(654, 1668),
      new Point(654, 600),
      new Point(1004, 937)
    ];
    var outline = new Outline(32, 63, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(432, 1636, 1),
      new OutlinePoint(336, 1580, 1),
      new OutlinePoint(272, 1700, 1),
      new OutlinePoint(563, 1700, 1),
      new OutlinePoint(648, 1794, 1),
      new OutlinePoint(707, 1773, 2),
      new OutlinePoint(771, 1727, 2),
      new OutlinePoint(794, 1682, 1),
      new OutlinePoint(717, 1605, 1),
      new OutlinePoint(717, 751, 1),
      new OutlinePoint(989, 952, 1),
      new OutlinePoint(1004, 937, 1),
      new OutlinePoint(717, 568, 1),
      new OutlinePoint(654, 498, 1),
      new OutlinePoint(601, 518, 2),
      new OutlinePoint(559, 552, 2),
      new OutlinePoint(528, 600, 1),
      new OutlinePoint(591, 663, 1),
      new OutlinePoint(591, 1636, 1)
    ];

    HengZheTiOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
