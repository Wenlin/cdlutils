/* eslint-env mocha */
var assert = require('chai').assert;
var ShuWanGouOutliner = require('../../src/outliners/ShuWanGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuWanGouOutliner#addPoints', function() {
  it('should return the same output as GetShuWanGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2556, 4128),
      new Point(2556, 2276),
      new Point(2892, 1659),
      new Point(3752, 1659),
      new Point(3752, 2469)
    ];
    var outline = new Outline(32, 103, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2453, 4293, 1),
      new OutlinePoint(2556, 4293, 2),
      new OutlinePoint(2659, 4231, 2),
      new OutlinePoint(2793, 4128, 1),
      new OutlinePoint(2659, 4039, 1),
      new OutlinePoint(2659, 2276, 1),
      new OutlinePoint(2659, 1987, 2),
      new OutlinePoint(2764, 1752, 2),
      new OutlinePoint(2892, 1752, 1),
      new OutlinePoint(3529, 1752, 1),
      new OutlinePoint(3640, 1752, 2),
      new OutlinePoint(3706, 1861, 2),
      new OutlinePoint(3752, 2469, 1),
      new OutlinePoint(3775, 2492, 1),
      new OutlinePoint(3798, 2039, 2),
      new OutlinePoint(3938, 1896, 2),
      new OutlinePoint(3938, 1705, 1),
      new OutlinePoint(3938, 1613, 2),
      new OutlinePoint(3845, 1566, 2),
      new OutlinePoint(3659, 1566, 1),
      new OutlinePoint(2892, 1566, 1),
      new OutlinePoint(2650, 1566, 2),
      new OutlinePoint(2453, 1885, 2),
      new OutlinePoint(2453, 2276, 1)
    ];

    ShuWanGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
