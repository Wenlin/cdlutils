/* eslint-env mocha */
var assert = require('chai').assert;
var WanPieOutliner = require('../../src/outliners/WanPieOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('WanPieOutliner#addPoints', function() {
  it('should return the same output as GetWanPieOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(1296, 2400), new Point(336, 480)];
    var outline = new Outline(32, 67, 0, 2);

    var expectedOutlinePoints = [
      new OutlinePoint(1229, 2507, 1),
      new OutlinePoint(1296, 2507, 2),
      new OutlinePoint(1363, 2467, 2),
      new OutlinePoint(1450, 2400, 1),
      new OutlinePoint(1363, 2342, 1),
      new OutlinePoint(1363, 2342, 1),
      new OutlinePoint(1363, 1187, 2),
      new OutlinePoint(1160, 852, 2),
      new OutlinePoint(285, 397, 1),
      new OutlinePoint(269, 413, 1),
      new OutlinePoint(1049, 903, 2),
      new OutlinePoint(1229, 1263, 2),
      new OutlinePoint(1229, 2507, 1)
    ];

    WanPieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
