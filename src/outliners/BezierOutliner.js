var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');
var OutlinePoint = require('../OutlinePoint');

module.exports = {
  addPoints: function(outline, refPoints) {
    var seven = OutlineHelpers.getBezierSevenFromFour(refPoints);
    var tt = OutlineHelpers.getBezierSevenFromFour(seven);
    tt.pop();
    tt = tt.concat(OutlineHelpers.getBezierSevenFromFour(seven.slice(3)));
    var p = OutlineHelpers.getBezierSevenFromFour(tt);
    p.pop();
    p = p.concat(OutlineHelpers.getBezierSevenFromFour(tt.slice(3)));
    p.pop();
    p = p.concat(OutlineHelpers.getBezierSevenFromFour(tt.slice(6)));
    p.pop();
    p = p.concat(OutlineHelpers.getBezierSevenFromFour(tt.slice(9)));

    var edge = [];
    for (var i = 0; i < 25 - 3; i += 3) {
      var parallelPoints = OutlineHelpers.getTwoParallelPoints(p.slice(i), outline.shuRadius);
      edge[i] = parallelPoints[0];
      edge[i + 1] = parallelPoints[1];
      parallelPoints = OutlineHelpers.getTwoParallelPoints(p.slice(i + 2), outline.shuRadius);
      edge[i + 2] = parallelPoints[0];
      edge[i + 3] = parallelPoints[1];
    }
    for (i = 0; i < 25; i++) {
      outline.addPoint(edge[i], ((i % 3) === 0) ? OutlinePoint.ON_CURVE : OutlinePoint.OFF_CURVE_CUBIC);
    }
    /* For opposite edge, just reflect the first edge's points symmetrically
        across the center. */
    for (i = 25 - 1; i >= 0; i--) {
      var e = new Point(2 * p[i].x - edge[i].x, 2 * p[i].y - edge[i].y);
      outline.addPoint(e, ((i % 3) === 0) ? OutlinePoint.ON_CURVE : OutlinePoint.OFF_CURVE_CUBIC);
    }
  }
};
