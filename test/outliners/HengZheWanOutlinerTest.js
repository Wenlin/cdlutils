/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheWanOutliner = require('../../src/outliners/HengZheWanOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheWanOutliner#addPoints', function() {
  it('should return the same output as GetHengZheWanOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2175, 4024),
      new Point(3105, 4024),
      new Point(3105, 3354),
      new Point(3462, 3099),
      new Point(3856, 3099)
    ];
    var outline = new Outline(32, 110, 1, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2175, 3992, 1),
      new OutlinePoint(2175, 4056, 1),
      new OutlinePoint(2946, 4056, 1),
      new OutlinePoint(3094, 4244, 1),
      new OutlinePoint(3197, 4207, 2),
      new OutlinePoint(3309, 4127, 2),
      new OutlinePoint(3349, 4048, 1),
      new OutlinePoint(3215, 3914, 1),
      new OutlinePoint(3215, 3354, 1),
      new OutlinePoint(3215, 3268, 2),
      new OutlinePoint(3326, 3198, 2),
      new OutlinePoint(3462, 3198, 1),
      new OutlinePoint(3757, 3198, 2),
      new OutlinePoint(3955, 3297, 2),
      new OutlinePoint(3955, 3099, 1),
      new OutlinePoint(3955, 3050, 2),
      new OutlinePoint(3905, 3000, 2),
      new OutlinePoint(3757, 3000, 1),
      new OutlinePoint(3462, 3000, 1),
      new OutlinePoint(3205, 3000, 2),
      new OutlinePoint(2995, 3159, 2),
      new OutlinePoint(2995, 3354, 1),
      new OutlinePoint(2995, 3992, 1)
    ];

    HengZheWanOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
