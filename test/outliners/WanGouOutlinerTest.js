/* eslint-env mocha */
var assert = require('chai').assert;
var WanGouOutliner = require('../../src/outliners/WanGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('WanGouOutliner#addPoints', function() {
  it('should return the same output as GetWanGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(599, 3868),
      new Point(1672, 2568),
      new Point(1100, 800),
      new Point(796, 1164)
    ];
    var outline = new Outline(32, 114, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(599, 3868, 1),
      new OutlinePoint(1340, 3502, 2),
      new OutlinePoint(1786, 3055, 2),
      new OutlinePoint(1786, 2568, 1),
      new OutlinePoint(1786, 1852, 2),
      new OutlinePoint(1786, 1354, 2),
      new OutlinePoint(1665, 1099, 1),
      new OutlinePoint(1545, 844, 2),
      new OutlinePoint(1265, 686, 2),
      new OutlinePoint(1100, 686, 1),
      new OutlinePoint(986, 686, 1),
      new OutlinePoint(986, 743, 2),
      new OutlinePoint(872, 925, 2),
      new OutlinePoint(742, 1148, 1),
      new OutlinePoint(758, 1164, 1),
      new OutlinePoint(929, 982, 2),
      new OutlinePoint(957, 914, 2),
      new OutlinePoint(986, 914, 1),
      new OutlinePoint(1100, 914, 2),
      new OutlinePoint(1272, 1003, 2),
      new OutlinePoint(1415, 1207, 1),
      new OutlinePoint(1558, 1411, 2),
      new OutlinePoint(1558, 1909, 2),
      new OutlinePoint(1558, 2568, 1),
      new OutlinePoint(1558, 3045, 2),
      new OutlinePoint(1187, 3482, 2),
      new OutlinePoint(571, 3840, 1)
    ];

    WanGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
