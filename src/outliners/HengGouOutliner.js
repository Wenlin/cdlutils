var OutlineHelpers = require('../OutlineHelpers');
var HengZheOutliner = require('./HengZheOutliner');

module.exports = {
  addPoints: function(outline, refPoints) {
    if (outline.isTailCut()) {
      /* Note: the first stroke of ma3 'horse' (simplified) is not
          really heng gou, it's heng zhe; but, the vertical stroke
          is slanted, and in the original font it was treated as hg.
          We therefore pretend it is hg, but say the end is cut off
          so it won't be pointed. Same in wu3 'five'.
          Should just use "hz" for all such characters! */
      HengZheOutliner.addPoints(outline, refPoints);
      return;
    }
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addHengGouRightEndOutlinePoints(outline, refPoints[1], refPoints[2]);
  }
};
