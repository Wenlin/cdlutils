/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheZheZheGouOutliner = require('../../src/outliners/HengZheZheZheGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheZheZheGouOutliner#addPoints', function() {
  it('should return the same output as GetHengZheZheZheGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2108, 4024),
      new Point(3213, 4024),
      new Point(2080, 2865),
      new Point(3752, 2865),
      new Point(3355, 800),
      new Point(3142, 1253)
    ];
    var outline = new Outline(32, 111, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2204, 3992, 1),
      new OutlinePoint(2108, 3936, 1),
      new OutlinePoint(2044, 4056, 1),
      new OutlinePoint(3053, 4056, 1),
      new OutlinePoint(3202, 4246, 1),
      new OutlinePoint(3306, 4208, 2),
      new OutlinePoint(3418, 4128, 2),
      new OutlinePoint(3459, 4048, 1),
      new OutlinePoint(3324, 3913, 1),
      new OutlinePoint(2191, 2897, 1),
      new OutlinePoint(3592, 2897, 1),
      new OutlinePoint(3741, 3087, 1),
      new OutlinePoint(3845, 3049, 2),
      new OutlinePoint(3957, 2969, 2),
      new OutlinePoint(3998, 2889, 1),
      new OutlinePoint(3863, 2754, 1),
      new OutlinePoint(3863, 2031, 2),
      new OutlinePoint(3863, 1443, 2),
      new OutlinePoint(3785, 1152, 1),
      new OutlinePoint(3708, 861, 2),
      new OutlinePoint(3476, 689, 2),
      new OutlinePoint(3355, 689, 1),
      new OutlinePoint(3244, 689, 1),
      new OutlinePoint(3244, 744, 2),
      new OutlinePoint(3133, 971, 2),
      new OutlinePoint(3006, 1237, 1),
      new OutlinePoint(3022, 1253, 1),
      new OutlinePoint(3188, 1026, 2),
      new OutlinePoint(3216, 911, 2),
      new OutlinePoint(3244, 911, 1),
      new OutlinePoint(3343, 911, 2),
      new OutlinePoint(3442, 1016, 2),
      new OutlinePoint(3541, 1257, 1),
      new OutlinePoint(3641, 1498, 2),
      new OutlinePoint(3641, 2086, 2),
      new OutlinePoint(3641, 2833, 1),
      new OutlinePoint(2191, 2833, 1),
      new OutlinePoint(2080, 2685, 1),
      new OutlinePoint(1987, 2721, 2),
      new OutlinePoint(1913, 2780, 2),
      new OutlinePoint(1858, 2865, 1),
      new OutlinePoint(1969, 2976, 1),
      new OutlinePoint(3102, 3992, 1)
    ];

    HengZheZheZheGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
