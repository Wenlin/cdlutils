var chai = require('chai');
/* eslint-env mocha */
var assert = chai.assert;

var CdlParser = require('../src/CdlParser');
var CdlChar = require('../src/CdlChar');
var CdlStroke = require('../src/CdlStroke');
var Point = require('../src/Point');

describe('CdlParser#loadAndParse', function() {
  var xmlParsers = [{name: 'xml2js parser', parser: require('xml2js')}];
  if (typeof window !== 'undefined') {
    // only test DomXmlParser in the browser since node doesn't have DOM apis
    xmlParsers.push({name: 'DOM xml parser', parser: require('../src/DomXmlParser')});
  }

  xmlParsers.forEach(function(xmlParser) {
    it('should work with chars containing only strokes using ' + xmlParser.name, function(done) {
      var rawCDL = '' +
      "<cdl char='人' uni='4EBA'>" +
      "  <stroke type='p' points='60,0 0,128' tail='long' />" +
      "  <stroke type='n' points='54,32 128,128' head='cut' />" +
      '</cdl>';

      var expectedParsedResults = new CdlChar('人', '4EBA', [
        new CdlStroke('p', [
          new Point(60, 0),
          new Point(0, 128)
        ], CdlStroke.STROKE_HEAD_NORMAL, CdlStroke.STROKE_TAIL_LONG),
        new CdlStroke('n', [
          new Point(54, 32),
          new Point(128, 128)
        ], CdlStroke.STROKE_HEAD_CUT,  CdlStroke.STROKE_TAIL_NORMAL)
      ]);

      var parser = new CdlParser(function() {
        return rawCDL;
      }, xmlParser.parser);
      parser.loadAndParse('人').then(function(results) {
        assert.deepEqual(results, expectedParsedResults);
        done();
      }).catch(function(err) { done(err); });
    });

    it('should ignore comments and extraneous XML tags using ' + xmlParser.name, function(done) {
      var rawCDL = '' +
      "<cdl char='人' uni='4EBA'>" +
      "  <stroke type='p' points='60,0 0,128' tail='long' />" +
      "  <invalid nonsense='attribute' />" +
      '  <!-- it should ignore this too -->' +
      "  <stroke type='n' points='54,32 128,128' head='cut' />" +
      '</cdl>';

      var expectedParsedResults = new CdlChar('人', '4EBA', [
        new CdlStroke('p', [
          new Point(60, 0),
          new Point(0, 128)
        ], CdlStroke.STROKE_HEAD_NORMAL, CdlStroke.STROKE_TAIL_LONG),
        new CdlStroke('n', [
          new Point(54, 32),
          new Point(128, 128)
        ], CdlStroke.STROKE_HEAD_CUT,  CdlStroke.STROKE_TAIL_NORMAL)
      ]);

      var parser = new CdlParser(function() {
        return rawCDL;
      }, xmlParser.parser);
      parser.loadAndParse('人').then(function(results) {
        assert.deepEqual(results, expectedParsedResults);
        done();
      }).catch(function(err) { done(err); });
    });

    it('should load and resolve recursive char definitions using ' + xmlParser.name, function(done) {
      var mainCharCdl = '' +
      "<cdl char='仁' uni='4EC1'>" +
      "  <comp char='亻' uni='4EBB' points='0,0 34,128' />" +
      "  <comp char='二' uni='4E8C' points='46,20 128,112' />" +
      '</cdl>';

      var firstCompCdl = '' +
      "<cdl char='亻' uni='4EBB' points='0,0 56,128'>" +
      "  <stroke type='p' points='128,0 0,64' />" +
      "  <stroke type='s' points='86,46 86,128' />" +
      '</cdl>';

      var secondCompCdl = '' +
      "<cdl char='二' uni='4E8C' points='0,18 128,112' radical='K1D1'>" +
      "  <stroke type='h' points='16,0 112,0' />" +
      "  <stroke type='h' points='0,128 128,128' />" +
      '  <!-- 2012-07-01T00:04:16Z -->' +
      '</cdl>';

      var expectedParsedResults = new CdlChar('仁', '4EC1', [
        new CdlChar('亻', '4EBB',
          [
            new CdlStroke('p', [
              new Point(128, 0),
              new Point(0, 64)
            ]),
            new CdlStroke('s', [
              new Point(86, 46),
              new Point(86, 128)
            ])
          ],
          [
            new Point(0, 0),
            new Point(34, 128)
          ]
        ),
        new CdlChar('二', '4E8C',
          [
            new CdlStroke('h', [
              new Point(16, 0),
              new Point(112, 0)
            ]),
            new CdlStroke('h', [
              new Point(0, 128),
              new Point(128, 128)
            ])
          ],
          [
            new Point(46, 20),
            new Point(128, 112)
          ]
        , 'K1D1')
      ]);

      var parser = new CdlParser(function(unicode) {
        if (unicode === '4EC1') return mainCharCdl;
        if (unicode === '4EBB') return firstCompCdl;
        if (unicode === '4E8C') return secondCompCdl;
      }, xmlParser.parser);
      parser.loadAndParse('仁').then(function(results) {
        assert.deepEqual(results, expectedParsedResults);
        done();
      }).catch(function(err) { done(err); });
    });
  });
});
