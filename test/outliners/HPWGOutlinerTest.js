/* eslint-env mocha */
var assert = require('chai').assert;
var HPWGOutliner = require('../../src/outliners/HPWGOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HPWGOutliner#addPoints', function() {
  it('should return the same output as GetHPWGOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2764, 3920),
      new Point(3753, 3920),
      new Point(3122, 2993),
      new Point(3856, 2262),
      new Point(3395, 1580),
      new Point(3002, 1872)
    ];
    var outline = new Outline(32, 111, 1, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2764, 3888, 1),
      new OutlinePoint(2764, 3952, 1),
      new OutlinePoint(3593, 3952, 1),
      new OutlinePoint(3742, 4142, 1),
      new OutlinePoint(3846, 4075, 2),
      new OutlinePoint(3958, 3931, 2),
      new OutlinePoint(3999, 3787, 1),
      new OutlinePoint(3864, 3709, 1),
      new OutlinePoint(3233, 3025, 1),
      new OutlinePoint(3691, 2810, 2),
      new OutlinePoint(3967, 2548, 2),
      new OutlinePoint(3967, 2262, 1),
      new OutlinePoint(3967, 1979, 2),
      new OutlinePoint(3967, 1807, 2),
      new OutlinePoint(3873, 1687, 1),
      new OutlinePoint(3780, 1567, 2),
      new OutlinePoint(3532, 1469, 2),
      new OutlinePoint(3395, 1469, 1),
      new OutlinePoint(3269, 1469, 1),
      new OutlinePoint(3269, 1524, 2),
      new OutlinePoint(3143, 1671, 2),
      new OutlinePoint(2986, 1856, 1),
      new OutlinePoint(3002, 1872, 1),
      new OutlinePoint(3198, 1726, 2),
      new OutlinePoint(3241, 1691, 2),
      new OutlinePoint(3284, 1691, 1),
      new OutlinePoint(3395, 1691, 2),
      new OutlinePoint(3514, 1722, 2),
      new OutlinePoint(3629, 1792, 1),
      new OutlinePoint(3745, 1862, 2),
      new OutlinePoint(3745, 2034, 2),
      new OutlinePoint(3745, 2262, 1),
      new OutlinePoint(3745, 2536, 2),
      new OutlinePoint(3511, 2787, 2),
      new OutlinePoint(3122, 2993, 1),
      new OutlinePoint(3642, 3888, 1)
    ];

    HPWGOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
