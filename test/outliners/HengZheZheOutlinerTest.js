/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheZheOutliner = require('../../src/outliners/HengZheZheOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheZheOutliner#addPoints', function() {
  it('should return the same output as GetHengZheZheOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(996, 1736),
      new Point(1986, 1736),
      new Point(1986, 1268),
      new Point(2804, 1268)
    ];
    var outline = new Outline(32, 102, 1, 1);

    var expectedOutlinePoints = [
      new OutlinePoint(996, 1704, 1),
      new OutlinePoint(996, 1768, 1),
      new OutlinePoint(1839, 1768, 1),
      new OutlinePoint(1976, 1940, 1),
      new OutlinePoint(2071, 1905, 2),
      new OutlinePoint(2175, 1831, 2),
      new OutlinePoint(2212, 1758, 1),
      new OutlinePoint(2088, 1634, 1),
      new OutlinePoint(2088, 1300, 1),
      new OutlinePoint(2804, 1300, 1),
      new OutlinePoint(2804, 1236, 1),
      new OutlinePoint(2088, 1236, 1),
      new OutlinePoint(1986, 1103, 1),
      new OutlinePoint(1900, 1135, 2),
      new OutlinePoint(1833, 1190, 2),
      new OutlinePoint(1782, 1268, 1),
      new OutlinePoint(1884, 1370, 1),
      new OutlinePoint(1884, 1704, 1)
    ];

    HengZheZheOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
