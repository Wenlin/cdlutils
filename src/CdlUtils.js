var CdlParser = require('./CdlParser');
var CdlChar = require('./CdlChar');
var CdlStroke = require('./CdlStroke');
var Point = require('./Point');
var CharacterRenderer = require('./CharacterRenderer');
var RenderBounds = require('./RenderBounds');
var DomXmlParser = require('./DomXmlParser');
var xmlDbLoader = require('./xmlDbLoader');
var apiDbLoader = require('./apiDbLoader');

var extractRenderBounds = function(options) {
  var width = options.width || 128;
  var height = options.height || 128;
  var x = options.x || 0;
  var y = -1 * (options.y || 0) - height;
  if (options.font === 'plain') {
    y = options.y || 0;
  }
  return new RenderBounds(x, y, width, height);
};

var CdlUtils = {

  getSvgPathStrings: function(charOrUnicode, options) {
    if (!options) options = {};
    var bounds = extractRenderBounds(options);
    return CdlUtils.fetchCdlChar(charOrUnicode, options).then(function(charCDL) {
      return CharacterRenderer.getSvgPathStrings(charCDL, bounds, options);
    });
  },

  fetchCdlChar: function(charOrUnicode, options) {
    if (!options) options = {};
    var fetchPromise = Promise.resolve().then(function() {
      if (charOrUnicode instanceof CdlChar) {
        return charOrUnicode;
      }
      var dataLoader = options.dataLoader || apiDbLoader(options);
      var parser = new CdlParser(dataLoader, options.xmlParser || DomXmlParser);
      return parser.loadAndParse(charOrUnicode, options.variant || 0);
    });
    if (options.onError) {
      fetchPromise = fetchPromise.catch(options.onError);
    }
    return fetchPromise;
  }
};

CdlUtils.xmlDbLoader = xmlDbLoader;
CdlUtils.CdlChar = CdlChar;
CdlUtils.CdlStroke = CdlStroke;
CdlUtils.Point = Point;
CdlUtils.Promise = Promise;

module.exports = CdlUtils;

// set up window.CdlUtils if we're in the browser
// TODO: research if this is the best way of doing this
if (typeof window !== 'undefined') {
  // store whatever used to be called CdlUtils in case of a conflict
  var previousCdlUtils = window.CdlUtils;

  // add a jQuery-esque noConflict method to restore the previous window['CdlUtils'] if necessary
  CdlUtils.noConflict = function() {
    window.CdlUtils = previousCdlUtils;
    return CdlUtils;
  };

  window.CdlUtils = CdlUtils;
}
