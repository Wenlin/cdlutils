/* eslint-env mocha */
var assert = require('chai').assert;
var HengXieGouOutliner = require('../../src/outliners/HengXieGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengXieGouOutliner#addPoints', function() {
  it('should return the same output as GetHengXieGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1464, 2932),
      new Point(3024, 2932),
      new Point(3492, 800),
      new Point(3856, 1684)
    ];
    var outline = new Outline(32, 115, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1560, 2900, 1),
      new OutlinePoint(1464, 2844, 1),
      new OutlinePoint(1400, 2964, 1),
      new OutlinePoint(2858, 2964, 1),
      new OutlinePoint(3012, 3162, 1),
      new OutlinePoint(3120, 3123, 2),
      new OutlinePoint(3237, 3040, 2),
      new OutlinePoint(3279, 2957, 1),
      new OutlinePoint(3139, 2817, 1),
      new OutlinePoint(3139, 1694, 2),
      new OutlinePoint(3181, 1369, 2),
      new OutlinePoint(3365, 927, 1),
      new OutlinePoint(3888, 1715, 1),
      new OutlinePoint(3919, 1684, 1),
      new OutlinePoint(3705, 1242, 2),
      new OutlinePoint(3619, 1021, 2),
      new OutlinePoint(3619, 800, 1),
      new OutlinePoint(3619, 705, 2),
      new OutlinePoint(3555, 673, 2),
      new OutlinePoint(3492, 673, 1),
      new OutlinePoint(3381, 673, 2),
      new OutlinePoint(3271, 673, 2),
      new OutlinePoint(3238, 737, 1),
      new OutlinePoint(2970, 1243, 2),
      new OutlinePoint(2909, 1615, 2),
      new OutlinePoint(2909, 2900, 1)
    ];

    HengXieGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
