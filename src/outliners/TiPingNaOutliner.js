var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addPingNaUpperCurve(outline, refPoints[1], refPoints[2]);
    OutlineHelpers.addPingNaRightEndPoints(outline, refPoints[3]);
    OutlineHelpers.addPingNaLowerCurve(outline, refPoints[1], refPoints[2]);
    OutlineHelpers.addSouthwestWideEndPoints(outline, refPoints[0]);
  }
};
