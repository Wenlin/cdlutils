var OutlinePoint = require('./OutlinePoint');
var CdlStroke = require('./CdlStroke');
var Point = require('./Point');
var Utils = require('./Utils');

var NA_PERC = 110;
var WAN_PERC = 90;
var SWG_PERC = 45;

var OutlineHelpers = {
  addHengLeftEndOutlinePoints: function(outline, refPoint) {
    var rad = outline.hengRadius;

    if (outline.isHeadCut()) {
      outline.addPoint(refPoint.x, refPoint.y - rad, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoint.x, refPoint.y + rad, OutlinePoint.ON_CURVE);
    } else {
      outline.addPoint(refPoint.x + rad * 3, refPoint.y - rad, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoint.x, refPoint.y - Utils.skPercent(rad, 275), OutlinePoint.ON_CURVE); // constant here controls size of onset nub
      outline.addPoint(refPoint.x - rad * 2, refPoint.y + rad, OutlinePoint.ON_CURVE);
    }
  },

  addHengRightEndOutlinePoints: function(outline, leftPoint, refPoint) {
    var rad = outline.hengRadius;

    if (outline.isTailCut()) {
      outline.addPoint(refPoint.x, refPoint.y + rad, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoint.x, refPoint.y - rad, OutlinePoint.ON_CURVE);
    } else { // "serif" right end
      var bumpWidth = outline.shuRadius * 4;
      var maxWidth = Utils.intCast((refPoint.x - leftPoint.x) / 2);
      if (bumpWidth > maxWidth) {
        bumpWidth = maxWidth;
      }
      var bumpHeight = Utils.skPercent(bumpWidth, 43);

      var rightX = refPoint.x + Utils.intCast(bumpWidth / 4);
      var rightY = refPoint.y - rad;
      var leftX = rightX - bumpWidth;
      var leftY = refPoint.y + rad;
      var topX = leftX + bumpHeight - Utils.intCast(bumpHeight / 4);
      var topY = leftY + bumpHeight;

      var ctrlOneX = topX + Utils.skPercent(bumpWidth, 59);
      var ctrlOneY = leftY;
      var ctrlTwoX = topX + Utils.skPercent(bumpWidth, 36);
      var ctrlTwoY = ctrlOneY + Utils.skPercent(bumpHeight, 66);

      outline.addPoint(leftX, leftY, OutlinePoint.ON_CURVE);
      outline.addPoint(topX, topY, OutlinePoint.ON_CURVE);
      outline.addPoint(ctrlTwoX, ctrlTwoY, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(ctrlOneX, ctrlOneY, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(rightX, rightY, OutlinePoint.ON_CURVE);
    }
  },

  addShuTopOutlinePoints: function(outline, refPoint) {
    var rad = outline.shuRadius;

    if (outline.isHeadCut()) {
      outline.addPoint(refPoint.x - rad, refPoint.y, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoint.x + rad, refPoint.y, OutlinePoint.ON_CURVE);
    } else {
      var y = refPoint.y + Utils.skPercent(rad, 160);
      var serifPtX = refPoint.x + Utils.skPercent(rad, 230);
      var serifPtY = refPoint.y;

      outline.addPoint(refPoint.x - rad, y, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoint.x, y, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(refPoint.x + rad, refPoint.y + rad, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(serifPtX, serifPtY, OutlinePoint.ON_CURVE);
      outline.addPoint(refPoint.x + rad, refPoint.y - Utils.skPercent(rad, 86), OutlinePoint.ON_CURVE);
    }
  },

  addShuBottomOutlinePoints: function(outline, endPoint) {
    var rad = outline.shuRadius;

    if (outline.isTailCut()) {
      outline.addPoint(endPoint.x + rad, endPoint.y, OutlinePoint.ON_CURVE);
      outline.addPoint(endPoint.x - rad, endPoint.y, OutlinePoint.ON_CURVE);
    } else {
      /* Move the whole thing up or down just by changing hangover */
      var delta = Utils.skPercent(rad, 30);
      var hangover;
      if (outline.isTailLong()) {
        /* First and second strokes of kou3 'mouth', for example */
        hangover = outline.hengRadius + Utils.skPercent(rad, 80);
      } else {
        hangover = Utils.intCast(rad / 4);
      }
      var y = endPoint.y - hangover - rad; /* Bottom left corner */

      outline.addPoint(endPoint.x + rad, y + rad, OutlinePoint.ON_CURVE);
      outline.addPoint(endPoint.x + delta, y + delta, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(endPoint.x - delta, y, OutlinePoint.OFF_CURVE_CUBIC);
      outline.addPoint(endPoint.x - rad, y, OutlinePoint.ON_CURVE);
    }
  },

  addPieTipOutline: function(outline, topRightPoint, bottomPoint, topLeftPoint, isVeryCurved) {
    if (outline.isTailLong()) {
      bottomPoint.x -= outline.shuRadius;
      bottomPoint.y -= outline.shuRadius;
    }
    var nearBottom = new Point(
      bottomPoint.x + Utils.intCast(outline.hengRadius / 2),
      bottomPoint.y - Utils.intCast(outline.hengRadius / 2)
    );
    var rightCtrlPnts = OutlineHelpers.getPieControlPoints(topRightPoint, nearBottom, isVeryCurved);
    var leftCtrlPnts = OutlineHelpers.getPieControlPoints(topLeftPoint, bottomPoint, isVeryCurved);

    outline.addPoint(topRightPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(rightCtrlPnts.top,  OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(rightCtrlPnts.bottom,  OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(nearBottom,    OutlinePoint.ON_CURVE);
    outline.addPoint(bottomPoint,   OutlinePoint.ON_CURVE);
    outline.addPoint(leftCtrlPnts.bottom,   OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(leftCtrlPnts.top,   OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topLeftPoint,  OutlinePoint.ON_CURVE);
  },

  addPieTopOutline: function(outline, refPoints, isVeryCurved) {
    var rad = outline.shuRadius; // pieRadius

    switch (outline.head) {
    case CdlStroke.STROKE_HEAD_CUT:
      /* Top cut off flat horizontally. */
      if (isVeryCurved) {
        /* Use shuRadius, exactly, for consistency with
            vertical strokes coming down from horizontals.
            Don't want discrepancy between "wp" and "sp". */
        // rad = outline.shuRadius;
      } else if ((refPoints[0].x - refPoints[1].x)
               > (refPoints[0].y - refPoints[1].y)) {
        /* Relatively flat (horizontal) */
        rad = Utils.skPercent(rad, 130);
      } else { /* Relatively vertical) */
        rad = Utils.skPercent(rad, 115);
      }
      return {
        start: new Point(refPoints[0].x + rad, refPoints[0].y),
        end: new Point(refPoints[0].x - rad, refPoints[0].y)
      };
    case CdlStroke.STROKE_HEAD_VERTICAL:
      /* Top right cut off flat vertically. For pie that comes
          off of shu without any heng, as in fantizi lai2 'come',
          also maybe ru4 'enter'. */
      if ((refPoints[0].x - refPoints[1].x)
          > (refPoints[0].y - refPoints[1].y)) {
        /* Relatively flat (horizontal) */
        rad = Utils.skPercent(rad, 220);
      } else { /* Relatively vertical) */
        rad = Utils.skPercent(rad, 250);
      }
      return {
        start: new Point(refPoints[0].x, refPoints[0].y - rad),
        end: new Point(refPoints[0].x, refPoints[0].y + rad)
      };
    case CdlStroke.STROKE_HEAD_CORNER:
      /* Top edge straight horizontal, upper right edge straight vertical.
          Useful in mu4 'tree'. */
      rad = Utils.skPercent(rad, 170);
      outline.addPoint(refPoints[0], OutlinePoint.ON_CURVE); // top right corner
      return {
        start: new Point(refPoints[0].x, refPoints[0].y - rad),
        end: new Point(refPoints[0].x - rad, refPoints[0].y)
      };
    case CdlStroke.STROKE_HEAD_NORMAL:
    default:
      if (isVeryCurved) {
        /* Must be consistent with stroke type "sp" */
        OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
        return {
          start: outline.getLastOutlinePoint().toPoint(),
          end: outline.getFirstOutlinePoint().toPoint()
        };
      }
      if ((refPoints[0].x - refPoints[1].x) * 2
          > (refPoints[0].y - refPoints[1].y) * 3) {
        /* Relatively flat (horizontal) */
        var serifPtX = refPoints[0].x + rad * 2;
        var serifPtY = refPoints[0].y - rad;
        var serifPt = new Point(serifPtX, serifPtY);

        var curveStartPointX = refPoints[0].x + rad;
        var curveStartPointY = refPoints[0].y - rad;
        var curveEndPointX   = refPoints[0].x - Utils.intCast(rad / 2);
        var curveEndPointY   = refPoints[0].y + rad;
        var curveEndPoint = new Point(curveEndPointX, curveEndPointY);
        outline.addPoint(curveEndPointX, curveEndPointY, OutlinePoint.ON_CURVE);
        OutlineHelpers.addNortheastConvexityPoints(outline, curveEndPoint, serifPt);
        outline.addPoint(serifPtX, serifPtY, OutlinePoint.ON_CURVE);
        return {
          start: new Point(curveStartPointX, curveStartPointY),
          end: curveEndPoint
        };
      }
      /* Relatively vertical, similar to ShuTop but narrower */
      OutlineHelpers.addPieZheTopOutlinePoints(outline, refPoints[0]);
      return {
        start: outline.getLastOutlinePoint().toPoint(),
        end: outline.getFirstOutlinePoint().toPoint()
      };
    }
  },

  addNortheastConvexityPoints: function(outline, topLeft, botRight) {
    var ctrlPoints = OutlineHelpers.interpolateNortheastConvexityPoints(botRight, topLeft);
    outline.addPoint(ctrlPoints[0].x, ctrlPoints[0].y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[1].x, ctrlPoints[1].y, OutlinePoint.OFF_CURVE_CUBIC);
  },

  interpolateNortheastConvexityPoints: function(botRight, topLeft) {
    var OL_NW_CTRL_X1 = 52;
    var OL_NW_CTRL_Y1 = 104;
    var OL_NW_CTRL_X2 = 108;
    var OL_NW_CTRL_Y2 = 52;
    var deltaX = botRight.x - topLeft.x;
    var deltaY = topLeft.y - botRight.y;
    return [
      new Point(
        topLeft.x + Utils.xyScaleMul(OL_NW_CTRL_X1, deltaX),
        botRight.y + Utils.xyScaleMul(OL_NW_CTRL_Y1, deltaY)
      ),
      new Point(
        topLeft.x + Utils.xyScaleMul(OL_NW_CTRL_X2, deltaX),
        botRight.y + Utils.xyScaleMul(OL_NW_CTRL_Y2, deltaY)
      )
    ];
  },

  addPieZheTopOutlinePoints: function(outline, refPoint) {
    var rad = Utils.skPercent(outline.shuRadius, 85); // pieRadius

    var serifPtX = refPoint.x + Utils.skPercent(rad, 230);
    var serifPtY = refPoint.y - Utils.intCast(rad / 2);
    var curveStartPointX = refPoint.x + rad;
    var curveStartPointY = refPoint.y - Utils.skPercent(rad, 136);
    var curveEndPointX = refPoint.x - rad;
    var curveEndPointY = refPoint.y + Utils.skPercent(rad, 110);

    var serifPt = new Point(serifPtX, serifPtY);
    var curveEndPoint = new Point(curveEndPointX, curveEndPointY);

    outline.addPoint(curveEndPointX, curveEndPointY, OutlinePoint.ON_CURVE);
    OutlineHelpers.addNortheastConvexityPoints(outline, curveEndPoint, serifPt);
    outline.addPoint(serifPtX, serifPtY, OutlinePoint.ON_CURVE);
    outline.addPoint(curveStartPointX, curveStartPointY, OutlinePoint.ON_CURVE);
  },

  getPieControlPoints: function(top, bottom, isVeryCurved) {
    var notVeryCurved = [
      54, 22, 106, 68
    ]; // not very curved
    var yesVeryCurved = [
      104, 30, 128, 52
    ]; // very curved
    var curve = isVeryCurved ? yesVeryCurved : notVeryCurved;
    var deltaX = top.x - bottom.x;
    var deltaY = top.y - bottom.y;

    return {
      top: new Point(
        bottom.x + Utils.xyScaleMul(curve[2], deltaX),
        bottom.y + Utils.xyScaleMul(curve[3], deltaY)
      ),
      bottom: new Point(
        bottom.x + Utils.xyScaleMul(curve[0], deltaX),
        bottom.y + Utils.xyScaleMul(curve[1], deltaY)
      )
    };
  },

  addNaStuff: function(outline, topRight, topLeft, bottomRef) {
    var endPoints = OutlineHelpers.prepareNaEndPoints(outline, bottomRef);

    outline.addPoint(topRight, OutlinePoint.ON_CURVE);

    var ctrlPoints = OutlineHelpers.interpolateNaPoints(topRight, endPoints.right, true);
    outline.addPoint(ctrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);

    outline.addPoint(endPoints.right, OutlinePoint.ON_CURVE);
    endPoints.right.y = endPoints.right.y - Utils.intCast(outline.shuRadius / 4); // avoid very sharp tip
    outline.addPoint(endPoints.right, OutlinePoint.ON_CURVE);

    ctrlPoints = OutlineHelpers.interpolateNaEndPoints(endPoints.right, endPoints.left);
    outline.addPoint(ctrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(endPoints.left, OutlinePoint.ON_CURVE);

    ctrlPoints = OutlineHelpers.interpolateNaPoints(topLeft, endPoints.left, false);
    outline.addPoint(ctrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topLeft, OutlinePoint.ON_CURVE);
  },

  prepareNaEndPoints: function(outline, refPoint) {
    var rad = Utils.skPercent(outline.shuRadius, NA_PERC); // naRadius

    return {
      left: new Point(refPoint.x - rad, refPoint.y - Utils.intCast(rad / 2) - Utils.intCast(rad / 4)),
      right: new Point(refPoint.x + rad + Utils.intCast(rad / 2), refPoint.y + rad)
    };
  },

  interpolateNaPoints: function(topLeft, botRight, isUpper) {
    var OL_NA_CURVE_CTRL_X1 = 22;
    var OL_NA_CURVE_CTRL_Y1 = 66;
    var OL_NA_CURVE_CTRL_X2 = 72;
    var OL_NA_CURVE_CTRL_Y2 = 22;
    var OL_NA_CURVE_CTRL_Y2_UPPER = 10;
    var ctrlY2 = isUpper ? OL_NA_CURVE_CTRL_Y2_UPPER : OL_NA_CURVE_CTRL_Y2;
    var deltaX = botRight.x - topLeft.x;
    var deltaY = topLeft.y - botRight.y;
    return [
      new Point(topLeft.x + Utils.xyScaleMul(OL_NA_CURVE_CTRL_X1, deltaX), botRight.y + Utils.xyScaleMul(OL_NA_CURVE_CTRL_Y1, deltaY)),
      new Point(topLeft.x + Utils.xyScaleMul(OL_NA_CURVE_CTRL_X2, deltaX), botRight.y + Utils.xyScaleMul(ctrlY2, deltaY))
    ];
  },

  interpolateNaEndPoints: function(topRight, bottomLeft) {
    var OL_NA_END_CTRL_X1 = 38;
    var OL_NA_END_CTRL_Y1 = 64;
    var OL_NA_END_CTRL_X2 = 74;
    var OL_NA_END_CTRL_Y2 = 104;
    var deltaX = topRight.x - bottomLeft.x;
    var deltaY = topRight.y - bottomLeft.y;
    return [
      new Point(
        bottomLeft.x + Utils.xyScaleMul(OL_NA_END_CTRL_X2, deltaX),
        bottomLeft.y + Utils.xyScaleMul(OL_NA_END_CTRL_Y2, deltaY)
      ),
      new Point(
        bottomLeft.x + Utils.xyScaleMul(OL_NA_END_CTRL_X1, deltaX),
        bottomLeft.y + Utils.xyScaleMul(OL_NA_END_CTRL_Y1, deltaY)
      )
    ];
  },

  addPingNaUpperCurve: function(outline, p, q) {
    p = p.clone();
    q = q.clone();

    var rad = Utils.skPercent(outline.shuRadius, NA_PERC); // naRadius

    p.y += rad;
    q.y += rad;

    /* In characters like 爬, don't want the "pn" stroke to be too thin at the top. */
    if (p.y - q.y > 2 * (q.x - p.y)) {
      p.x += Utils.intCast(rad / 2);
    }
    outline.addPoint(p, OutlinePoint.ON_CURVE);
    outline.addPoint(Utils.intCast((p.x * 3 + q.x) / 4), q.y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(q, OutlinePoint.OFF_CURVE_CUBIC);
  },

  addPingNaRightEndPoints: function(outline, refPoint) {
    var endPoints = OutlineHelpers.preparePingNaEndPoints(outline, refPoint);

    outline.addPoint(endPoints.right, OutlinePoint.ON_CURVE);
    endPoints.right.y = endPoints.right.y - Utils.intCast(outline.shuRadius / 4); // avoid sharp point
    outline.addPoint(endPoints.right, OutlinePoint.ON_CURVE);

    var ctrlPoints = OutlineHelpers.interpolateNaEndPoints(endPoints.right, endPoints.left);

    outline.addPoint(ctrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(endPoints.left, OutlinePoint.ON_CURVE);
  },

  preparePingNaEndPoints: function(outline, refPoint) {
    var rad = Utils.skPercent(outline.shuRadius, NA_PERC); // naRadius
    return {
      left: new Point(refPoint.x - rad, refPoint.y - rad),
      right: new Point(refPoint.x + rad + rad, refPoint.y + rad + rad)
    };
  },

  addPingNaLowerCurve: function(outline, p, q) {
    q = q.clone();
    q.y -= Utils.skPercent(outline.shuRadius, NA_PERC); // naRadius

    outline.addPoint(q, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(Utils.intCast((p.x * 3 + q.x) / 4), q.y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(p, OutlinePoint.ON_CURVE);
  },

  addSouthwestWideEndPoints: function(outline, refPoint) {
    var rad = outline.shuRadius;
    outline.addPoint(refPoint.x, refPoint.y - Utils.skPercent(rad, 162), OutlinePoint.ON_CURVE);
    outline.addPoint(refPoint.x - Utils.skPercent(rad, 84), refPoint.y - Utils.skPercent(rad, 130), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(refPoint.x - Utils.skPercent(rad, 150), refPoint.y - Utils.skPercent(rad, 76), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(refPoint.x - rad * 2, refPoint.y, OutlinePoint.ON_CURVE);
  },

  addWanGouStuff: function(outline, topLeft, topRight, refPoints, shouldFixHook) {
    var OL_WG_CTRL_X1 = 80;
    var OL_WG_CTRL_Y1 = 92;
    var OL_WG_CTRL_Y2 = 48;
    var midLeft  = refPoints[0].clone(); // middle
    var midRight = refPoints[0].clone();
    midLeft.x -= outline.shuRadius;
    midRight.x += outline.shuRadius;

    var deltaX = midRight.x - topRight.x;
    var deltaY = topRight.y - midRight.y;
    outline.addPoint(topRight, OutlinePoint.ON_CURVE);
    outline.addPoint(topRight.x + Utils.xyScaleMul(OL_WG_CTRL_X1, deltaX),
          midRight.y + Utils.xyScaleMul(OL_WG_CTRL_Y1, deltaY), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(midRight.x,
          midRight.y + Utils.xyScaleMul(OL_WG_CTRL_Y2, deltaY), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(midRight, OutlinePoint.ON_CURVE);

    OutlineHelpers.addWanGouBottomOutline(outline, refPoints[0], refPoints[1], refPoints[2], shouldFixHook);

    deltaX = midLeft.x - topLeft.x;
    deltaY = topLeft.y - midLeft.y;
    outline.addPoint(midLeft, OutlinePoint.ON_CURVE);
    outline.addPoint(midLeft.x, midLeft.y + Utils.xyScaleMul(OL_WG_CTRL_Y2, deltaY), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topLeft.x + Utils.xyScaleMul(OL_WG_CTRL_X1, deltaX), midLeft.y + Utils.xyScaleMul(OL_WG_CTRL_Y1, deltaY), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topLeft, OutlinePoint.ON_CURVE);
  },

  addWanGouBottomOutline: function(outline, topPoint, bottomPoint, hookPoint, shouldFixHook) {
    var rad = outline.shuRadius;

    var maxX = bottomPoint.x - rad * 3;
    if (shouldFixHook && hookPoint.x > maxX) {
      /* Don't do this for stroke type "hpwg": can cause hook
           to penetrate horizontal stroke in 阝. */
      hookPoint.x = maxX;
    }
    /* Hook point uses hengRadius instead of shuRadius; sharper. */
    var nearHook = new Point(
      hookPoint.x - Utils.intCast(outline.hengRadius / 2),
      hookPoint.y - Utils.intCast(outline.hengRadius / 2)
    );

    var aboveHeel = new Point(bottomPoint.x - rad, bottomPoint.y + rad);
    var delta = topPoint.y - aboveHeel.y;

    var offL = new Point(topPoint.x - rad, topPoint.y - Utils.skPercent(delta, 40));
    var offK = new Point(topPoint.x - rad, Utils.intCast((offL.y + aboveHeel.y) / 2));

    var offA = new Point(topPoint.x + rad, offL.y - Utils.intCast(rad / 2));
    var offB = new Point(topPoint.x + rad, offK.y - Utils.intCast(rad / 2));

    delta = offK.y - aboveHeel.y;
    var offJ = new Point(Utils.intCast((aboveHeel.x + offK.x) / 2), aboveHeel.y + Utils.skPercent(delta, 18));

    var offI = new Point(aboveHeel.x + rad, aboveHeel.y);
    maxX = Utils.intCast((aboveHeel.x + offJ.x) / 2);
    if (offI.x > maxX) {
      offI.x = maxX;
    }
    var offG = new Point(
      Utils.intCast((bottomPoint.x + hookPoint.x) / 2),
      Utils.intCast((bottomPoint.y + hookPoint.y) / 2)
    );
    var offH = new Point(Utils.intCast((offG.x + aboveHeel.x) / 2), aboveHeel.y);
    var offF = new Point(offG.x - Utils.intCast(rad / 2), offG.y - Utils.intCast(rad / 2));
    var heelPoint = new Point(Utils.intCast((offF.x + bottomPoint.x) / 2), bottomPoint.y - rad);
    var minX = bottomPoint.x - Utils.skPercent(rad, 160);
    if (heelPoint.x < minX) {
      heelPoint.x = minX;
    }

    var offE = new Point(heelPoint.x, Utils.intCast((bottomPoint.y + heelPoint.y) / 2));
    var nearHeel = new Point(bottomPoint.x, heelPoint.y);
    var offC = new Point(offJ.x + Utils.skPercent(rad, 240), offJ.y - Utils.skPercent(rad, 140));
    var offD = new Point(Utils.intCast((aboveHeel.x + offC.x) / 2), heelPoint.y);
    if (offD.x < aboveHeel.x + rad) {
      offD.x = aboveHeel.x + rad;
    }

    var onBC = new Point(
      Utils.intCast((offB.x + offC.x) / 2),
      Utils.intCast((offB.y + offC.y) / 2)
    );

    var onJK = new Point(
      Utils.intCast((offJ.x + offK.x) / 2),
      Utils.intCast((offJ.y + offK.y) / 2)
    );

    outline.addPoint(offA, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(offB, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(onBC, OutlinePoint.ON_CURVE);
    outline.addPoint(offC, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(offD, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(nearHeel, OutlinePoint.ON_CURVE);
    outline.addPoint(heelPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(offE, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(offF, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(nearHook, OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(offG, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(offH, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(aboveHeel, OutlinePoint.ON_CURVE);
    outline.addPoint(offI, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(offJ, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(onJK, OutlinePoint.ON_CURVE);
    outline.addPoint(offK, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(offL, OutlinePoint.OFF_CURVE_CUBIC);
  },

  //  Given two points (which define a center line) and a radius, get two points
  //  on a parallel line, whose distance from the center line is the radius.
  getTwoParallelPoints: function(input, r) {
    var hyp = Utils.hypotenuse(input[1].x - input[0].x, input[1].y - input[0].y);

    var point1 = new Point(
      input[0].x + Utils.intCast(((input[0].y - input[1].y) * r) / hyp),
      input[0].y + Utils.intCast(((input[1].x - input[0].x) * r) / hyp)
    );
    return [
      point1,
      new Point(
        point1.x + input[1].x - input[0].x,
        point1.y + input[1].y - input[0].y
      )
    ];
  },

  // Given an array of four Bezier points, fill in an array of seven Bezier points
  // giving a closer approximation of the curve.
  getBezierSevenFromFour: function(four) {
    var sev = [];
    /* mid is halfway between four[1] and four[2]. */
    var mid = new Point(
      Utils.intCast((four[1].x + four[2].x) / 2),
      Utils.intCast((four[1].y + four[2].y) / 2)
    );

    /* The first and last output points are same as input. */
    sev[0] = four[0];
    sev[6] = four[3];

    /* sev[1] is halfway between four[0] and four[1]. */
    sev[1] = new Point(
      Utils.intCast((four[0].x + four[1].x) / 2),
      Utils.intCast((four[0].y + four[1].y) / 2)
    );

    /* sev[5] is halfway between four[2] and four[3]. */
    sev[5] = new Point(
      Utils.intCast((four[2].x + four[3].x) / 2),
      Utils.intCast((four[2].y + four[3].y) / 2)
    );

    /* sev[2] is halfway between sev[1] and mid. */
    sev[2] = new Point(
      Utils.intCast((sev[1].x + mid.x) / 2),
      Utils.intCast((sev[1].y + mid.y) / 2)
    );

    /* sev[4] is halfway between sev[5] and mid. */
    sev[4] = new Point(
      Utils.intCast((sev[5].x + mid.x) / 2),
      Utils.intCast((sev[5].y + mid.y) / 2)
    );

    /* sev[3] is halfway between sev[2] and sev[4]. */
    sev[3] = new Point(
      Utils.intCast((sev[2].x + sev[4].x) / 2),
      Utils.intCast((sev[2].y + sev[4].y) / 2)
    );

    return sev;
  },

  addHengPieOutlineCornerPoints: function(outline, refPoint) {
    OutlineHelpers.addHengZheOrPieCorner(outline, refPoint, true);
  },

  addHengZheOrPieCorner: function(outline, refPoint, isPie) {
    var bottomRight;
    var farRight;
    var rad = outline.shuRadius;

    /* If isPie === false, bottomRight must match curveEndPoint in GetHZWGFOutline(),
        else must match curveEndPoint in GetHengZheZhePieOutline(). */

    var bottomRightX = refPoint.x + rad;
    var farRightX = refPoint.x + Utils.skPercent(rad, 222);
    if (isPie) {
      bottomRight = new Point(bottomRightX, refPoint.y - Utils.skPercent(rad, 190));
      farRight = new Point(farRightX, refPoint.y - Utils.skPercent(rad, 120));
    } else {
      bottomRight = new Point(bottomRightX, refPoint.y - rad);
      farRight = new Point(farRightX, refPoint.y + Utils.skPercent(rad, 22));
    }
    var top = new Point(refPoint.x - Utils.skPercent(rad, 10), refPoint.y + rad * 2);
    var left = new Point(refPoint.x - Utils.skPercent(rad, 144), refPoint.y + outline.hengRadius);

    outline.addPoint(left, OutlinePoint.ON_CURVE);
    outline.addPoint(top, OutlinePoint.ON_CURVE);
    OutlineHelpers.addNortheastConvexityPoints(outline, top, farRight);
    outline.addPoint(farRight, OutlinePoint.ON_CURVE);
    outline.addPoint(bottomRight, OutlinePoint.ON_CURVE);
  },

  addHengzheOutlineCornerPoints: function(outline, refPoint) {
    OutlineHelpers.addHengZheOrPieCorner(outline, refPoint, false);
  },

  addNortheastCornerPoint: function(outline, refPoint) {
    outline.addPoint(refPoint.x + outline.shuRadius, refPoint.y + outline.hengRadius, OutlinePoint.ON_CURVE);
  },

  addShuZheOutlineCornerPoints: function(outline, refPoint) {
    outline.addPoint(refPoint.x + outline.shuRadius, refPoint.y - outline.hengRadius, OutlinePoint.ON_CURVE);
    OutlineHelpers.addSouthwestWideEndPoints(outline, refPoint);
    outline.addPoint(refPoint.x - outline.shuRadius, refPoint.y + outline.shuRadius, OutlinePoint.ON_CURVE);
  },

  addSouthwestCornerPoint: function(outline, refPoint) {
    outline.addPoint(refPoint.x - outline.shuRadius, refPoint.y - outline.hengRadius, OutlinePoint.ON_CURVE);
  },

  addSWGInsidePoints: function(outline, topLeftPoint, botRightPoint) {
    topLeftPoint = topLeftPoint.clone();
    botRightPoint = botRightPoint.clone();
    topLeftPoint.x += outline.shuRadius;
    botRightPoint.y += Utils.skPercent(outline.shuRadius, WAN_PERC); // wanRadius

    /* This can happen. Fixed bug where Utils.skPercent was given negative argument. */
    if (botRightPoint.y > topLeftPoint.y) {
      botRightPoint.y = topLeftPoint.y;
    }
    if (topLeftPoint.x > botRightPoint.x) {
      topLeftPoint.x = botRightPoint.x;
    }

    outline.addPoint(topLeftPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(topLeftPoint.x,
          botRightPoint.y + Utils.skPercent(topLeftPoint.y - botRightPoint.y, SWG_PERC),
          OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topLeftPoint.x +
          Utils.skPercent(botRightPoint.x - topLeftPoint.x, SWG_PERC),
          botRightPoint.y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(botRightPoint, OutlinePoint.ON_CURVE);
  },

  addSWGRightEndPoints: function(outline, bottomPoint, topPoint) {
    topPoint = topPoint.clone();
    bottomPoint = bottomPoint.clone();
    var rad = Utils.skPercent(outline.shuRadius, WAN_PERC); // wanRadius
    var minY = bottomPoint.y + rad * 4;
    if (topPoint.y < minY) {
      topPoint.y = minY;
    }
    var y3 = bottomPoint.y + Utils.intCast(rad / 2);
    var y4 = Utils.intCast((3 * y3 + topPoint.y) / 4);
    var y5 = Utils.intCast((3 * y4 + topPoint.y) / 4);
    outline.addPoint(bottomPoint.x - Utils.skPercent(rad, 240), bottomPoint.y + rad, OutlinePoint.ON_CURVE);
    outline.addPoint(bottomPoint.x - Utils.skPercent(rad, 120), bottomPoint.y + rad, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x - Utils.intCast(rad / 2), Utils.intCast((3 * bottomPoint.y + topPoint.y) / 4), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(topPoint.x + Utils.intCast(rad / 4), topPoint.y + Utils.intCast(rad / 4), OutlinePoint.ON_CURVE);
    outline.addPoint(bottomPoint.x + Utils.intCast(rad / 2), y5, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x + rad * 2, y4, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x + rad * 2, y3, OutlinePoint.ON_CURVE);
    outline.addPoint(bottomPoint.x + rad * 2, bottomPoint.y - Utils.intCast(rad / 2), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x + rad, bottomPoint.y - rad, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x - rad, bottomPoint.y - rad, OutlinePoint.ON_CURVE);
  },

  addSWGOutsidePoints: function(outline, topLeftPoint, botRightPoint) {
    topLeftPoint = topLeftPoint.clone();
    botRightPoint = botRightPoint.clone();
    topLeftPoint.x -= outline.shuRadius;
    botRightPoint.y -= Utils.skPercent(outline.shuRadius, WAN_PERC); // wanRadius

    outline.addPoint(botRightPoint, OutlinePoint.ON_CURVE);

    outline.addPoint(
          topLeftPoint.x + Utils.skPercent(botRightPoint.x - topLeftPoint.x, SWG_PERC),
          botRightPoint.y,
          OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(
          topLeftPoint.x,
          botRightPoint.y + Utils.skPercent(topLeftPoint.y - botRightPoint.y, SWG_PERC),
          OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(topLeftPoint, OutlinePoint.ON_CURVE);
  },

  addSCRightEndPoints: function(outline, right) {
    var rad = Utils.skPercent(outline.shuRadius, WAN_PERC); // wanRadius;

    outline.addPoint(right.x - rad, right.y + rad, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(right.x + rad, right.y + rad * 2, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(right.x + rad, right.y, OutlinePoint.ON_CURVE);
    outline.addPoint(right.x + rad, right.y - Utils.intCast(rad / 2), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(right.x + Utils.intCast(rad / 2), right.y - rad, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(right.x - rad, right.y - rad, OutlinePoint.ON_CURVE);
  },

  addXieGouExceptTop: function(outline, topRight, bottomPoint, hookPoint, topLeft) {
    hookPoint = hookPoint.clone();
    var rad = Utils.skPercent(outline.shuRadius, NA_PERC); // naRadius

    /* Move hookPoint to the right. (Otherwise, at small sizes, the hook would
       look like a loop, curling back too far to the left.)  */
    hookPoint.x += Utils.intCast(rad / 2);

    var corner = new Point(bottomPoint.x - rad, bottomPoint.y + rad);
    var lowLeft = new Point(bottomPoint.x - rad * 2, bottomPoint.y - Utils.intCast(rad / 2));
    var xieRightPoints = OutlineHelpers.interpolateXiePoints(topRight, corner);
    var xieLeftPoints = OutlineHelpers.interpolateXiePoints(topLeft, lowLeft);

    var rug = new Point(bottomPoint.x, bottomPoint.y - rad);
    var heelA;
    /* Make heelA coutlinenear with xieLeftB and lowLeft */
    if (xieLeftPoints[1].y === lowLeft.y) {
      heelA = new Point(Utils.intCast((lowLeft.x + rug.x) / 2), rug.y);
    } else {
      heelA = new Point(
        lowLeft.x + Utils.intCast(((lowLeft.x - xieLeftPoints[1].x) * (lowLeft.y - rug.y)) / (xieLeftPoints[1].y - lowLeft.y)),
        rug.y
      );
      if (heelA.x > rug.x) {
        heelA.x = rug.x;
      }
    }
    var heelB = new Point(Utils.intCast((heelA.x + rug.x) / 2), rug.y);

    var bumper = new Point(bottomPoint.x + rad, bottomPoint.y - Utils.intCast(rad / 2));
    var nose = new Point(bumper.x, bottomPoint.y);
    var noseHookA = new Point(bumper.x, Utils.intCast((bottomPoint.y * 3 + hookPoint.y) / 4));
    var noseHookB = new Point(
      Utils.intCast((bottomPoint.x + hookPoint.x) / 2),
      Utils.intCast((bottomPoint.y + hookPoint.y) / 2)
    );

    outline.addPoint(xieRightPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(xieRightPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(corner, OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint.x - Utils.intCast(rad / 4), hookPoint.y + Utils.intCast(rad / 4), OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(noseHookB, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(noseHookA, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(nose, OutlinePoint.ON_CURVE);
    outline.addPoint(bumper.x, Utils.intCast((rug.y + bumper.y) / 2), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(Utils.intCast((rug.x + bumper.x) / 2), rug.y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(rug, OutlinePoint.ON_CURVE);
    outline.addPoint(heelB, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(heelA, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(lowLeft, OutlinePoint.ON_CURVE);
    outline.addPoint(xieLeftPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(xieLeftPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
  },

  interpolateXiePoints: function(topLeft, bottomRight) {
    var OL_XIE_CURVE_CTRL_Y1 = 52;
    var OL_XIE_CURVE_CTRL_X2 = 24;
    var OL_XIE_CURVE_CTRL_Y2 = 30;
    var deltaX = bottomRight.x - topLeft.x;
    var deltaY = topLeft.y - bottomRight.y;
    return [
      new Point(topLeft.x, bottomRight.y + Utils.xyScaleMul(OL_XIE_CURVE_CTRL_Y1, deltaY)),
      new Point(topLeft.x + Utils.xyScaleMul(OL_XIE_CURVE_CTRL_X2, deltaX), bottomRight.y + Utils.xyScaleMul(OL_XIE_CURVE_CTRL_Y2, deltaY))
    ];
  },

  addHZWGYOutline: function(outline, interp, refPoints) {
    var rad = outline.shuRadius; /* to match GetSWGOutsidePoints() */

    var corner = outline.getLastOutlinePoint().toPoint();

    /* Next comes a straight diagonal falling to the left,
        followed by a curve tangential to it. */
    var p = interp.clone();
    p.x += rad;
    p.y -= rad; // make it a little thicker here
    var q = refPoints[2].clone();
    q.x += rad;
    var ctrlPoints = OutlineHelpers.interpolateBackwardsDianControlPoints(corner, p, q);
    outline.addPoint(p, OutlinePoint.ON_CURVE);
    outline.addPoint(ctrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(q, OutlinePoint.ON_CURVE);
    OutlineHelpers.addSWGInsidePoints(outline, refPoints[2], refPoints[3]);
    OutlineHelpers.addSWGRightEndPoints(outline, refPoints[4], refPoints[5]);
    OutlineHelpers.addSWGOutsidePoints(outline, refPoints[2], refPoints[3]);
    p.x -= rad * 2;
    p.y += rad;
    q.x -= rad * 2;
    corner.x -= rad * 2;
    corner.y = refPoints[1].y - outline.hengRadius;
    ctrlPoints = OutlineHelpers.interpolateBackwardsDianControlPoints(corner, p, q);
    outline.addPoint(q, OutlinePoint.ON_CURVE);
    outline.addPoint(ctrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(p, OutlinePoint.ON_CURVE);
    outline.addPoint(corner, OutlinePoint.ON_CURVE);
  },

  interpolateBackwardsDianControlPoints: function(corner, p, q) {
    var ctrlA;
    /* Want (p.y - ctrlA->y) / (p.x - ctrlA->x)
            == (corner.y - p.y) / (corner.x - p.x) */
    if (p.x === corner.x) {
      ctrlA = q.clone(); // don't divide by zero!
    } else {
      ctrlA = new Point(q.x, p.y - Utils.intCast(((p.x - q.x) * (corner.y - p.y)) / (corner.x - p.x)));
    }
    var ctrlB = new Point(q.x, Utils.intCast((q.y + ctrlA.y) / 2));

    /* Sanity check */
    if (ctrlA.y < q.y || ctrlB.y < q.y || ctrlA.y > p.y || ctrlB.y > p.y
        || ctrlA.x < q.x || ctrlB.x < q.x || ctrlA.x > p.x || ctrlB.x > p.x) {
      ctrlA = p.clone();
      ctrlB = q.clone();
    }
    return [ctrlA, ctrlB];
  },

  addShuGouBottomOutline: function(outline, bottomPoint, hookPoint) {
    var rad = outline.shuRadius;

    var maxX = bottomPoint.x - rad * 3;
    if (hookPoint.x > maxX) {
      hookPoint.x = maxX;
    }
    var p = new Point(
      Utils.intCast((bottomPoint.x + hookPoint.x) / 2),
      Utils.intCast((bottomPoint.y + hookPoint.y) / 2)
    );
    var delta = Utils.intCast(rad / 4);
    var pp = p.clone();
    var nearHook = hookPoint.clone();
    nearHook.x -= delta;
    nearHook.y -= delta;
    pp.x -= delta;
    pp.y -= delta;
    var poo = new Point(
      Utils.intCast((pp.x + bottomPoint.x) / 2),
      bottomPoint.y - Utils.intCast(rad / 2)
    );

    var minX = bottomPoint.x - rad * 2;
    if (poo.x < minX) {
      poo.x = minX;
    }
    var how = new Point(poo.x, bottomPoint.y - rad);
    var lastPoint = new Point(bottomPoint.x + rad, bottomPoint.y + rad);

    outline.addPoint(lastPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(lastPoint.x, Utils.intCast((how.y + lastPoint.y) / 2), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(Utils.intCast((how.x + lastPoint.x) / 2), how.y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(how, OutlinePoint.ON_CURVE);
    outline.addPoint(poo, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(pp, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(nearHook, OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(p, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x - rad, bottomPoint.y + Utils.skPercent(rad, 170), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(bottomPoint.x - rad, bottomPoint.y + rad * 2, OutlinePoint.ON_CURVE);
  },

  addHengGouRightEndOutlinePoints: function(outline, rightPoint, hookPoint) {
    var rad = outline.shuRadius;
    var x = rightPoint.x + Utils.skPercent(rad, 140);
    var y = rightPoint.y - Utils.skPercent(rad, 120);

    /* Straight line down left to far left */
    outline.addPoint(rightPoint.x - rad * 2, rightPoint.y + outline.hengRadius, OutlinePoint.ON_CURVE);

    /* Curve left up convex to tippy top */
    outline.addPoint(rightPoint.x - Utils.skPercent(rad, 68), rightPoint.y + rad * 2, OutlinePoint.ON_CURVE);
    outline.addPoint(rightPoint.x + Utils.skPercent(rad, 112), rightPoint.y + Utils.skPercent(rad, 40), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(x, y + Utils.skPercent(rad, 70), OutlinePoint.OFF_CURVE_CUBIC);

    /* Curve to far right */
    outline.addPoint(x, y, OutlinePoint.ON_CURVE);
    outline.addPoint(rightPoint.x + Utils.skPercent(rad, 10), rightPoint.y - Utils.skPercent(rad, 140), OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(hookPoint.x + Utils.skPercent(rad, 88), hookPoint.y + Utils.skPercent(rad, 40), OutlinePoint.OFF_CURVE_CUBIC);

    /* Tip of hook */
    outline.addPoint(hookPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint.x - Utils.intCast(rad / 4), hookPoint.y - Utils.intCast(rad / 4), OutlinePoint.ON_CURVE);

    /* Inside corner */
    outline.addPoint(rightPoint.x - Utils.skPercent(rad, 180), rightPoint.y - outline.hengRadius, OutlinePoint.ON_CURVE);
  },

  addShuTiBottomPoints: function(outline, bottomPoint, hookPoint) {
    var rad = outline.shuRadius;
    var a = new Point(hookPoint.x - Utils.intCast(rad / 4), hookPoint.y + Utils.intCast(rad / 4));
    var b = new Point(bottomPoint.x + rad, bottomPoint.y + Utils.skPercent(rad, 240));
    if (b.y > a.y) {
      b.y = a.y;
    }
    outline.addPoint(b, OutlinePoint.ON_CURVE);
    outline.addPoint(a, OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint, OutlinePoint.ON_CURVE);
    OutlineHelpers.addShuZheOutlineCornerPoints(outline, bottomPoint);
  },

  addPieGouBottomOutline: function(outline, curveStartPoint, bottomRefPoint, hookPoint, curveEndPoint) {
    var rad = outline.shuRadius;

    var heelPoint = new Point(
      bottomRefPoint.x - rad - Utils.intCast(rad / 2),
      bottomRefPoint.y - rad
    );

    var restPoint = new Point(
      bottomRefPoint.x,
      bottomRefPoint.y + rad
    );

    var endCtrlPoints = OutlineHelpers.getPieControlPoints(curveEndPoint, restPoint, false);
    var startCtrlPoints = OutlineHelpers.getPieControlPoints(curveStartPoint, heelPoint, false);

    var ctrlF = new Point(
      Utils.intCast((bottomRefPoint.x + hookPoint.x) / 2),
      Utils.intCast((bottomRefPoint.y + hookPoint.y) / 2)
    );
    var ctrlE = new Point(
      Utils.intCast((ctrlF.x + restPoint.x) / 2),
      restPoint.y
    );
    var delta = Utils.intCast(rad / 2);
    var nearHook = new Point(hookPoint.x - delta, hookPoint.y - delta);
    var ctrlG = new Point(ctrlF.x - delta, ctrlF.y - delta);
    var ctrlH = new Point(heelPoint.x, Utils.intCast((bottomRefPoint.y + heelPoint.y) / 2));

    outline.addPoint(startCtrlPoints.top, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(startCtrlPoints.bottom, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(heelPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(ctrlH, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlG, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(nearHook, OutlinePoint.ON_CURVE);
    outline.addPoint(hookPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(ctrlF, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlE, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(restPoint, OutlinePoint.ON_CURVE);
    outline.addPoint(endCtrlPoints.bottom, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(endCtrlPoints.top, OutlinePoint.OFF_CURVE_CUBIC);
  }

};

module.exports = OutlineHelpers;
