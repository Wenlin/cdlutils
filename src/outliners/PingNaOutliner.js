var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addPingNaUpperCurve(outline, refPoints[0], refPoints[1]);
    OutlineHelpers.addPingNaRightEndPoints(outline, refPoints[2]);
    OutlineHelpers.addPingNaLowerCurve(outline, refPoints[0], refPoints[1]);
  }
};
