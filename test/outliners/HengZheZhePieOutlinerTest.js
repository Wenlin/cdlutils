/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheZhePieOutliner = require('../../src/outliners/HengZheZhePieOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheZhePieOutliner#addPoints', function() {
  it('should return the same output as GetHengZheZhePieOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(900, 4024),
      new Point(2599, 4024),
      new Point(2351, 3066),
      new Point(3180, 3066),
      new Point(1149, 800)
    ];
    var outline = new Outline(32, 117, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(996, 3992, 1),
      new OutlinePoint(900, 3936, 1),
      new OutlinePoint(836, 4056, 1),
      new OutlinePoint(2430, 4056, 1),
      new OutlinePoint(2587, 4258, 1),
      new OutlinePoint(2697, 4219, 2),
      new OutlinePoint(2816, 4134, 2),
      new OutlinePoint(2859, 4050, 1),
      new OutlinePoint(2716, 3907, 1),
      new OutlinePoint(2468, 3098, 1),
      new OutlinePoint(3011, 3098, 1),
      new OutlinePoint(3168, 3300, 1),
      new OutlinePoint(3278, 3229, 2),
      new OutlinePoint(3397, 3077, 2),
      new OutlinePoint(3440, 2926, 1),
      new OutlinePoint(3297, 2844, 1),
      new OutlinePoint(2930, 1878, 2),
      new OutlinePoint(2064, 1138, 2),
      new OutlinePoint(1165, 784, 1),
      new OutlinePoint(1149, 800, 1),
      new OutlinePoint(1956, 1183, 2),
      new OutlinePoint(2734, 1986, 2),
      new OutlinePoint(3063, 3034, 1),
      new OutlinePoint(2468, 3034, 1),
      new OutlinePoint(2351, 2876, 1),
      new OutlinePoint(2253, 2914, 2),
      new OutlinePoint(2175, 2977, 2),
      new OutlinePoint(2117, 3066, 1),
      new OutlinePoint(2234, 3183, 1),
      new OutlinePoint(2482, 3992, 1)
    ];

    HengZheZhePieOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
