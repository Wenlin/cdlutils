/* eslint-env mocha */
var assert = require('chai').assert;
var DianNaOutliner = require('../../src/outliners/DianNaOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('DianNaOutliner#addPoints', function() {
  it('should return the same output as GetDianNaOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1256, 4128),
      new Point(2088, 3712),
      new Point(3856, 800)
    ];
    var outline = new Outline(32, 118, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1138, 4128, 1),
      new OutlinePoint(1138, 4144, 2),
      new OutlinePoint(1256, 4160, 2),
      new OutlinePoint(1374, 4160, 1),
      new OutlinePoint(1764, 4160, 2),
      new OutlinePoint(2154, 3936, 2),
      new OutlinePoint(2206, 3712, 1),
      new OutlinePoint(2523, 2364, 2),
      new OutlinePoint(3243, 1147, 2),
      new OutlinePoint(4051, 930, 1),
      new OutlinePoint(3913, 887, 2),
      new OutlinePoint(3822, 816, 2),
      new OutlinePoint(3726, 703, 1),
      new OutlinePoint(2957, 1214, 2),
      new OutlinePoint(2271, 2238, 2),
      new OutlinePoint(1970, 3680, 1),
      new OutlinePoint(1884, 4096, 2),
      new OutlinePoint(1138, 4096, 2)
    ];

    DianNaOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
