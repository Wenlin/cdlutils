var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[1]);
    OutlineHelpers.addShuBottomOutlinePoints(outline, refPoints[2]);
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[1]);
  }
};
