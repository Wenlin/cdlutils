/* eslint-env mocha */
var assert = require('chai').assert;
var ShuWanZuoOutliner = require('../../src/outliners/ShuWanZuoOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuWanZuoOutliner#addPoints', function() {
  it('should return the same output as GetShuWanZuoOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(3440, 3712),
      new Point(3440, 1216),
      new Point(944, 1216)
    ];
    var outline = new Outline(32, 120, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(3320, 3904, 1),
      new OutlinePoint(3440, 3904, 2),
      new OutlinePoint(3560, 3832, 2),
      new OutlinePoint(3716, 3712, 1),
      new OutlinePoint(3560, 3609, 1),
      new OutlinePoint(3560, 1088, 1),
      new OutlinePoint(3476, 1004, 2),
      new OutlinePoint(3404, 968, 2),
      new OutlinePoint(3320, 968, 1),
      new OutlinePoint(3320, 1184, 1),
      new OutlinePoint(1040, 1184, 1),
      new OutlinePoint(944, 1128, 1),
      new OutlinePoint(880, 1248, 1),
      new OutlinePoint(3320, 1248, 1)
    ];

    ShuWanZuoOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
