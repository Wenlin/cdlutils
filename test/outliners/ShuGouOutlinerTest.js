/* eslint-env mocha */
var assert = require('chai').assert;
var ShuGouOutliner = require('../../src/outliners/ShuGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuGouOutliner#addPoints', function() {
  it('should return the same output as GetShuGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1311, 2400),
      new Point(1311, 480),
      new Point(786, 780)
    ];
    var outline = new Outline(32, 67, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1244, 2507, 1),
      new OutlinePoint(1311, 2507, 2),
      new OutlinePoint(1378, 2467, 2),
      new OutlinePoint(1465, 2400, 1),
      new OutlinePoint(1378, 2342, 1),
      new OutlinePoint(1378, 547, 1),
      new OutlinePoint(1378, 480, 2),
      new OutlinePoint(1277, 413, 2),
      new OutlinePoint(1177, 413, 1),
      new OutlinePoint(1177, 447, 2),
      new OutlinePoint(1032, 614, 2),
      new OutlinePoint(770, 764, 1),
      new OutlinePoint(786, 780, 1),
      new OutlinePoint(1048, 630, 2),
      new OutlinePoint(1244, 594, 2),
      new OutlinePoint(1244, 614, 1)
    ];

    ShuGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
