/* eslint-env mocha */
var assert = require('chai').assert;
var Utils = require('../src/Utils');

describe('Utils', function() {
  describe('skPercent', function() {
    it('should return the same output as SK_PERCENT in ch_zzsol.c case #1', function() {
      assert.strictEqual(Utils.skPercent(115, 115), 132);
    });

    it('should return the same output as SK_PERCENT in ch_zzsol.c case #2', function() {
      assert.strictEqual(Utils.skPercent(61, 170), 104);
    });

    it('should return the same output as SK_PERCENT in ch_zzsol.c case #3', function() {
      assert.strictEqual(Utils.skPercent(638, 80), 511);
    });

    it('should return the same output as SK_PERCENT in ch_zzsol.c case #4', function() {
      assert.strictEqual(Utils.skPercent(460, 43), 198);
    });
  });

  describe('xyScaleMul', function() {
    it('should return the same output as XY_SCALE_MUL in ch_zzsol.c case #1', function() {
      assert.strictEqual(Utils.xyScaleMul(54, 2779), 1172);
    });

    it('should return the same output as XY_SCALE_MUL in ch_zzsol.c case #2', function() {
      assert.strictEqual(Utils.xyScaleMul(22, 2731), 469);
    });

    it('should return the same output as XY_SCALE_MUL in ch_zzsol.c case #3', function() {
      assert.strictEqual(Utils.xyScaleMul(106, 2779), 2301);
    });

    it('should return the same output as XY_SCALE_MUL in ch_zzsol.c case #4', function() {
      assert.strictEqual(Utils.xyScaleMul(68, 2731), 1450);
    });
  });

  describe('xyScaleMulFloat', function() {
    it('should return the same output as XY_SCALE_MUL for floats in ch_zzsol.c case #1', function() {
      assert.strictEqual(Utils.xyScaleMulFloat(80, 518), 323.75);
    });
  });
});
