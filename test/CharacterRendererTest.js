/* eslint-env mocha */
var assert = require('chai').assert;
var CharacterRenderer = require('../src/CharacterRenderer');
var CdlStroke = require('../src/CdlStroke');
var CdlChar = require('../src/CdlChar');
var RenderBounds = require('../src/RenderBounds');
var OutlinePoint = require('../src/OutlinePoint');
var Point = require('../src/Point');
var SongSvgRenderer = require('../src/SongSvgRenderer');

describe('CharacterRenderer', function() {
  describe('calculateStrokeRadii', function() {
    var hengRadiusFactor = 13;
    var shuRadiusFactor = 37;

    it('should return the same output as CalculateStrokeRadii in ch_zzsol.c case #1', function() {
      var expectedOutput = {heng: 32, shu: 67};
      var actualOutput = CharacterRenderer.calculateStrokeRadii(hengRadiusFactor, shuRadiusFactor, 30, 3);
      assert.deepEqual(actualOutput, expectedOutput);
    });

    it('should return the same output as CalculateStrokeRadii in ch_zzsol.c case #2', function() {
      var expectedOutput = {heng: 32, shu: 63};
      var actualOutput = CharacterRenderer.calculateStrokeRadii(hengRadiusFactor, shuRadiusFactor, 30, 7);
      assert.deepEqual(actualOutput, expectedOutput);
    });

    it('should return the same output as CalculateStrokeRadii in ch_zzsol.c case #3', function() {
      var expectedOutput = {heng: 32, shu: 62};
      var actualOutput = CharacterRenderer.calculateStrokeRadii(hengRadiusFactor, shuRadiusFactor, 30, 8);
      assert.deepEqual(actualOutput, expectedOutput);
    });
  });

  describe('renderStrokeOutline', function() {
    it('should return the same output as WriteOutlineStroke in ch_zzstr.c case #1', function() {
      var bounds = new RenderBounds(2444.8, 2444.8, 21094.4, 21094.4);
      var origBounds = new RenderBounds(2444.8, 2444.8, 21094.4, 21094.4);
      var stroke = new CdlStroke('p', [
        new Point(60, 0),
        new Point(0, 128)
      ], null, CdlStroke.STROKE_TAIL_LONG);
      var points = [
        new Point(12332.8, 2444.8),
        new Point(2444.8, 23539.2)
      ];
      var radii = {
        heng: 256,
        shu: 752
      };

      var expectedOutput = [
        new OutlinePoint(11708, 24276, 1),
        new OutlinePoint(12566, 24083, 2),
        new OutlinePoint(13490, 23667, 2),
        new OutlinePoint(13821, 23251, 1),
        new OutlinePoint(12988, 22701, 1),
        new OutlinePoint(12988, 22701, 1),
        new OutlinePoint(11071, 12808, 2),
        new OutlinePoint(6540, 5223, 2),
        new OutlinePoint(1836, 1596, 1),
        new OutlinePoint(1708, 1724, 1),
        new OutlinePoint(5926, 5600, 2),
        new OutlinePoint(9989, 13704, 2),
        new OutlinePoint(11708, 24276, 1)
      ];

      var output = CharacterRenderer.renderStrokeOutline(stroke, radii, bounds, origBounds, points);
      assert.deepEqual(output, expectedOutput);
    });
  });

  describe('getSvgPathStrings', function() {
    it('should return the same output as StrokeMiaomiaoAZ in ch_zzstr.c case #1', function() {
      var bounds = new RenderBounds(8, 12, 52, 52);
      var cdlChar = new CdlChar('人', '4EBA', [
        new CdlStroke('p', [
          new Point(60, 0),
          new Point(0, 128)
        ], null, CdlStroke.STROKE_TAIL_LONG),
        new CdlStroke('n', [
          new Point(54, 32),
          new Point(128, 128)
        ], CdlStroke.STROKE_HEAD_CUT)
      ]);

      var expectedStrokes = [];
      expectedStrokes.push(SongSvgRenderer.getPathString([
        new OutlinePoint(1988, 4238, 1),
        new OutlinePoint(2122, 4208, 2),
        new OutlinePoint(2266, 4143, 2),
        new OutlinePoint(2318, 4078, 1),
        new OutlinePoint(2188, 3992, 1),
        new OutlinePoint(2188, 3992, 1),
        new OutlinePoint(1885, 2432, 2),
        new OutlinePoint(1169, 1237, 2),
        new OutlinePoint(426, 666, 1),
        new OutlinePoint(410, 682, 1),
        new OutlinePoint(1075, 1293, 2),
        new OutlinePoint(1716, 2571, 2),
        new OutlinePoint(1988, 4238, 1)
      ], bounds.y, bounds.height));
      expectedStrokes.push(SongSvgRenderer.getPathString([
        new OutlinePoint(1932, 3296, 1),
        new OutlinePoint(2296, 2149, 2),
        new OutlinePoint(3123, 1114, 2),
        new OutlinePoint(4051, 930, 1),
        new OutlinePoint(4051, 901, 1),
        new OutlinePoint(3913, 863, 2),
        new OutlinePoint(3822, 802, 2),
        new OutlinePoint(3726, 703, 1),
        new OutlinePoint(2928, 1146, 2),
        new OutlinePoint(2216, 2032, 2),
        new OutlinePoint(1903, 3282, 1)
      ], bounds.y, bounds.height));

      var output = CharacterRenderer.getSvgPathStrings(cdlChar, bounds, {fixMargin: false});
      assert.deepEqual(output, expectedStrokes);
    });
  });
});
