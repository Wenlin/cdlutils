var OutlineHelpers = require('../OutlineHelpers');
var OutlinePoint = require('../OutlinePoint');
var Point = require('../Point');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[1]);
    var curveStartPoint = outline.getLastOutlinePoint().toPoint();
    var curveEndPoint = new Point(
      refPoints[1].x - outline.shuRadius,
      refPoints[1].y - outline.hengRadius
    );
    OutlineHelpers.addXieGouExceptTop(outline, curveStartPoint, refPoints[2], refPoints[3], curveEndPoint);
    outline.addPoint(curveEndPoint, OutlinePoint.ON_CURVE);
  }
};
