var OutlineHelpers = require('../OutlineHelpers');
var CdlStroke = require('../CdlStroke');

module.exports = {
  addPoints: function(outline, refPoints) {
    /* The head is always cut. */
    outline.head = CdlStroke.STROKE_HEAD_CUT;
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    var kLeft  = refPoints[2].clone();
    var kRight = refPoints[2].clone();
    kRight.x += outline.shuRadius;
    kRight.y += outline.hengRadius;

    OutlineHelpers.addHengPieOutlineCornerPoints(outline, refPoints[1]);
    OutlineHelpers.addWanGouStuff(outline, kLeft, kRight, refPoints.slice(3), false /* don't fix hook */);
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[1]);
  }
};
