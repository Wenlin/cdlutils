var Utils = require('../Utils');
var OutlinePoint = require('../OutlinePoint');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var hrad = outline.hengRadius;
    var srad = outline.shuRadius;
    var delta = Utils.skPercent(srad, 30); // shu segment wants a long tail

    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);

    var hangover = hrad + Utils.skPercent(srad, 80);
    var y = refPoints[1].y - hangover - srad;
    outline.addPoint(refPoints[1].x + srad, y + srad, OutlinePoint.ON_CURVE);
    outline.addPoint(refPoints[1].x + delta, y + delta, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(refPoints[1].x - delta, y, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(refPoints[1].x - srad, y, OutlinePoint.ON_CURVE);
    outline.addPoint(refPoints[1].x - srad, refPoints[1].y - hrad, OutlinePoint.ON_CURVE);
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[2]);
    outline.addPoint(refPoints[1].x - srad, refPoints[1].y + hrad, OutlinePoint.ON_CURVE);
  }
};
