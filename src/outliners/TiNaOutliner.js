var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var topLeft = refPoints[1].clone();
    var topRight = topLeft.clone();
    topLeft.y -= outline.hengRadius;
    topRight.y += outline.hengRadius;
    OutlineHelpers.addNaStuff(outline, topRight, topLeft, refPoints[2]);
    OutlineHelpers.addSouthwestWideEndPoints(outline, refPoints[0]);
  }
};
