var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addShuTiBottomPoints(outline, refPoints[1], refPoints[2]);
  }
};
