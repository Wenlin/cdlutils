/* eslint-env mocha */
var assert = require('chai').assert;
var TiOutliner = require('../../src/outliners/TiOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('TiOutliner#addPoints', function() {
  it('should return the same output as GetTiOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(612, 956),
      new Point(1204, 2490)
    ];
    var outline = new Outline(32, 110, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1204, 2490, 1),
      new OutlinePoint(1204, 2435, 1),
      new OutlinePoint(722, 879, 1),
      new OutlinePoint(634, 846, 2),
      new OutlinePoint(513, 868, 2),
      new OutlinePoint(425, 1165, 1),
      new OutlinePoint(502, 1121, 1)
    ];

    TiOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetTiOutline in ch_zzsol.c case #2', function() {
    var refPoints = [
      new Point(528, 1112),
      new Point(2192, 1580)
    ];
    var outline = new Outline(32, 108, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2192, 1580, 1),
      new OutlinePoint(2192, 1526, 1),
      new OutlinePoint(528, 1004, 1),
      new OutlinePoint(517, 1015, 2),
      new OutlinePoint(431, 1058, 2),
      new OutlinePoint(377, 1296, 1),
      new OutlinePoint(452, 1274, 1)
    ];

    TiOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
