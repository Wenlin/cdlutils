var Utils = require('../Utils');
var OutlineHelpers = require('../OutlineHelpers');
var OutlinePoint = require('../OutlinePoint');
var Point = require('../Point');

module.exports = {
  addPoints: function(outline, refPoints) {
    var rad = outline.shuRadius; // pieRadius

    var top    = refPoints[0].clone();
    var corner = refPoints[1].clone();
    var right  = refPoints[2].clone();
    var pieOutsideTop;
    var pieInsideTop;
    var gotTop = false;
    if (outline.isHeadCut()) {
      /* Top cut off flat horizontally */
      pieOutsideTop = new Point(top.x - rad, top.y);
      pieInsideTop = new Point(top.x + rad, top.y);
    } else {
      OutlineHelpers.addPieZheTopOutlinePoints(outline, top);
      pieInsideTop  = outline.getLastOutlinePoint().toPoint();
      pieOutsideTop = outline.getFirstOutlinePoint().toPoint();
      gotTop = true;
    }
    var pieInsideBottom = new Point(corner.x + Utils.intCast(rad / 2), corner.y + rad);
    var pieOutsideBottom = new Point(pieInsideBottom.x - Utils.intCast(rad / 2), pieInsideBottom.y + Utils.intCast(rad / 2));

    var left = new Point(
      corner.x - Utils.skPercent(rad, 140),
      pieInsideBottom.y - Utils.intCast(rad / 4)
    );

    var outsidePoints = OutlineHelpers.getPieControlPoints(pieOutsideTop, pieOutsideBottom, false);
    var insidePoints = OutlineHelpers.getPieControlPoints(pieInsideTop, pieInsideBottom, false);

    var leftAY = Utils.intCast((left.y + pieOutsideBottom.y) / 2);
    var leftA;
    /* Make leftA colinear with old outsidePoints.bottom and pieOutsideBottom */
    if (outsidePoints.bottom.y === pieOutsideBottom.y) { // don't divide by zero
      leftA = new Point(pieOutsideBottom.x, leftAY);
    } else {
      var numerator = (pieOutsideBottom.y - leftAY) * (outsidePoints.bottom.x - pieOutsideBottom.x);
      leftA = new Point(
        pieOutsideBottom.x - Utils.intCast(numerator / (outsidePoints.bottom.y - pieOutsideBottom.y)),
        leftAY
      );
    }
    var leftB = new Point(Utils.intCast((3 * left.x + pieOutsideBottom.x) / 4), left.y);

    var cake = new Point(corner.x, corner.y - rad);
    var cheese = new Point(cake.x + rad, cake.y + Utils.intCast(rad / 2));

    if (!gotTop) {
      outline.addPoint(pieInsideTop, OutlinePoint.ON_CURVE);
    }
    outline.addPoint(insidePoints.top, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(insidePoints.bottom, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(pieInsideBottom, OutlinePoint.ON_CURVE);
    outline.addPoint(right.x, right.y + outline.hengRadius, OutlinePoint.ON_CURVE);
    outline.addPoint(right.x, right.y - outline.hengRadius, OutlinePoint.ON_CURVE);
    outline.addPoint(cheese, OutlinePoint.ON_CURVE);
    outline.addPoint(cake, OutlinePoint.ON_CURVE);
    outline.addPoint(left, OutlinePoint.ON_CURVE);
    outline.addPoint(leftB, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(leftA, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(pieOutsideBottom, OutlinePoint.ON_CURVE);
    outline.addPoint(outsidePoints.bottom, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(outsidePoints.top, OutlinePoint.OFF_CURVE_CUBIC);
    if (!gotTop) {
      outline.addPoint(pieOutsideTop, OutlinePoint.ON_CURVE);
    }
  }
};
