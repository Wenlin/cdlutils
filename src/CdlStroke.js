var getLabelForTail = function(tail) {
  if (tail === CdlStroke.STROKE_TAIL_NORMAL) return 'normal';
  if (tail === CdlStroke.STROKE_TAIL_CUT) return 'cut';
  if (tail === CdlStroke.STROKE_TAIL_LONG) return 'long';
  if (tail === CdlStroke.STROKE_TAIL_RESERVED) return 'reserved';
};

var getLabelForHead = function(head) {
  if (head === CdlStroke.STROKE_HEAD_NORMAL) return 'normal';
  if (head === CdlStroke.STROKE_HEAD_CUT) return 'cut';
  if (head === CdlStroke.STROKE_HEAD_CORNER) return 'corner';
  if (head === CdlStroke.STROKE_HEAD_VERTICAL) return 'vertical';
};

var CdlStroke = function(type, points, head, tail) {
  this.type = type;
  this.points = points;
  this.head = head || CdlStroke.STROKE_HEAD_NORMAL;
  this.tail = tail || CdlStroke.STROKE_TAIL_NORMAL;
  this.isStroke = true;
};

// ------ consts ------

CdlStroke.STROKE_HEAD_NORMAL   = 0; /* cdl: no 'head' attribute */
CdlStroke.STROKE_HEAD_CUT      = 1; /* cdl: head='cut' */
CdlStroke.STROKE_HEAD_CORNER   = 2; /* cdl: head='corner' */
CdlStroke.STROKE_HEAD_VERTICAL = 3; /* cdl: head='vertical' */

// tail (cut|long)
CdlStroke.STROKE_TAIL_NORMAL   = 0; /* cdl: no 'tail' attribute */
CdlStroke.STROKE_TAIL_CUT      = 1; /* cdl: head='cut' */
CdlStroke.STROKE_TAIL_LONG     = 2; /* cdl: head='long' */
CdlStroke.STROKE_TAIL_RESERVED = 3;

// ------ getters ------

CdlStroke.prototype.toRawJs = function() {
  var points = this.points.map(function(point) {
    return point.x + ',' + point.y;
  }).join(' ');
  var rawJs = {$tag: 'stroke', type: this.type, points: points};
  if (this.head) rawJs.head = getLabelForHead(this.head);
  if (this.tail) rawJs.tail = getLabelForTail(this.tail);
  return rawJs;
};

module.exports = CdlStroke;
