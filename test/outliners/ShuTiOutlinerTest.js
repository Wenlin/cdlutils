/* eslint-env mocha */
var assert = require('chai').assert;
var ShuTiOutliner = require('../../src/outliners/ShuTiOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuTiOutliner#addPoints', function() {
  it('should return the same output as GetShuTiOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1360, 2912),
      new Point(1360, 800),
      new Point(2257, 1080)
    ];
    var outline = new Outline(32, 106, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1254, 3082, 1),
      new OutlinePoint(1360, 3082, 2),
      new OutlinePoint(1466, 3018, 2),
      new OutlinePoint(1604, 2912, 1),
      new OutlinePoint(1466, 2821, 1),
      new OutlinePoint(1466, 1054, 1),
      new OutlinePoint(2231, 1106, 1),
      new OutlinePoint(2257, 1080, 1),
      new OutlinePoint(1466, 768, 1),
      new OutlinePoint(1360, 628, 1),
      new OutlinePoint(1271, 662, 2),
      new OutlinePoint(1201, 719, 2),
      new OutlinePoint(1148, 800, 1),
      new OutlinePoint(1254, 906, 1)
    ];

    ShuTiOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
