var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addXieGouExceptTop(outline, outline.getLastOutlinePoint().toPoint(), refPoints[1],
        refPoints[2], outline.getFirstOutlinePoint().toPoint());
  }
};
