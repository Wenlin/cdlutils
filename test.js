/* eslint-env mocha */
// require.context is used by webpack to require full directories
var req = require.context('./test', true, /\.js$/);
req.keys().forEach(function(key) {
  req(key);
});

mocha.run();
