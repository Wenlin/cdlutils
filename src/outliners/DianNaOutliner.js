var Utils = require('../Utils');
var Point = require('../Point');
var OutlinePoint = require('../OutlinePoint');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    // where dian and na meet
    var midLeft = new Point(
      refPoints[1].x - outline.shuRadius,
      refPoints[1].y - outline.hengRadius
    );

    var midRight = refPoints[1].clone();
    midRight.x += outline.shuRadius;

    var naBottomPoints = OutlineHelpers.prepareNaEndPoints(outline, refPoints[2]);
    var naRightCtrlPoints = OutlineHelpers.interpolateNaPoints(midRight, naBottomPoints.right, true);
    var naLeftCtrlPoints = OutlineHelpers.interpolateNaPoints(midLeft, naBottomPoints.left, false);

    var top = new Point(
      refPoints[0].x + outline.shuRadius, // dianRadius
      refPoints[0].y + outline.hengRadius
    );

    var left = refPoints[0].clone();
    left.x -= outline.shuRadius;
    outline.addPoint(left, OutlinePoint.ON_CURVE);

    var ctrlA = new Point(left.x, Utils.intCast((left.y + top.y) / 2));
    var ctrlB = new Point(Utils.intCast((left.x + top.x) / 2), top.y);
    outline.addPoint(ctrlA, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlB, OutlinePoint.OFF_CURVE_CUBIC);

    outline.addPoint(top, OutlinePoint.ON_CURVE);

    ctrlB.y = Utils.intCast((top.y + midRight.y) / 2);
    /* Want (ctrlB.y - midRight.y) / (midRight.x - ctrlB.x)
            == (midRight.y - naRightCtrlPoints[0].y) / (naRightCtrlPoints[0].x - midRight.x) */
    if (midRight.y === naRightCtrlPoints[0].y) {
      ctrlB.x = midRight.x; // don't divide by zero
    } else {
      var numerator = (ctrlB.y - midRight.y) * (naRightCtrlPoints[0].x - midRight.x);
      ctrlB.x = midRight.x - Utils.intCast(numerator / (midRight.y - naRightCtrlPoints[0].y));
    }
    ctrlA.y = top.y;
    ctrlA.x = Utils.intCast((top.x + ctrlB.x) / 2);
    outline.addPoint(ctrlA, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlB, OutlinePoint.OFF_CURVE_CUBIC);

    outline.addPoint(midRight, OutlinePoint.ON_CURVE);
    outline.addPoint(naRightCtrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(naRightCtrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    var naEndPoints = OutlineHelpers.interpolateNaEndPoints(naBottomPoints.right, naBottomPoints.left);
    outline.addPoint(naBottomPoints.right, OutlinePoint.ON_CURVE);
    outline.addPoint(naEndPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(naEndPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(naBottomPoints.left, OutlinePoint.ON_CURVE);
    outline.addPoint(naLeftCtrlPoints[1], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(naLeftCtrlPoints[0], OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(midLeft, OutlinePoint.ON_CURVE);

    ctrlA.x = left.x;
    ctrlA.y = left.y - outline.hengRadius;
    ctrlB.y = ctrlA.y;

    /* Want (midLeft.y - ctrlB.y) / (midLeft.x - ctrlB.x)
            == (naLeftCtrlPoints[0].y - midLeft.y) / (naLeftCtrlPoints[0].x - midLeft.x) */
    ctrlB.x = midLeft.x - Utils.intCast(((naLeftCtrlPoints[0].x - midLeft.x) * (midLeft.y - ctrlB.y)) / (naLeftCtrlPoints[0].y - midLeft.y));
    outline.addPoint(ctrlB, OutlinePoint.OFF_CURVE_CUBIC);
    outline.addPoint(ctrlA, OutlinePoint.OFF_CURVE_CUBIC);
  }
};
