var Utils = require('../Utils');
var OutlineHelpers = require('../OutlineHelpers');
var Point = require('../Point');

module.exports = {
  addPoints: function(outline, refPoints) {
    var topLeft;
    var topRight;
    if (!outline.isHeadCut()
        && (refPoints[1].x - refPoints[0].x) * 3
        < (refPoints[0].y - refPoints[1].y) * 4) {
      /* as in ba1 'eight', fen1 'divide', gua3 'few' for example. */
      OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
      topLeft = outline.getFirstOutlinePoint();
      topRight = outline.getLastOutlinePoint().toPoint();
      outline.deleteLastPoint();
      /* Must make sure topRight isn't to the left
           of the curve starting at topLeft. (Problem in gua3 'few'.)
           For simplicity, just look at the line from topLeft to refPoints[1]. */
      if (topLeft.y <= refPoints[1].y) {
        topRight.y = refPoints[1].y + outline.hengRadius;
      } else {
        var minX = topLeft.x + ((topLeft.y - topRight.y) * (refPoints[1].x - topLeft.x)) / (topLeft.y - refPoints[1].y);
        if (topRight.x < minX) {
          topRight.x = minX;
        }
      }
    } else {
      topLeft = new Point(
        refPoints[0].x - Utils.intCast(outline.shuRadius / 4),
        refPoints[0].y - Utils.intCast(outline.shuRadius / 8)
      );
      topRight = refPoints[0];
    }
    OutlineHelpers.addNaStuff(outline, topRight, topLeft, refPoints[1]);
  }
};
