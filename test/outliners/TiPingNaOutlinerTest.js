/* eslint-env mocha */
var assert = require('chai').assert;
var TiPingNaOutliner = require('../../src/outliners/TiPingNaOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('TiPingNaOutliner#addPoints', function() {
  it('should return the same output as GetTiPingNaOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(528, 995),
      new Point(1100, 1336),
      new Point(2816, 800),
      new Point(3856, 800)
    ];
    var outline = new Outline(32, 110, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1100, 1457, 1),
      new OutlinePoint(1529, 921, 2),
      new OutlinePoint(2816, 921, 2),
      new OutlinePoint(4098, 1042, 1),
      new OutlinePoint(4098, 1015, 1),
      new OutlinePoint(3944, 952, 2),
      new OutlinePoint(3842, 847, 2),
      new OutlinePoint(3735, 679, 1),
      new OutlinePoint(2816, 679, 2),
      new OutlinePoint(1529, 679, 2),
      new OutlinePoint(1100, 1336, 1),
      new OutlinePoint(528, 817, 1),
      new OutlinePoint(436, 852, 2),
      new OutlinePoint(363, 911, 2),
      new OutlinePoint(308, 995, 1)
    ];

    TiPingNaOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
