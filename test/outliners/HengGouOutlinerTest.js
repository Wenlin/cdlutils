/* eslint-env mocha */
var assert = require('chai').assert;
var HengGouOutliner = require('../../src/outliners/HengGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengGouOutliner#addPoints', function() {
  it('should return the same output as GetHengGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(531, 2081),
      new Point(2256, 2081),
      new Point(2046, 1810)
    ];
    var outline = new Outline(32, 64, 1, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(531, 2049, 1),
      new OutlinePoint(531, 2113, 1),
      new OutlinePoint(2128, 2113, 1),
      new OutlinePoint(2212, 2209, 1),
      new OutlinePoint(2328, 2107, 2),
      new OutlinePoint(2346, 2049, 2),
      new OutlinePoint(2346, 2004, 1),
      new OutlinePoint(2263, 1991, 2),
      new OutlinePoint(2102, 1836, 2),
      new OutlinePoint(2046, 1810, 1),
      new OutlinePoint(2030, 1794, 1),
      new OutlinePoint(2141, 2049, 1)
    ];

    HengGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
