var Utils = require('../Utils');
var Point = require('../Point');
var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    var topLeft = new Point(
      refPoints[0].x - Utils.intCast(outline.shuRadius / 4),
      refPoints[0].y - Utils.intCast(outline.shuRadius / 4)
    );

    OutlineHelpers.addWanGouStuff(outline, topLeft, refPoints[0], refPoints.splice(1), true);
  }
};
