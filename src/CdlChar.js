var Point = require('./Point');
var Utils = require('./Utils');

var CdlChar = function(char, unicode, components, points, radical, variant) {
  this.char = char;
  this.unicode = unicode;
  this.components = components;
  this.points = points;
  this.radical = radical;
  this.variant = variant || 0;
  this.isScaled = !!this.points;
  this.isStroke = false;
  if (!this.points) {
    this.points = [
      new Point(0, 0),
      new Point(128, 128)
    ];
  }
};

// ----- getters -----

CdlChar.prototype.getNumStrokes = function() {
  var numStrokes = 0;
  this.components.forEach(function(component) {
    if (component.isStroke) {
      numStrokes += 1;
    } else {
      numStrokes += component.getNumStrokes();
    }
  });
  return numStrokes;
};

// options:
// recursive - whether or not to embed all the children chars. default false
CdlChar.prototype.toXml = function(options) {
  var rawJs = this.toRawJs(options);
  return Utils.xmlFromRawJs(rawJs);
};

CdlChar.prototype.toRawJs = function(options) {
  if (!options) options = {};
  var includeChildren = !options.shallow;
  if (!options.recursive) {
    options.shallow = true;
  }
  var tagName = options.tagName || 'cdl';
  options.tagName = 'comp';
  var rawJs = {$tag: tagName};
  if (this.char) {
    rawJs.char = this.char;
  }
  rawJs.uni = this.unicode;
  if (this.isScaled) {
    rawJs.points = this.points.map(function(point) {
      return point.x + ',' + point.y;
    }).join(' ');
  }
  if (this.variant) {
    rawJs.variant = this.variant;
  }
  if (this.radical && includeChildren) {
    rawJs.radical = this.radical;
  }
  if (includeChildren) {
    rawJs.$children = this.components.map(function(component) {
      return component.toRawJs(options);
    });
  }
  return rawJs;
};

module.exports = CdlChar;
