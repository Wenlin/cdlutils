var OutlinePoint = require('./OutlinePoint');
var CdlStroke = require('./CdlStroke');
var Utils = require('./Utils');

var Outline = function(hengRadius, shuRadius, head, tail) {
  this.outlinePoints = [];
  this.hengRadius = hengRadius;
  this.shuRadius = shuRadius;
  this.head = head || CdlStroke.STROKE_HEAD_NORMAL;
  this.tail = tail || CdlStroke.STROKE_TAIL_NORMAL;
};

// ----- getters -----

Outline.prototype.isHeadCut = function() { return this.head === CdlStroke.STROKE_HEAD_CUT; };
Outline.prototype.isTailCut = function() { return this.tail === CdlStroke.STROKE_TAIL_CUT; };
Outline.prototype.isTailLong = function() { return this.tail === CdlStroke.STROKE_TAIL_LONG; };

Outline.prototype.getFirstOutlinePoint = function() { return this.outlinePoints[0]; };
Outline.prototype.getLastOutlinePoint = function() { return this.outlinePoints[this.outlinePoints.length - 1]; };

// ----- mutators -----

// accepts either:
//  x, y, and flag args
// or a Point and a flag arg
// or a single OutlinePoint arg
Outline.prototype.addPoint = function() {
  if (arguments.length === 3) {
    this.outlinePoints.push(new OutlinePoint(arguments[0], arguments[1], arguments[2]));
  } else if (arguments.length === 2) {
    this.outlinePoints.push(new OutlinePoint(arguments[0].x, arguments[0].y, arguments[1]));
  } else {
    this.outlinePoints.push(arguments[0]);
  }
};

Outline.prototype.deleteLastPoint = function() {
  this.outlinePoints.pop();
};

Outline.prototype.rotateAndTranslate = function(origin, dx, dy, hypotenuse) {
  this.outlinePoints.forEach(function(point) {
    var x = point.x;
    var y = point.y;
    point.x = origin.x + Utils.intCast((x * dx - y * dy) / hypotenuse);
    point.y = origin.y + Utils.intCast((y * dx + x * dy) / hypotenuse);
  });
};

module.exports = Outline;
