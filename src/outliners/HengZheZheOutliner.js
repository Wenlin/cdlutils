var OutlineHelpers = require('../OutlineHelpers');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[1]);
    OutlineHelpers.addNortheastCornerPoint(outline, refPoints[2]);
    OutlineHelpers.addHengRightEndOutlinePoints(outline, refPoints[2], refPoints[3]);
    OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[2]);
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[1]);
  }
};
