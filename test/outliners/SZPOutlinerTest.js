/* eslint-env mocha */
var assert = require('chai').assert;
var SZPOutliner = require('../../src/outliners/SZPOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('SZPOutliner#addPoints', function() {
  it('should return the same output as GetSZPOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2634, 4128),
      new Point(2061, 2152),
      new Point(3359, 2152),
      new Point(2710, 1268)
    ];
    var outline = new Outline(32, 111, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2523, 4306, 1),
      new OutlinePoint(2634, 4306, 2),
      new OutlinePoint(2745, 4239, 2),
      new OutlinePoint(2889, 4128, 1),
      new OutlinePoint(2745, 4033, 1),
      new OutlinePoint(2172, 2184, 1),
      new OutlinePoint(3137, 2184, 1),
      new OutlinePoint(3284, 2374, 1),
      new OutlinePoint(3483, 2196, 2),
      new OutlinePoint(3514, 2097, 2),
      new OutlinePoint(3514, 2019, 1),
      new OutlinePoint(3370, 1997, 2),
      new OutlinePoint(2808, 1312, 2),
      new OutlinePoint(2710, 1268, 1),
      new OutlinePoint(2683, 1241, 1),
      new OutlinePoint(3159, 2120, 1),
      new OutlinePoint(2172, 2120, 1),
      new OutlinePoint(2061, 1972, 1),
      new OutlinePoint(1968, 2008, 2),
      new OutlinePoint(1894, 2067, 2),
      new OutlinePoint(1839, 2152, 1),
      new OutlinePoint(1950, 2263, 1)
    ];

    SZPOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
