var OutlinePoint = require('./OutlinePoint');
var Utils = require('./Utils');

module.exports = {
  getPathString: function(outlinePoints, y, height) {
    if (outlinePoints.length === 0) {
      return null;
    }
    // endpoint same as starting point -- needed for 'pz' at least
    var end = new OutlinePoint(outlinePoints[0].x, outlinePoints[0].y, OutlinePoint.ON_CURVE);
    outlinePoints.push(end);

    /* Divide by 64, to make up for FreeType conversion (which was << FT_SHIFT).
        Flip the y value around the midpoint, since in SVG the y coordinate increases downward.
        The midpoint is yMid = y + height / 2.
        The transformation is yNew = 2 * yMid - yOld,
            which is the same as yNew = flipHeight - yOld,
            where flipHeight = 2 * yMid = y * 2 + height. */
    var flipHeight = y * 2 + height;

    var svgSongX = function(xVal) { return xVal / 64; };
    var svgSongY = function(yVal) { return (flipHeight - yVal) / 64; };

    var pathString = Utils.getSvgPathStartString(
      svgSongX(outlinePoints[0].x),
      svgSongY(outlinePoints[0].y)
    );
    for (var i = 1; i < outlinePoints.length;) {
      if (outlinePoints[i].flag === OutlinePoint.ON_CURVE) {
        pathString += Utils.getSvgLineString(
          svgSongX(outlinePoints[i].x),
          svgSongY(outlinePoints[i].y)
        );
        i += 1;
      } else if (outlinePoints[i].flag === OutlinePoint.OFF_CURVE_QUADRATIC) {
        pathString += Utils.getSvgQuadraticCurveString(
          svgSongX(outlinePoints[i + 0].x), svgSongY(outlinePoints[i + 0].y),
          svgSongX(outlinePoints[i + 1].x), svgSongY(outlinePoints[i + 1].y)
        );
        i += 2;
      } else if (outlinePoints[i].flag === OutlinePoint.OFF_CURVE_CUBIC) {
        pathString += Utils.getSvgCubicCurveString(
          svgSongX(outlinePoints[i + 0].x), svgSongY(outlinePoints[i + 0].y),
          svgSongX(outlinePoints[i + 1].x), svgSongY(outlinePoints[i + 1].y),
          svgSongX(outlinePoints[i + 2].x), svgSongY(outlinePoints[i + 2].y)
        );
        i += 3;
      } else {
        break;
      }
    }
    return pathString + '\tz';
  }
};
