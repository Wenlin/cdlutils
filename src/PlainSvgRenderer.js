var Utils = require('./Utils');
var Point = require('./Point');

// TODO: add path and o types
var strokeSegments = {
  'h': ['h'],
  't': ['L'],
  's': ['s'],
  'sg': ['s', 'L'],
  'p': ['p'],
  'wp': ['P'],
  'sp': ['s', 'P'],
  'd': ['d'],
  'n': ['n'],
  'dn': ['d', 'n'],
  'pn': ['c', 'h'],
  'tn': ['L', 'n'],
  'tpn': ['L', 'c', 'h'],
  'hz': ['h', 's'],
  'hp': ['h', 'p'],
  'hg': ['h', 'L'],
  'sz': ['s', 'h'],
  'sw': ['s', 'c', 'h'],
  'st': ['s', 'L'],
  'pz': ['p', 'L'],
  'pd': ['p', 'd'],
  'pg': ['p', 'L'],
  'wg': ['d', 'P', 'L'],
  'xg': ['N', 's'],
  'hzz': ['h', 's', 'h'],
  'hzw': ['h', 's', 'c', 'h'],
  'hzt': ['h', 's', 'L'],
  'hzg': ['h', 'p', 'L'],
  'hxg': ['h', 'N', 'L'],
  'szz': ['s', 'h', 's'],
  'szp': ['L', 'h', 'L'],
  'swg': ['s', 'c', 'h', 's'],
  'hzzz': ['h', 's', 'h', 's'],
  'hzzp': ['h', 'L', 'h', 'p'],
  'hzwg': ['h', 'L', 'c', 'h', 's'],
  'hpwg': ['h', 'p', 'd', 'P', 'L'],
  'szzg': ['s', 'h', 'P', 'L'],
  'hzzzg': ['h', 'L', 'h', 'P', 'L'],
  'b': ['z'],
  'c': ['c'],
  'bd': ['B'],
  'swz': ['s', 'L'],
  'bxg': ['s', 'c', 'h', 's'],
  'hxwg': ['h', 'L', 'c', 'h', 's']
};

var SZPAlternateSegs = ['L', 'h', 'p'];
var HZWGAlternateSegs = ['h', 'L', 'B', 'c', 'h', 's'];

var svgPlainX = function(val) {
  return val / 64.0;
};
var svgPlainY = svgPlainX;

var typeCInterpolateCurveXY = function(curvePoints, pointA, pointB) {
  /* Support all four quadrants. Direction is counter-clockwise from (xa, ya) to (xb, yb).
      Endpoints of segment determine quadrant.
      If we do this right, U+3007 〇 should not be lopsided!
      Watch out for rounding errors (no pun intended). */
  if ((pointA.x < pointB.x && pointA.y < pointB.y) || (pointA.x > pointB.x && pointA.y > pointB.y)) {
    // bottom-left or top-right quadrant
    curvePoints[1] = new Point(pointA.x, (pointA.y + pointB.y) / 2);
    curvePoints[2] = new Point((pointA.x + pointB.x) / 2, pointB.y);
  } else { // bottom-right or top-left quadrant
    curvePoints[1] = new Point((pointA.x + pointB.x) / 2, pointA.y);
    curvePoints[2] = new Point(pointB.x, (pointA.y + pointB.y) / 2);
  }
  return curvePoints;
};

var interpolateCurveXY = function(pointA, pointB, segType) {
  var curvePoints = [];
  curvePoints[0] = pointA.clone();
  curvePoints[3] = pointB.clone();
  if (segType === 'c') {
    return typeCInterpolateCurveXY(curvePoints, pointA, pointB);
  }
  var interpolations = {
    d: [80, 36, 124, 80],
    B: [46, 36, 4, 80], // bug fix 2017-12-1, was lowercase b, caused 'Invalid Segtype' warning.
    p: [106, 60, 54, 106],
    n: [22, 62, 72, 106],
    P: [128, 76, 104, 98],
    N: [0, 76, 24, 98]
  };
  var xy = interpolations[segType];
  if (!xy) {
    throw new Error('Invalid Segtype ' + segType);
  }
  if (pointA.x > pointB.x) { /* do this AFTER assigning curvex[0], etc. */
    Point.swapXs(pointA, pointB);
  }
  if (pointA.y > pointB.y) {
    Point.swapYs(pointA, pointB);
  }
  var wid = pointB.x - pointA.x;
  var hi =  pointB.y - pointA.y;
  curvePoints[1] = new Point(
    pointA.x + Utils.xyScaleMulFloat(xy[0], wid),
    pointA.y + Utils.xyScaleMulFloat(xy[1], hi)
  );
  curvePoints[2] = new Point(
    pointA.x + Utils.xyScaleMulFloat(xy[2], wid),
    pointA.y + Utils.xyScaleMulFloat(xy[3], hi)
  );
  return curvePoints;
};

// Adjust the array of stroke segments if appropriate for the given stroke type and coordinates.
var adjustSegmentsForSomeStrokeTypes = function(strokeType, segments, points) {
  var adjustedPoints = points.slice(0);
  var adjustedSegments = segments.slice(0);
  if (strokeType === 'd') {
    if (points[1].x < points[0].x) {
      /* Make left-falling dian (formerly called "d2") same as pie. */
      adjustedSegments = strokeSegments.p;
    }
  } else if (strokeType === 'szp') {
    /* Change last segment from "l" (line) to "p" if appropriate. */
    if (points[3].x <= points[1].x) {
      adjustedSegments = SZPAlternateSegs; // "Lhp";
    }
  } else if (strokeType === 'hzwg') {
    /* Interpolate one point if appropriate. */
    if (points[2].x !== points[1].x) {
      adjustedSegments = HZWGAlternateSegs;
      var interPoint = new Point(
        /* Insert a new third point. Caution: unlike the C code, we're creating a new array, not modifying
           the original array in place, so we use points[2] here, not points[3] as in the C code where the
           array gets shifted before we do this calculation. */
        Utils.skPercent(points[1].x, 22) + Utils.skPercent(points[2].x, 78),
        Utils.skPercent(points[1].y, 40) + Utils.skPercent(points[2].y, 60)
      );
      adjustedPoints.splice(2, 0, interPoint); // interPoint becomes the new third point = adjustedPoints[2]
    }
  }
  return { segments: adjustedSegments, points: adjustedPoints };
};

module.exports = {
  getPathString: function(strokeType, points) {
    var segments = strokeSegments[strokeType];
    var adjustments = adjustSegmentsForSomeStrokeTypes(strokeType, segments, points);
    var adjustedPoints = adjustments.points;
    var adjustedSegments = adjustments.segments;

    var pathString = Utils.getSvgPathStartString(
      svgPlainX(adjustedPoints[0].x),
      svgPlainY(adjustedPoints[0].y)
    );

    var pointsIndex = 0;
    for (var i = 0; i < adjustedSegments.length; i++) {
      var segment = adjustedSegments[i];
      if (segment === 'h' || segment === 's' || segment === 'L') {
        pathString += Utils.getSvgLineString(
          svgPlainX(adjustedPoints[pointsIndex + 1].x),
          svgPlainY(adjustedPoints[pointsIndex + 1].y)
        );
        pointsIndex += 1;
      } else if (segment === 'z') { /* BEZIER */
        pathString += Utils.getSvgCubicCurveString(
          svgPlainX(adjustedPoints[pointsIndex + 1].x), svgPlainY(adjustedPoints[pointsIndex + 1].y),
          svgPlainX(adjustedPoints[pointsIndex + 2].x), svgPlainY(adjustedPoints[pointsIndex + 2].y),
          svgPlainX(adjustedPoints[pointsIndex + 3].x), svgPlainY(adjustedPoints[pointsIndex + 3].y)
        );
        pointsIndex += 3;
      } else {
        var curvePoints = interpolateCurveXY(adjustedPoints[pointsIndex], adjustedPoints[pointsIndex + 1], segment);
        pathString += Utils.getSvgCubicCurveString(
          svgPlainX(curvePoints[1].x), svgPlainY(curvePoints[1].y),
          svgPlainX(curvePoints[2].x), svgPlainY(curvePoints[2].y),
          svgPlainX(curvePoints[3].x), svgPlainY(curvePoints[3].y)
        );
        pointsIndex += 1;
      }
    }
    return pathString;
  }
};

