/* eslint-env mocha */
var assert = require('chai').assert;
var ShuZheZheGouOutliner = require('../../src/outliners/ShuZheZheGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuZheZheGouOutliner#addPoints', function() {
  it('should return the same output as GetShuZheZheGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1070, 2442),
      new Point(924, 1813),
      new Point(3752, 1813),
      new Point(3362, 904),
      new Point(2898, 1079)
    ];
    var outline = new Outline(32, 103, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(967, 2607, 1),
      new OutlinePoint(1070, 2607, 2),
      new OutlinePoint(1173, 2545, 2),
      new OutlinePoint(1307, 2442, 1),
      new OutlinePoint(1173, 2353, 1),
      new OutlinePoint(1027, 1845, 1),
      new OutlinePoint(3604, 1845, 1),
      new OutlinePoint(3742, 2019, 1),
      new OutlinePoint(3839, 1984, 2),
      new OutlinePoint(3943, 1910, 2),
      new OutlinePoint(3981, 1836, 1),
      new OutlinePoint(3855, 1710, 1),
      new OutlinePoint(3855, 1441, 2),
      new OutlinePoint(3855, 1198, 2),
      new OutlinePoint(3778, 1052, 1),
      new OutlinePoint(3701, 906, 2),
      new OutlinePoint(3480, 801, 2),
      new OutlinePoint(3362, 801, 1),
      new OutlinePoint(3220, 801, 1),
      new OutlinePoint(3220, 852, 2),
      new OutlinePoint(3079, 940, 2),
      new OutlinePoint(2882, 1063, 1),
      new OutlinePoint(2898, 1079, 1),
      new OutlinePoint(3130, 991, 2),
      new OutlinePoint(3194, 1007, 2),
      new OutlinePoint(3259, 1007, 1),
      new OutlinePoint(3356, 1007, 2),
      new OutlinePoint(3454, 1050, 2),
      new OutlinePoint(3551, 1149, 1),
      new OutlinePoint(3649, 1249, 2),
      new OutlinePoint(3649, 1492, 2),
      new OutlinePoint(3649, 1781, 1),
      new OutlinePoint(1027, 1781, 1),
      new OutlinePoint(924, 1646, 1),
      new OutlinePoint(837, 1679, 2),
      new OutlinePoint(769, 1735, 2),
      new OutlinePoint(718, 1813, 1),
      new OutlinePoint(821, 1916, 1)
    ];

    ShuZheZheGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
