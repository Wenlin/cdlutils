var Utils = require('../Utils');
var Point = require('../Point');
var CdlStroke = require('../CdlStroke');
var OutlineHelpers = require('../OutlineHelpers');
var HengOutliner = require('./HengOutliner');

module.exports = {
  addPoints: function(outline, refPoints) {
    if (refPoints[1].y === refPoints[0].y) {
      // Easy, first segment is exactly horizontal
      OutlineHelpers.addHengLeftEndOutlinePoints(outline, refPoints[0]);
    } else { // Starts out slanted, rotation required.
      /* First part is rotated heng */
      var dx = refPoints[1].x - refPoints[0].x;
      var dy = refPoints[1].y - refPoints[0].y;
      var hypotenuse = Utils.hypotenuse(dx, dy);

      /* First make all points relative to refPoints[0] */
      var flatPoints = [];
      flatPoints[0] = new Point(0, 0);
      flatPoints[1] = new Point(hypotenuse, 0);
      var tail = outline.tail; // save
      outline.tail = CdlStroke.STROKE_TAIL_CUT;
      HengOutliner.addPoints(outline, flatPoints);
      outline.tail = tail; // restore
      /* Now rotate around origin, and add refPoints[0] */
      outline.rotateAndTranslate(refPoints[0], dx, dy, hypotenuse);

      // discard last two points (right end of heng)
      outline.deleteLastPoint();
      outline.deleteLastPoint();
    }
    OutlineHelpers.addHengzheOutlineCornerPoints(outline, refPoints[1]);
    if (refPoints[2].x === refPoints[1].x) {
      OutlineHelpers.addShuGouBottomOutline(outline, refPoints[2], refPoints[3]);
    } else {
      OutlineHelpers.addWanGouBottomOutline(outline, refPoints[1], refPoints[2], refPoints[3], true);
    }
    OutlineHelpers.addSouthwestCornerPoint(outline, refPoints[1]);
  }
};
