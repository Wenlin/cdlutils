var RenderBounds = function(x, y, width, height) {
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
};

// ----- getters -----

RenderBounds.prototype.clone = function() {
  return new RenderBounds(this.x, this.y, this.width, this.height);
};

module.exports = RenderBounds;
