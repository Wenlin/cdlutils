/* eslint-env mocha */
var assert = require('chai').assert;
var ShuOutliner = require('../../src/outliners/ShuOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('ShuOutliner#addPoints', function() {
  it('should return the same output as GetShuOutline in ch_zzsol.c case #1', function() {
    var refPoints = [new Point(1256, 4128), new Point(1256, 800)];
    var outline = new Outline(32, 108, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1148, 4301, 1),
      new OutlinePoint(1256, 4301, 2),
      new OutlinePoint(1364, 4236, 2),
      new OutlinePoint(1504, 4128, 1),
      new OutlinePoint(1364, 4035, 1),
      new OutlinePoint(1364, 773, 1),
      new OutlinePoint(1288, 697, 2),
      new OutlinePoint(1224, 665, 2),
      new OutlinePoint(1148, 665, 1)
    ];

    ShuOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
