var DEFAULT_API_URL = 'https://api.wenlincdl.com';

var httpGet = function(url, apiKey) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    if (xhr.overrideMimeType) { // IE 9 and 10 don't seem to support this...
      xhr.overrideMimeType('application/json');
    }
    xhr.open('GET', url, true);
    xhr.setRequestHeader('x-api-key', apiKey);
    xhr.onreadystatechange = function() {
      // TODO: add error handling
      if (xhr.readyState === 4 && xhr.status === 200) {
        resolve(xhr.responseText);
      }
    };
    xhr.send(null);
  });
};

// options.apiKey: the user's API key
// options.apiUrl: the base URL of the api. You probably don't need to change this.

var apiDbLoader = function(options) {
  var baseUrl = options.apiUrl || DEFAULT_API_URL;

  return function(unicode, variant, xmlParser) {
    var url = baseUrl + '/cdl/' + unicode + '.xml?variant=' + variant + '&recursive=true';
    return httpGet(url, options.apiKey);
  };
};

module.exports = apiDbLoader;
