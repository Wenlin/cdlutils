module.exports = {};

// javascript doesn't do int math, so this function simulates how C truncates ints
var intCast = function(val) {
  return val < 0 ? Math.ceil(val) : Math.floor(val);
};
module.exports.intCast = intCast;

module.exports.hypotenuse = function(dx, dy) {
  var hypotenuse = intCast(Math.sqrt((dx * dx) + (dy * dy)));
  return (hypotenuse < 1) ? 1 : hypotenuse;
};

/* SK_PERCENT() is convenient macro to represent a percentage of a dimension.
  For efficiency, implement using division by 256 rather than 100. */
// #define SK_PERCENT(d, percent) (((d) * (percent) + 50) / 100)
module.exports.skPercent = function(d, percent) {
  // flooring here to get results closer to what C does with int division
  return intCast(((d) * intCast((percent * 256 + 50) / 100) + 128) / 256);
};

var GRID_SIZE = 128;
var GRID_SHIFT = 7;
// tests pass removing GRID_SIZE/2 below, even though in the c file it's 128...
// not sure what's up... keep an eye on this if other stuff starts breaking
module.exports.xyScaleMul = function(x, wid) {
  return (x * wid /* + GRID_SIZE / 2 */) >> GRID_SHIFT;
};

module.exports.xyScaleMulFloat = function(x, wid) {
  return (x * wid) / GRID_SIZE;
};

var xmlParsingOpts = {
  explicitChildren: true,
  preserveChildrenOrder: true,
  explicitRoot: false
};

// promise wrapper around xml2js.parseString
module.exports.parseXml = function(xml, xmlParser) {
  return new Promise(function(resolve, reject) {
    xmlParser.parseString(xml, xmlParsingOpts, function(err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
};

module.exports.getSvgPathStartString = function(x, y) {
  return 'M' + x.toFixed(4) + ',' + y.toFixed(4) + '\n';
};
module.exports.getSvgLineString = function(x, y) {
  return '\tL' + x.toFixed(4) + ',' + y.toFixed(4) + '\n';
};
module.exports.getSvgCubicCurveString = function(x1, y1, x2, y2, x3, y3) {
  return '\tC' + x1.toFixed(4) + ',' + y1.toFixed(4) + ' '
               + x2.toFixed(4) + ',' + y2.toFixed(4) + ' '
               + x3.toFixed(4) + ',' + y3.toFixed(4) + '\n';
};
module.exports.getSvgQuadraticCurveString = function(x1, y1, x2, y2) {
  return '\tQ' + x1.toFixed(4) + ',' + y1.toFixed(4) + ' '
               + x2.toFixed(4) + ',' + y2.toFixed(4) + '\n';
};

// assuming '$tag' is the $tag name and '$children' are the children
var xmlFromRawJs = function(rawJs) {
  var xml = '<' + rawJs.$tag;
  for (var prop in rawJs) {
    if (rawJs.hasOwnProperty(prop) && prop !== '$tag' && prop !== '$children') {
      xml += ' ' + prop + "='" + rawJs[prop] + "'";
    }
  }
  if (!rawJs.$children) {
    return xml + ' />';
  }
  var childrenXml = rawJs.$children.map(function(child) {
    return xmlFromRawJs(child).split('\n').map(function(xmlLine) {
      return '\t' + xmlLine + '\n'; // indent each line by another tab
    }).join('');
  });
  xml += '>\n' + childrenXml.join('');
  return xml + '</' + rawJs.$tag + '>';
};
module.exports.xmlFromRawJs = xmlFromRawJs;
