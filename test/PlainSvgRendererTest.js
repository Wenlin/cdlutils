/* eslint-env mocha */
var assert = require('chai').assert;
var PlainSvgRenderer = require('../src/PlainSvgRenderer');
var Point = require('../src/Point');
var splitParseAndRound = require('./Helpers').splitParseAndRound;


describe('PlainSvgRenderer#getPathString', function() {
  it('should return the same output as WritePlainStroke in ch_zzstr.c case #1', function() {
    var strokeType = 'p';
    var refPoints = [
      new Point(2048, 0),
      new Point(0, 4096)
    ];

    var expectedPathString = 'M32.0000,0.0000\n' +
      '\tC26.5000,30.0000 13.5000,53.0000 0.0000,64.0000\n' +
      '\t';

    var pathString = PlainSvgRenderer.getPathString(strokeType, refPoints);
    assert.deepEqual(splitParseAndRound(pathString), splitParseAndRound(expectedPathString));
  });

  it('should return the same output as WritePlainStroke in ch_zzstr.c case #2', function() {
    var strokeType = 'd';
    var refPoints = [
      new Point(5198, 0),
      new Point(5716, 1196)
    ];

    var expectedPathString = 'M81.2188,0.0000\n' +
      '\tC86.2773,5.2559 89.0596,11.6797 89.3125,18.6875\n' +
      '\t';

    var pathString = PlainSvgRenderer.getPathString(strokeType, refPoints);
    assert.deepEqual(splitParseAndRound(pathString), splitParseAndRound(expectedPathString));
  });

  it('should return the same output as WritePlainStroke in ch_zzstr.c case #3', function() {
    var strokeType = 'hz';
    var refPoints = [
      new Point(0, 3436),
      new Point(1408, 3436),
      new Point(1408, 6916)
    ];

    var expectedPathString = 'M0.0000,53.6875\n' +
      '\tL22.0000,53.6875\n' +
      '\tL22.0000,108.0625\n' +
      '\t';

    var pathString = PlainSvgRenderer.getPathString(strokeType, refPoints);
    assert.deepEqual(splitParseAndRound(pathString), splitParseAndRound(expectedPathString));
  });

  it('should return the same output as WritePlainStroke in ch_zzstr.c case #4', function() {
    var strokeType = 'tpn';
    var refPoints = [
      new Point(0, 7728),
      new Point(1408, 6916),
      new Point(5632, 8192),
      new Point(8192, 8192)
    ];

    var expectedPathString = 'M0.0000,120.7500\n' +
      '\tL22.0000,108.0625\n' +
      '\tC22.0000,118.0312 55.0000,128.0000 88.0000,128.0000\n' +
      '\tL128.0000,128.0000\n' +
      '\t';

    var pathString = PlainSvgRenderer.getPathString(strokeType, refPoints);
    assert.deepEqual(splitParseAndRound(pathString), splitParseAndRound(expectedPathString));
  });

  it('should return the same output as WritePlainStroke in ch_zzstr.c case #5', function() {
    var strokeType = 'hzzp';
    var refPoints = [
      new Point(918, 256),
      new Point(5100, 256),
      new Point(4488, 2612),
      new Point(6528, 2612),
      new Point(1530, 8192)
    ];

    var expectedPathString = 'M14.3438,4.0000\n' +
      '\tL79.6875,4.0000\n' +
      '\tL70.1250,40.8125\n' +
      '\tL102.0000,40.8125\n' +
      '\tC88.5776,81.6816 56.8521,113.0146 23.9062,128.0000\n' +
      '\t';

    var pathString = PlainSvgRenderer.getPathString(strokeType, refPoints);
    assert.deepEqual(splitParseAndRound(pathString), splitParseAndRound(expectedPathString));
  });
});
