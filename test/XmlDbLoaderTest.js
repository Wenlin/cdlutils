/* eslint-env mocha */
var chai = require('chai');
var assert = chai.assert;

var xmlDbLoader = require('../src/xmlDbLoader');
var Utils = require('../src/Utils');
var xml2js = require('xml2js');

describe('xmlDbLoader', function() {
  var cdlDb = '' +
  "<?xml version='1.0' standalone='no' ?>\n" +
  "<!DOCTYPE cdl-list SYSTEM 'cdl.dtd'>\n" +
  '<cdl-list>\n' +
  '  <!-- CDL (Character Description Language)\n' +
  '    Copyright (c) 2015 Wenlin Institute, Inc. SPC. All rights reserved.\n' +
  '    http://wenlin.com/cdl/\n' +
  '    Exported from database\n' +
  '    2015-12-10T23:40:18Z (2015-12-10T18:40:18COT)\n' +
  '  -->\n' +
  "  <cdl char='仁' uni='4EC1'>\n" +
  "    <comp char='亻' uni='4EBB' points='0,0 34,128' />\n" +
  "    <comp char='二' uni='4E8C' points='46,20 128,112' />\n" +
  '  </cdl>\n' +
  "  <cdl char='仁󰀁' uni='4EC1' variant='1' radical='K0'>\n" +
  "    <comp char='亻' uni='4EBB' points='0,0 31,128' />\n" +
  "    <comp char='二' uni='4E8C' points='45,8 128,38' />\n" +
  '    <!-- variant based on [U+E927](V=0) -->\n' +
  '  </cdl>\n' +
  "  <cdl char='砦' uni='7826' points='0,0 128,124'>\n" +
  "    <comp char='此' uni='6B64' points='0,0 128,56' />\n" +
  "    <comp char='石' uni='77F3' points='4,72 124,128' />\n" +
  '  </cdl>\n' +
  '</cdl-list>';

  it('should take in an XML list full of CDL chars and return a valid loader func', function(done) {
    var expectedCdlCharXml = '' +
    "<cdl char='仁' uni='4EC1'>\n" +
    "  <comp char='亻' uni='4EBB' points='0,0 34,128' />\n" +
    "  <comp char='二' uni='4E8C' points='46,20 128,112' />\n" +
    '</cdl>';

    var loader = xmlDbLoader(cdlDb);
    loader('4EC1', 0, xml2js).then(function(actualCharData) {
      return Utils.parseXml(expectedCdlCharXml, xml2js).then(function(expectedCharData) {
        assert.deepEqual(actualCharData, expectedCharData);
        done();
      });
    }).catch(function(err) {
      done(err);
    });
  });

  it('should work with different char variants too', function(done) {
    var expectedCdlCharXml = '' +
      "<cdl char='仁󰀁' uni='4EC1' variant='1' radical='K0'>\n" +
      "  <comp char='亻' uni='4EBB' points='0,0 31,128' />\n" +
      "  <comp char='二' uni='4E8C' points='45,8 128,38' />\n" +
      '  <!-- variant based on [U+E927](V=0) -->\n' +
      '</cdl>';

    var loader = xmlDbLoader(cdlDb);
    loader('4EC1', 1, xml2js).then(function(actualCharData) {
      return Utils.parseXml(expectedCdlCharXml, xml2js).then(function(expectedCharData) {
        assert.deepEqual(actualCharData, expectedCharData);
        done();
      });
    }).catch(function(err) {
      done(err);
    });
  });
});
