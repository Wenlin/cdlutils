var outlinerMap = {
  h: require('./outliners/HengOutliner'),
  t: require('./outliners/TiOutliner'),
  s: require('./outliners/ShuOutliner'),
  sg: require('./outliners/ShuGouOutliner'),
  p: require('./outliners/PieOutliner'),
  wp: require('./outliners/WanPieOutliner'),
  sp: require('./outliners/ShuPieOutliner'),
  d: require('./outliners/DianOutliner'),
  n: require('./outliners/NaOutliner'),
  dn: require('./outliners/DianNaOutliner'),
  pn: require('./outliners/PingNaOutliner'),
  tn: require('./outliners/TiNaOutliner'),
  tpn: require('./outliners/TiPingNaOutliner'),
  hz: require('./outliners/HengZheOutliner'),
  hp: require('./outliners/HengPieOutliner'),
  hg: require('./outliners/HengGouOutliner'),
  sz: require('./outliners/ShuZheOutliner'),
  sw: require('./outliners/ShuWanOutliner'),
  st: require('./outliners/ShuTiOutliner'),
  pz: require('./outliners/PieZheOutliner'),
  pd: require('./outliners/PieDianOutliner'),
  pg: require('./outliners/PieGouOutliner'),
  wg: require('./outliners/WanGouOutliner'),
  xg: require('./outliners/XieGouOutliner'),
  hzz: require('./outliners/HengZheZheOutliner'),
  hzw: require('./outliners/HengZheWanOutliner'),
  hzt: require('./outliners/HengZheTiOutliner'),
  hzg: require('./outliners/HengZheGouOutliner'),
  hxg: require('./outliners/HengXieGouOutliner'),
  szz: require('./outliners/ShuZheZheOutliner'),
  szp: require('./outliners/SZPOutliner'),
  swg: require('./outliners/ShuWanGouOutliner'),
  hzzz: require('./outliners/HengZheZheZheOutliner'),
  hzzp: require('./outliners/HengZheZhePieOutliner'),
  hzwg: require('./outliners/HZWGOutliner'),
  hpwg: require('./outliners/HPWGOutliner'),
  szzg: require('./outliners/ShuZheZheGouOutliner'),
  hzzzg: require('./outliners/HengZheZheZheGouOutliner'),
  b: require('./outliners/BezierOutliner'),
  c: require('./outliners/ArcQuadrantOutliner'),
  swz: require('./outliners/ShuWanZuoOutliner')
};


module.exports = {
  getOutlinerForStroke: function(strokeName) {
    return outlinerMap[strokeName];
  }
};
