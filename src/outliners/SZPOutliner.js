var OutlineHelpers = require('../OutlineHelpers');
var Point = require('../Point');

module.exports = {
  addPoints: function(outline, refPoints) {
    OutlineHelpers.addShuTopOutlinePoints(outline, refPoints[0]);
    OutlineHelpers.addNortheastCornerPoint(outline, refPoints[1]);
    if (refPoints[3].x > refPoints[1].x) { // make the bottom a straight hook, not curved
      OutlineHelpers.addHengGouRightEndOutlinePoints(outline, refPoints[2], refPoints[3]);
    } else { // bottom curved pie
      // TODO: figure out how to test this branch
      OutlineHelpers.addHengPieOutlineCornerPoints(outline, refPoints[2]);
      var curveStartPoint = outline.getLastOutlinePoint().toPoint();
      outline.deleteLastPoint(); // don't add curveStartPoint twice!
      var bottomRefPoint = new Point(
        refPoints[2].x - outline.shuRadius,
        refPoints[2].y - outline.hengRadius
      );
      OutlineHelpers.addPieTipOutline(outline, curveStartPoint, refPoints[3], bottomRefPoint, false);
    }
    OutlineHelpers.addShuZheOutlineCornerPoints(outline, refPoints[1]);
  }
};
