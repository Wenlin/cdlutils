/* eslint-env mocha */
var assert = require('chai').assert;
var TiNaOutliner = require('../../src/outliners/TiNaOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('TiNaOutliner#addPoints', function() {
  it('should return the same output as GetTiNaOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(2088, 4128),
      new Point(2842, 4128),
      new Point(3856, 2464)
    ];
    var outline = new Outline(32, 114, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2842, 4160, 1),
      new OutlinePoint(3048, 3399, 2),
      new OutlinePoint(3518, 2712, 2),
      new OutlinePoint(4045, 2590, 1),
      new OutlinePoint(4045, 2562, 1),
      new OutlinePoint(3912, 2526, 2),
      new OutlinePoint(3823, 2466, 2),
      new OutlinePoint(3730, 2370, 1),
      new OutlinePoint(3341, 2666, 2),
      new OutlinePoint(2994, 3259, 2),
      new OutlinePoint(2842, 4096, 1),
      new OutlinePoint(2088, 3943, 1),
      new OutlinePoint(1992, 3980, 2),
      new OutlinePoint(1917, 4041, 2),
      new OutlinePoint(1860, 4128, 1)
    ];

    TiNaOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
