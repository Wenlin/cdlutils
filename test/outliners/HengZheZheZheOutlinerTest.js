/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheZheZheOutliner = require('../../src/outliners/HengZheZheZheOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheZheZheOutliner#addPoints', function() {
  it('should return the same output as GetHengZheZheZheOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(632, 4024),
      new Point(2192, 4024),
      new Point(2192, 2561),
      new Point(3752, 2561),
      new Point(3752, 904)
    ];
    var outline = new Outline(32, 120, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(728, 3992, 1),
      new OutlinePoint(632, 3936, 1),
      new OutlinePoint(568, 4056, 1),
      new OutlinePoint(2019, 4056, 1),
      new OutlinePoint(2180, 4264, 1),
      new OutlinePoint(2292, 4223, 2),
      new OutlinePoint(2414, 4136, 2),
      new OutlinePoint(2458, 4050, 1),
      new OutlinePoint(2312, 3904, 1),
      new OutlinePoint(2312, 2593, 1),
      new OutlinePoint(3579, 2593, 1),
      new OutlinePoint(3740, 2801, 1),
      new OutlinePoint(3852, 2760, 2),
      new OutlinePoint(3974, 2673, 2),
      new OutlinePoint(4018, 2587, 1),
      new OutlinePoint(3872, 2441, 1),
      new OutlinePoint(3872, 874, 1),
      new OutlinePoint(3788, 790, 2),
      new OutlinePoint(3716, 754, 2),
      new OutlinePoint(3632, 754, 1),
      new OutlinePoint(3632, 2529, 1),
      new OutlinePoint(2312, 2529, 1),
      new OutlinePoint(2192, 2366, 1),
      new OutlinePoint(2091, 2405, 2),
      new OutlinePoint(2012, 2470, 2),
      new OutlinePoint(1952, 2561, 1),
      new OutlinePoint(2072, 2681, 1),
      new OutlinePoint(2072, 3992, 1)
    ];

    HengZheZheZheOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
