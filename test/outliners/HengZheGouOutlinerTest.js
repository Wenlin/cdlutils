/* eslint-env mocha */
var assert = require('chai').assert;
var HengZheGouOutliner = require('../../src/outliners/HengZheGouOutliner');
var Point = require('../../src/Point');
var OutlinePoint = require('../../src/OutlinePoint');
var Outline = require('../../src/Outline');

describe('HengZheGouOutliner#addPoints', function() {
  it('should return the same output as GetHengZheGouOutline in ch_zzsol.c case #1', function() {
    var refPoints = [
      new Point(1186, 2280),
      new Point(2196, 2280),
      new Point(2196, 480),
      new Point(1815, 719)
    ];
    var outline = new Outline(32, 63, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1282, 2248, 1),
      new OutlinePoint(1186, 2192, 1),
      new OutlinePoint(1122, 2312, 1),
      new OutlinePoint(2105, 2312, 1),
      new OutlinePoint(2190, 2406, 1),
      new OutlinePoint(2249, 2385, 2),
      new OutlinePoint(2313, 2339, 2),
      new OutlinePoint(2336, 2294, 1),
      new OutlinePoint(2259, 2217, 1),
      new OutlinePoint(2259, 543, 1),
      new OutlinePoint(2259, 480, 2),
      new OutlinePoint(2176, 417, 2),
      new OutlinePoint(2093, 417, 1),
      new OutlinePoint(2093, 449, 2),
      new OutlinePoint(1990, 584, 2),
      new OutlinePoint(1800, 704, 1),
      new OutlinePoint(1815, 719, 1),
      new OutlinePoint(2005, 599, 2),
      new OutlinePoint(2133, 587, 2),
      new OutlinePoint(2133, 606, 1),
      new OutlinePoint(2133, 2248, 1)
    ];

    HengZheGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetHengZheGouOutline in ch_zzsol.c case #2', function() {
    var refPoints = [
      new Point(2521, 3504),
      new Point(3752, 3504),
      new Point(3402, 800),
      new Point(3000, 1268)
    ];
    var outline = new Outline(32, 108, 1, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(2521, 3472, 1),
      new OutlinePoint(2521, 3536, 1),
      new OutlinePoint(3596, 3536, 1),
      new OutlinePoint(3741, 3720, 1),
      new OutlinePoint(3842, 3684, 2),
      new OutlinePoint(3952, 3606, 2),
      new OutlinePoint(3992, 3528, 1),
      new OutlinePoint(3860, 3396, 1),
      new OutlinePoint(3860, 2416, 2),
      new OutlinePoint(3860, 1635, 2),
      new OutlinePoint(3794, 1266, 1),
      new OutlinePoint(3728, 897, 2),
      new OutlinePoint(3511, 692, 2),
      new OutlinePoint(3402, 692, 1),
      new OutlinePoint(3274, 692, 1),
      new OutlinePoint(3274, 746, 2),
      new OutlinePoint(3147, 980, 2),
      new OutlinePoint(2984, 1252, 1),
      new OutlinePoint(3000, 1268, 1),
      new OutlinePoint(3201, 1034, 2),
      new OutlinePoint(3247, 908, 2),
      new OutlinePoint(3294, 908, 1),
      new OutlinePoint(3381, 908, 2),
      new OutlinePoint(3469, 1048, 2),
      new OutlinePoint(3556, 1368, 1),
      new OutlinePoint(3644, 1689, 2),
      new OutlinePoint(3644, 2470, 2),
      new OutlinePoint(3644, 3472, 1)
    ];

    HengZheGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });

  it('should return the same output as GetHengZheGouOutline in ch_zzsol.c case #3', function() {
    var refPoints = [
      new Point(1516, 2339),
      new Point(3526, 3422),
      new Point(3344, 2062),
      new Point(2996, 2364)
    ];
    var outline = new Outline(32, 114, 0, 0);

    var expectedOutlinePoints = [
      new OutlinePoint(1615, 2356, 1),
      new OutlinePoint(1557, 2262, 1),
      new OutlinePoint(1445, 2337, 1),
      new OutlinePoint(3362, 3454, 1),
      new OutlinePoint(3514, 3650, 1),
      new OutlinePoint(3621, 3611, 2),
      new OutlinePoint(3737, 3529, 2),
      new OutlinePoint(3779, 3447, 1),
      new OutlinePoint(3640, 3308, 1),
      new OutlinePoint(3640, 2869, 2),
      new OutlinePoint(3640, 2494, 2),
      new OutlinePoint(3617, 2289, 1),
      new OutlinePoint(3594, 2084, 2),
      new OutlinePoint(3412, 1948, 2),
      new OutlinePoint(3344, 1948, 1),
      new OutlinePoint(3228, 1948, 1),
      new OutlinePoint(3228, 2005, 2),
      new OutlinePoint(3113, 2156, 2),
      new OutlinePoint(2980, 2348, 1),
      new OutlinePoint(2996, 2364, 1),
      new OutlinePoint(3170, 2213, 2),
      new OutlinePoint(3200, 2176, 2),
      new OutlinePoint(3230, 2176, 1),
      new OutlinePoint(3275, 2176, 2),
      new OutlinePoint(3321, 2243, 2),
      new OutlinePoint(3366, 2397, 1),
      new OutlinePoint(3412, 2551, 2),
      new OutlinePoint(3412, 2926, 2),
      new OutlinePoint(3412, 3390, 1)
    ];

    HengZheGouOutliner.addPoints(outline, refPoints);

    assert.deepEqual(outline.outlinePoints, expectedOutlinePoints);
  });
});
