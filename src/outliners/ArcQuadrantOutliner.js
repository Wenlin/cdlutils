var Utils = require('../Utils');
var Point = require('../Point');
var BezierOutliner = require('./BezierOutliner');

module.exports = {
  addPoints: function(outline, refPoints) {
    var xStart = refPoints[0].x;
    var yStart = refPoints[0].y;
    var xEnd   = refPoints[1].x;
    var yEnd   = refPoints[1].y;

    var bPoints = [];
    bPoints[0] = refPoints[0];
    bPoints[3] = refPoints[1];
    /* For conic control points, should be 0.586 (2-sqrt(2))
        distance from corner to each end. */
    if ((yEnd > yStart && xEnd > xStart) || (yEnd < yStart && xEnd < xStart)) {
      // want xmid closer to xEnd, ymid closer to yStart
      bPoints[1] = new Point(Utils.intCast(xStart + 0.586 * (xEnd - xStart)), yStart);
      bPoints[2] = new Point(xEnd,  Utils.intCast(yStart + 0.586 * (yEnd - yStart)));
    } else {
      // want xmid closer to xStart, ymid closer to yEnd
      bPoints[1] = new Point(xStart, Utils.intCast(yEnd + 0.586 * (yStart - yEnd)));
      bPoints[2] = new Point(Utils.intCast(xEnd + 0.586 * (xStart - xEnd)), yEnd);
    }
    BezierOutliner.addPoints(outline, bPoints);
  }
};
