module.exports.splitParseAndRound = function(pathString) {
  return pathString.split(/\s+/).map(function(pathStringPart) {
    // chop off the last digit because JS and C round slightly differently
    // and the difference basically doesn't matter
    var coords = pathStringPart.split(',').map(function(coord) {
      return coord.slice(0, -1);
    });
    return coords.join(',');
  });
};
